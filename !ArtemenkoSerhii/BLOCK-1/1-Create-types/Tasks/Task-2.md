# Task 002

## Создание простых классов (Конструкторы, методы, свойства для полей и т.д.)

1. Определить класс с именем Order содержащий следующие поля:
    - расчетный счет плательщика (формат уточнить в Интернете);
    - расчетный счет получателя;
    - перечисляемая сумма в грн, $, евро
    Методы:
    - упорядочить по убыванию перечисляемой суммы;
    - вывод информации о тех плательщиках, перечисляемая сумма которых не меньше суммы, введенной пользователем.
    - ввод данных в массив из n элементов типа Order в Main;

2. Определить класс с именем Worker, содержащий следующие поля:
    - фамилия и инициалы работника;
    - название занимаемой должности;
    - год поступления на работу.
    Методы:
    - ввод данных в массив из n элементов в типа Worker;
    - упорядочить по алфавиту фамилии работников;
    - вывод работников, чей стаж работы в фирме превышает значение, введенное пользователем.

3. Определить класс с именем Price, содержащий следующие поля:
    - название товара;
    - название магазина, в котором продается товар;
    - стоимость товара в грн.
    Методы:
    - ввод данных в массив из n элементов в типа Price;
    - упорядочить в алфавитном порядке по названиям товаров;
    - вывод информации о товаре, название которого ввел пользователь.

4. Создать класс с именем Rectangle, содержащий следующие поля:
    - два поля, описывающие длины сторон double side1, side2.
    - создать пользовательский конструктор Rectangle(double side1, double side2), в теле которого поля side1 и side2 инициализируются значениями аргументов
    Методы
    - вычисление площади прямоугольника - double AreaCalculator()
    - периметр прямоугольника - double PerimeterCalculator()
    - Создать два свойства double Area и double Perimeter с одним методом доступа get
    - Написать программу, которая принимает от пользователя длины двух сторон прямоугольника и выводит на экран периметр и площадь.

### Вопросы

- You want to store information about the employees that work in my company. Is this a job for a value type or a reference type?
- Can an object be referred to by more than one reference?
- What happens when an object no longer has any references to it?
- If you use a value type rather than a reference type, will your program still work correctly?
- How can you make a data type immutable?
- Are value types more efficient that reference types?
- What is the difference between the stack and the heap?
- Do you need to add a constructor method for every object that you create?
- Is there ever any point in creating objects that cannot be constructed?
- What does it mean when a member of a type is made static?
- Should you provide default values for all of the parameters to a method?
- Does using a lot of overridden methods slow a program down?
