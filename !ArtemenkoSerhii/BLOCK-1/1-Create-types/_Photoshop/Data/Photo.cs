using System;

namespace MyPhotoshop {
    public class Photo {
        public readonly int width;
        public readonly int height;
        public readonly Pixel[,] data;

        public Photo(int w, int h) {
            width = w;
            height = h;
            data = new Pixel[w, h];
        }

        public Pixel this[int index1, int index2] {
            get { return data[index1, index2]; }
            set { data[index1, index2] = value; }
        }
    }
}

