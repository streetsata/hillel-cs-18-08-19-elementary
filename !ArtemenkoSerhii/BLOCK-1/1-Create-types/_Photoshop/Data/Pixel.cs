﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPhotoshop {
    public struct Pixel {
        private double r;
        private double g;
        private double b;

        public Pixel(double r, double g, double b) {
            this.r = 0; this.g = 0; this.b = 0;
            R = r;
            G = g;
            B = b;
        }

        public double R { get { return r; } set { r = Check(value); } }
        public double G { get { return g; } set { g = Check(value); } }
        public double B { get { return b; } set { b = Check(value); } }



        /// <summary>
        /// Проверка значения в диапазоне
        /// </summary>
        /// <param name="v">Значение, которое необходимо проверить</param>
        /// <returns>Значение или исключение</returns>
        private double Check(double v) {
            if (v >= 0 && v <= 1) return v;
            else throw new Exception();
        }

        public static double Trim(double v) {
            if (v < 0) return 0;
            if (v > 1) return 1;

            return v;
        }

        public static Pixel operator *(Pixel p, double c) {
            return new Pixel(
                        Trim(p.R * c),
                        Trim(p.G * c),
                        Trim(p.B * c));
        }

        public static Pixel operator *(double c, Pixel p) {
            return p * c;
        }
    }
}
