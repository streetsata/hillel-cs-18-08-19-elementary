﻿using System;
using System.Collections;
using System.Text;
using Create_Types.Artemenko;

namespace Create_Types
{
    public static class ExtensionMethods {
        public static int LineCount(this string str) {
            return str.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static int IndexOf(this StringBuilder str, char ch) {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ch) return i;
            }

            return -1;
        }
    }
    partial class Program
    {
        static void Main(params string[] args)
        {
            string str = @"By constraining the type parameter,
you increase the number of allowable operations and method
calls to those supported by the constraining type and all types
in its inheritance hierarchy. When you design generic classes or methods, 
if you will be performing any operation on the generic members beyond
simple assignment or calling any methods not supported by System.Object,
you will have to apply constraints to the type parameter. For example,
the base class constraint tells the compiler that only objects o
f this type or derived from this type will be used as type arguments.
Once the compiler has this guarantee, it can allow method";

            StringBuilder stringBuilder = new StringBuilder("Hello, World!");
            Console.WriteLine(stringBuilder.IndexOf(','));


            Console.WriteLine(str.LineCount());


        }


    }
}
