﻿namespace Create_Types.Artemenko
{
    enum ConcoleColore {
        red, green, blue
    };
    class ClassData
    {
        private byte ageOfPerson;

        private ConcoleColore ConcoleColore;

        public byte AgeOfPerson {
            private get {
                return ageOfPerson;
            }
            set
            {
                if (value >= 18)
                    ageOfPerson = value;
            }
        }

        internal ConcoleColore ConcoleColore1 { get => ConcoleColore; set => ConcoleColore = value; }
        public byte AgeOfPerson1 { get => ageOfPerson; set => ageOfPerson = value; }

        public readonly int Some2 = 8;
        public const int Some = 9;
        public int xClass, yClass;
    }
}
