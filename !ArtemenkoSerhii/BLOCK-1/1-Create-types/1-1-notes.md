# Creating types

* Create value types, including structs and enum

* Create reference types, generic types, constructors, static variables, methods, classes, and extension methods

* Create optional and named parameters

* Create indexed properties

* Create overloaded and overridden methods
