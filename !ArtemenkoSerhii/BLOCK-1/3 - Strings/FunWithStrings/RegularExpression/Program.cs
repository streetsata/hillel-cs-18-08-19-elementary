﻿using System;
using System.Net;
using System.Text.RegularExpressions;

/*
     МЕТАСИМВОЛЫ - это символы для составления шаблона поиска в подстроке

    \d - Определяет символ цифр
    \D - Любой символ, который не является цифрой
    \w - Любой символ цифры, буквы, знака подчеркивания [0-9][a-zA-Z][_]
    \W - Любой символ НЕ(цифры, буквы, знака подчеркивания) [@#$%...]
    \s - Любой пробельный (непечатный) символ [Tab, Enter, Space]
    \S - Любой символ, кроме пробельных и возврата каретки
     . - Любой символ, кроме символов новой строки
    \. - Определяет символ точки
    [0-9][a-zA-Z] - диапазон

    КВАНТИФИКАТОРЫ - символы которые определяют, где и сколько раз необходимое вхождение символа может встречатся

    ^ в начале строки
    $ В конце строки
    + - одно и более вхождений шаблонов в строке
    * - Может встречатся/не встречатся
*/

namespace RegularExpression {
    class Program {
        static void Main(string[] args) {
            {
                //string pattern = @"\d";
                //Regex regex = new Regex(pattern);
                //bool result = regex.IsMatch("ssa3");
            }

            {
                //string pattern = @"\d+";
                //string pattern = @"^\d+";
                //string pattern = @"\d+$";
                //string pattern = @"^\d*\D+\d+$"; // 23

                //var regex = new Regex(pattern);
                //string[] array = { "test", "123test", "test123", "123", "123test123" };

                //foreach (var item in array) {
                //    Console.WriteLine(
                //        regex.IsMatch(item)
                //        ? $"Строка {item} соответствует шаблону {pattern}"
                //        : $"Строка {item} НЕ соответствует шаблону {pattern}");
                //}
            }

            {
                //string line = "Какой-то текст, текст который от другого тексттекста ничем не отличается";
                //Regex regex = new Regex("текст");
                //MatchCollection matches = regex.Matches(line);
                //Console.WriteLine(matches.Count);
            }
            {
                //string line = "Какой-то текст,             \n текст который от другого     тексттекста ничем не отличается";
                //line = new Regex(@"\s+").Replace(line, " ");
            }
            {
                string line = string.Empty;
                using (WebClient wc = new WebClient()) {
                    //line = wc.DownloadString("https://bank.gov.ua/markets/exchangerates/?date=06.10.2019&period=daily");
                    line = wc.DownloadString("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange");
                    line = new Regex(@"\s+").Replace(line, " ");
                    Match match = Regex.Match(line, "<txt>Долар США</txt> <rate>(.*?)</rate> <cc>(.*?)</cc>", RegexOptions.Singleline);
                    Console.WriteLine(match.Groups[1].Value);
                    Console.WriteLine(match.Groups[2].Value);
                }
            }
        }
    }
}
