﻿using System;
using System.Globalization;
using System.Text;

namespace Strings {
    class Program {
        static void Main(string[] args) {
            #region Неизменяемость строк (Immutable)
            {
                //string s = "Hello";
                //Console.WriteLine(s);
                //Console.WriteLine(s.GetHashCode());
                //s = s.Insert(3, "kk");
                //Console.WriteLine(s);
                //Console.WriteLine(s.GetHashCode());
            }
            #endregion

            #region StringBuilder
            {
                //StringBuilder stringBuilder = new StringBuilder();
                //stringBuilder.Append("Hello");
                //Console.WriteLine(stringBuilder.GetHashCode());
                //stringBuilder.Append(", World!");
                //Console.WriteLine(stringBuilder.GetHashCode());
            }
            #endregion

            #region Интернирование строк
            //string str1 = "c:\\windows\\system32";
            //string str2 = @"c:\windows\system32";

            //// Ссылки совпадают
            //Console.WriteLine(ReferenceEquals(str1, str2));

            ////string strNew = Console.ReadLine();
            //string strNew = string.Intern(Console.ReadLine());
            //string strNew2 = string.Intern(Console.ReadLine());

            //Console.WriteLine(ReferenceEquals(strNew2, strNew));
            #endregion

            #region IFormatable
            //Temperature temperature = new Temperature(-273.15m);
            
            //Console.WriteLine($"Temperature [default]       = {temperature}");
            //Console.WriteLine($"Temperature [Fahrenheit]    = {temperature:F}");
            //Console.WriteLine($"Temperature [CultureInfo]    = {temperature.ToString("G", CultureInfo.CreateSpecificCulture("de-de"))}");
            #endregion

            #region CultureInfo
            CultureInfo cultureInfo = CultureInfo.CurrentCulture;
            Console.WriteLine(cultureInfo);

            CultureInfo[] cultureInfos = CultureInfo.GetCultures(CultureTypes.AllCultures);

            Console.WriteLine(cultureInfos.Length);

            foreach (var item in cultureInfos) {
                Console.WriteLine($"{item.EnglishName} | {item.ToString()}");
            }

            Console.ReadKey();
            string[] days = CultureInfo.CurrentCulture.DateTimeFormat.DayNames;

            foreach (var item in days) {
                Console.WriteLine(item);
            }
            Console.ReadKey();
            days = CultureInfo.GetCultureInfo("ja").DateTimeFormat.DayNames;

            foreach (var item in days) {
                Console.WriteLine(item);
            }
            #endregion
        }
    }
}
