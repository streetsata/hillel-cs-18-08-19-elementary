﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Strings {
    class Temperature : IFormattable {
        private decimal temperature;

        public Temperature(decimal temperature) {
            if (temperature < -273.15m)
                throw new ArgumentOutOfRangeException($"{temperature} is less then absolute zero");
            
            this.temperature = temperature;
        }

        public decimal Celsius { get { return temperature; } }
        public decimal Fahrenheit { get { return temperature * 9 / 5 + 32; } }
        public decimal Kelvin { get { return temperature + 273.15m; } }

        public override string ToString() {
            return this.ToString("G", CultureInfo.CurrentCulture);
        }

        public string ToString(string format, IFormatProvider formatProvider) {
            if (string.IsNullOrEmpty(format)) format = "G";
            if (formatProvider == null) formatProvider = CultureInfo.CurrentCulture;

            switch (format.ToUpperInvariant()) {
                case "C":
                case "G":
                    return temperature.ToString("F2", formatProvider) + " °C";
                case "F":
                    return Fahrenheit.ToString("F2", formatProvider) + " °F";
                case "K":
                    return Kelvin.ToString("F2", formatProvider) + " K";

                default:
                    throw new FormatException($"The {formatProvider} format is not supported");
            }
        }
    }
}
