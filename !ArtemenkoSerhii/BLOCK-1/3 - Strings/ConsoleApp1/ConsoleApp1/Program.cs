﻿using System;
using System.Globalization;

namespace ConsoleApp1 {
    class Program {
        static void Main(string[] args) {
            // Даны переменные A, B, C. Изменить их значения, переместив содержимое A в B, B — в C, C — в A, и вывести новые значения переменных A, B, C.​

            //int A = 1, B = 2, C = 3;
            //Console.WriteLine($"{A} {B} {C}");
            //A ^= B;
            //B ^= A;
            //A ^= B;
            //A ^= C;
            //C ^= A;
            //A ^= C;
            //Console.WriteLine($"{A} {B} {C}");

            /*Дано двузначное число.Вывести вначале его левую цифру(десятки), а затем — его правую цифру(единицы).Для нахождения десятков использовать операцию деления нацело, для нахождения единиц — операцию взятия остатка от деления​*/

            //int a = 46;
            //int first = a / 10;
            //int second = a % 10;
            //Console.WriteLine(second + " " + first);

            //int a = new Random().Next(-100, 100);

            //if (a > 0) {
            //    Console.WriteLine($"{a} положительное число");
            //}
            //else{
            //    Console.WriteLine($"{a} отрицательное число");
            //}

            //Дано целое число.Если оно является положительным, то прибавить к нему 1; в противном случае не изменять его. Вывести полученное число.​
            //int a = new Random().Next(-100, 100);
            //Console.WriteLine(a);
            //if (a > 0) {
            //    a++;
            //}

            //Console.WriteLine(a);


            //Дано целое число.Если оно является положительным, то прибавить к нему 1; в противном случае вычесть из него 2.Вывести полученное число.​

            //int a = new Random().Next(-100, 100);
            //Console.WriteLine(a);
            //if (a > 0) {
            //    a++;
            //}
            //else {
            //    a -= 2;
            //}
            //Console.WriteLine(a);

            //Даны два числа.Вывести большее из них​
            //int a = new Random().Next(-100, 100);
            //int b = new Random().Next(-100, 100);

            //if (a > b) {
            //    Console.WriteLine(a);
            //}
            //else if (a == b) {
            //    Console.WriteLine($"{a} {b} Они равны");
            //}
            //else {
            //    Console.WriteLine(b);
            //}


            //Даны три числа.Найти наименьшее из них.​

            //int a = new Random().Next(-100, 100);
            //int b = new Random().Next(-100, 100);
            //int c = new Random().Next(-100, 100);

            //if (a < b & a < c) {
            //    Console.WriteLine($"a: {a}");
            //}
            //else if (b < a & b < c) {
            //    Console.WriteLine($"b: {b}");
            //}
            //else {
            //    Console.WriteLine($"c: {c}");
            //}


            //Console.WriteLine(Math.Min(a, Math.Min(b, c)));


            /*Даны две переменные целого типа: A и B.Если их значения не равны, то присвоить каждой переменной сумму этих значений, а если равны, то присвоить переменным нулевые значения.Вывести новые значения переменных A и B.​*/

            //int a = new Random().Next(-100, 100);
            //int b = new Random().Next(-100, 100);

            //if (a != b) {
            //    a = b = a + b;
            //}
            //else {
            //    a = b = 0;
            //}
            //CultureInfo culture = CultureInfo.CurrentCulture;

            //CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);

            //foreach (var item in cultures) {
            //    Console.WriteLine($"{item.EnglishName} | {item.ToString()}");
            //}

            //Console.WriteLine(cultures.Length);
            //Console.WriteLine(culture);

            //switch (culture.ToString()) {
            //    case "uk-UA":
            //        Console.WriteLine("Перевод на украинский");
            //        break;
            //    case "ru-RU":
            //        Console.WriteLine("Перевод на русский");
            //        break;
            //    case "en-US":
            //    case "en-GB":
            //        Console.WriteLine("Translate to English");
            //        break;
            //    default:
            //        Console.WriteLine("Translate to English");
            //        break;
            //}

            int a = new Random().Next(-100, 100);
            int b = new Random().Next(-100, 100);

            int min = (a < b) ? a : b;

        }
    }
}
