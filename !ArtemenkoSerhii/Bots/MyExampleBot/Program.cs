﻿/*
 Что может бот?
 - рассылка новостей, погода, курсы валют
 - взаимодействие со сторонними сервисами: Gmail, YouTube (API)
 - платежи, интернет-магазины, PrivatBot
 - игры
 - чаты, антиспамы, фильтры, опросники
 
 Чем отличается бот от аккаунта
 - нет привязки к мобильному телефону
 - имя бота заканчивается на "****bot"
 - бот не может начать диалог первым
 - бот не имеет онлайн статуса и время последнего посещения
 - бот имеет доступ только (ели программист не написал обратного) к своим сообщениям и сообщениям, где он упоминается 

 Как взаимодействовать с ботом?
 - можно написать напрямую @name_bot, либо упамянуть имя в чате
 - бот воспринимает как обычные команды, так и спец. команды /start
 - кнопки

 Как работает бот?
 - Все сообщения хранятся на серверах telegrama, пока бот не прочитает

 Как взаимодейстовать с telegram?
 - на сторне telegram есть сервис, который отвечает за доставку и отправку сообщений
 - сам бот на "нашей" стороне
    -- через API по протоколу HTTPS
    -- WebHooks


Http Long Pooling
 mybot.***.asurewebsite.microsft.com:1150
 */

using System;
using Telegram.Bot;

namespace MyExampleBot {
    class Program {
        private static ITelegramBotClient botClient;

        static void Main(string[] args) {
            botClient = new TelegramBotClient("957086041:AAG8c2lH6taUu11nbUkNg7Zba4xEEy2PwNY") { Timeout = TimeSpan.FromSeconds(10)};

            //var me = botClient.GetMeAsync().Result;

            //Console.WriteLine($"Bot id: {me.Id}, Bot Name: {me.FirstName}");

            botClient.OnMessage += BotClient_OnMessage;
            botClient.StartReceiving();

            Console.ReadLine();
        }

        private async static void BotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e) {
            var text = e?.Message?.Text;

            if (text == null) return;

            Console.WriteLine($"Recives text: {text} in chat {e.Message.Chat.Id}");

            await botClient.SendTextMessageAsync(
                chatId: e.Message.Chat,
                text: $"You said: \'{text}\'"
                ).ConfigureAwait(false);
        }
    }
}
