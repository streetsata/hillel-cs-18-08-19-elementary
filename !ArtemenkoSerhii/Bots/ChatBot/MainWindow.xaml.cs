﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telegram.Bot;
using System.IO;
using System.Diagnostics;

namespace ChatBot {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        ObservableCollection<TelegramUser> Users;
        TelegramBotClient botClient;


        public MainWindow() {
            InitializeComponent();
            InitBot();
        }

        private void InitBot() {
            Users = new ObservableCollection<TelegramUser>();
            usersList.ItemsSource = Users;

            string token = "865430424:AAHjCuC3gLmVa-F3hF6thu6Qv3-I2WcN6zg";

            botClient = new TelegramBotClient(token) { Timeout = TimeSpan.FromSeconds(5) };

            botClient.OnMessage += BotClient_OnMessage;
            botClient.StartReceiving();

            //var vvv = await botClient.GetChatMemberAsync(423395108, this.botClient.BotId);
            BtnSendMsg.Click += SendMsg;
            txtTBSendMsg.KeyDown += (s, e) => { if (e.Key == Key.Return) { SendMsg(s, e); } };

        }

        private async void SendMsg(object sender, RoutedEventArgs e) {
            var concreteUser = Users[Users.IndexOf(usersList.SelectedItem as TelegramUser)];

            if (string.IsNullOrEmpty(txtTBSendMsg.Text) || string.IsNullOrWhiteSpace(txtTBSendMsg.Text))
                txtTBSendMsg.Text = "Chat support says nothing";
            else {

                string responseMsg = $"Support Chat: {txtTBSendMsg.Text}";

                concreteUser.AddMessage(responseMsg);

                await botClient.SendTextMessageAsync(concreteUser.ID, txtTBSendMsg.Text);

                string logText = $"{DateTime.Now}: {concreteUser.ID} >> {responseMsg}";
                File.AppendAllText("data.log", logText);

                txtTBSendMsg.Text = string.Empty;
            }
        }

        private void BotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e) {
            string msg = $"{DateTime.Now}: {e.Message.Chat.FirstName} {e.Message.Chat.Id} {e.Message.Text}";

            File.AppendAllText("data.log", $"{msg}\n");

            Debug.WriteLine(msg);

            this.Dispatcher.Invoke(() => {
                var person = new TelegramUser(e.Message.Chat.FirstName, e.Message.Chat.Id);

                if (!Users.Contains(person))
                    Users.Add(person);

                Users[Users.IndexOf(person)].AddMessage($"{person.Nick}: {e.Message.Text}");
            });


        }
    }
}
