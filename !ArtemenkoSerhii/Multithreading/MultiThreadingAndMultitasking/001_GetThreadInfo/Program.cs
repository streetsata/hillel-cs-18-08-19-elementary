﻿using System;
using System.Threading;

namespace _001_GetThreadInfo
{

    class Program
    {
        //class Params
        //{
        //    public int Id { get; private set; }
        //    public int PersonID { get; private set; }

        //    public Params(int id, int pID)
        //    {
        //        Id = id;
        //        PersonID = pID;
        //    }
        //}

        //static void Add(object? o)
        //{
        //    if (o is Params)
        //    {
        //        Params p = (Params)o;
        //        Console.WriteLine(p.Id + " " + p.PersonID);
        //    }
        //}
        static void Main(string[] args)
        {
            //{
            //    ParameterizedThreadStart parameterizedThreadStart = new ParameterizedThreadStart(Add);
            //    Thread thread = new Thread(parameterizedThreadStart);
            //    thread.Start(new Params(10, 20));
            //}

            #region simple Thread
            //{
            //    //Thread thread = Thread.CurrentThread;
            //    //thread.Name = "Main";

            //    //Console.WriteLine(thread.Name);
            //    //Console.WriteLine(thread.IsBackground);
            //    //Console.WriteLine(thread.Priority);
            //    //Console.WriteLine(thread.ManagedThreadId);
            //    //Console.WriteLine(thread.ThreadState);
            //}
            //{
            //    //Console.WriteLine(Environment.ProcessorCount);
            //}
            //{
            //    ThreadStart threadStart = new ThreadStart(Func1);

            //    Thread t1 = new Thread(threadStart);
            //    t1.Start();
            //    t1.IsBackground = true;

            //    Thread.Sleep(1000);
            //    Console.WriteLine("Main thread was ended");
            //}



            //static void Func1()
            //{
            //    for (int i = 0; i < 100000; i++)
            //    {
            //        Thread.Sleep(500);
            //        Console.WriteLine(new string('+', 5));
            //    }
            //}
            #endregion

            Console.WriteLine("Sync call");
            int res = DelegateThread(1, 2000);
            Console.WriteLine("Sync res " + res);
            Console.WriteLine("Async call");

            MyDelegate my = DelegateThread;

            IAsyncResult asyncResult = my.BeginInvoke(1, 5000, null, null);

            while (!asyncResult.IsCompleted) 
            {
                Console.Write(".");
                Thread.Sleep(50);
            }

            int resAsync = my.EndInvoke(asyncResult);

        }

        delegate int MyDelegate(int data, int time);
        static int DelegateThread(int data, int time)
        {
            Console.WriteLine("DelegateThread was started");
            Console.WriteLine($"Thread ID {Environment.CurrentManagedThreadId}");
            Thread.Sleep(time);
            Console.WriteLine("DelegateThread was ended");

            return ++data;
        }
    }
}
