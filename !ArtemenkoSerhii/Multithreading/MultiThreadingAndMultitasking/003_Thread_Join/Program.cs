﻿using System;
using System.Threading;

namespace _003_Thread_Join
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Main Thread # {Thread.CurrentThread.GetHashCode()}");
            WriteChar("1", 80, ConsoleColor.Green);

            var t2 = new Thread(Method);
            t2.Start();
            t2.Join();

            WriteChar("1", 80, ConsoleColor.Green);

            Console.WriteLine("Main Thread was ended");
        }

        private static void Method()
        {
            Console.WriteLine($"Second Thread # {Thread.CurrentThread.GetHashCode()}");
            WriteChar("2", 80, ConsoleColor.Blue);

            var t3 = new Thread(Method2);
            t3.Start();
            t3.Join();

            Console.WriteLine("Second Thread was ended");
        }

        private static void Method2()
        {
            Console.WriteLine($"Third Thread # {Thread.CurrentThread.GetHashCode()}");
            WriteChar("3", 80, ConsoleColor.Yellow);

            Console.WriteLine("Third Thread was ended");
        }

        private static void WriteChar(string v1, int v2, ConsoleColor color)
        {
            Console.ForegroundColor = color;

            for (int i = 0; i < v2; i++)
            {
                Thread.Sleep(20);
                Console.Write(v1);
            }
        }
    }
}
