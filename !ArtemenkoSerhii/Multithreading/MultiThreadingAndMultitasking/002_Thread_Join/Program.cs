﻿using System;
using System.Threading;

namespace _002_Thread_Join
{
    class Program
    {
        public static int counter;

        static void Main(string[] args)
        {
            var t = new Thread(Method);
            t.Start();
            t.Join();

            Console.WriteLine("Main Thread was ended");
        }

        private static void Method()
        {
            if (counter < 10)
            {
                Thread.Sleep(500);
                counter++;
                Console.WriteLine(counter + " Start   " + Thread.CurrentThread.GetHashCode());
                var t2 = new Thread(Method);
                t2.Start();
                t2.Join();
            }

            Console.WriteLine($"Thread {Thread.CurrentThread.GetHashCode()} was ended");
        }
    }
}
