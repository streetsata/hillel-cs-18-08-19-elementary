﻿using System;
using System.Threading;

namespace _Monitor
{
    // Критическая секция
    // Monitor.Enter() блокирует блок кода так, что его может использовать только текущий поток. При этом все остальные потоки ждут, пока текущий поток закончит работу. Monitor.Exit();
    // Объект блокировки

    class Program
    {
        object block = new object();  // Объект блокировки

        void Foo()
        {
            lock (block)
            {
                int hash = Thread.CurrentThread.GetHashCode();
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine($"Thread # {hash} step: {i}");
                    Thread.Sleep(100);
                }
                Console.WriteLine(new string('-', 30));
            }
            //Monitor.Exit(block);
        }


        static void Main(string[] args)
        {
            Program p = new Program();

            for (int i = 0; i < 3; i++)
            {
                new Thread(p.Foo).Start();
            }

            Console.ReadLine();
        }
    }
}
