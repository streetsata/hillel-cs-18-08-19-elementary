﻿using System;
using System.Threading;

namespace Priority
{
    class ThreadWork {
        public Thread RunThread;
        public readonly ConsoleColor Color;
        //[ThreadStatic]
        private static bool stop;
    
        public int Count { get; set; }

        public ThreadWork(string name, ConsoleColor color)
        {
            RunThread = new Thread(Run) { Name = name };
            this.Color = color;
            Console.ForegroundColor = this.Color;
            Console.WriteLine($"Thread {RunThread.Name} was started");

        }
        
        private void Run()
        {
            do
            {
                Count++;
            } while (stop == false && Count < 200000000);
            stop = true;
        }

        public void BeginInvoke() { RunThread.Start(); }
        public void EndInvoke() { RunThread.Join(); }

        public ThreadPriority Priority { set => RunThread.Priority = value; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var work1 = new ThreadWork("High priority", ConsoleColor.Green);
            var work2 = new ThreadWork("Low priority", ConsoleColor.Red);

            work1.Priority = ThreadPriority.Highest;
            work2.Priority = ThreadPriority.Lowest;

            work2.BeginInvoke();
            work1.BeginInvoke();

            work1.EndInvoke();
            work2.EndInvoke();

            Console.WriteLine();
            Console.WriteLine($"Tread {work1.RunThread.Name}, counter: {work1.Count}");
            Console.WriteLine($"Tread {work2.RunThread.Name}, counter: {work2.Count}");

            Console.ReadLine();
        }
    }
}
