﻿using System;
using System.Threading;

namespace INterlocked
{
    class Program
    {
        static long counter;
        static readonly Random random = new Random();

        static void Foo() 
        {
            //counter++;
            Interlocked.Increment(ref counter);
            try
            {
                int time = random.Next(500, 1500);
                Thread.Sleep(time);
            }
            finally
            {
                Interlocked.Decrement(ref counter);
                //counter--;
            }
        }

        static void Report()
        {
            while (true)
            {
                long number = Interlocked.Read(ref counter);

                Console.WriteLine($"{number} threds are active");
                Thread.Sleep(100);
            }
        }

        static void Main(string[] args)
        {
            var report = new Thread(Report) { IsBackground = true };
            report.Start();

            var treads = new Thread[150];

            for (int i = 0; i < 150; i++)
            {
                treads[i] = new Thread(Foo);
                treads[i].Start();
            }

            Thread.Sleep(15000);
        }
    }
}
