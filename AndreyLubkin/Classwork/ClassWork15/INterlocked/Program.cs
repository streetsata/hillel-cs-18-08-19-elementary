﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace INterlocked {
    class Program {
        static long counter;
        static readonly Random random = new Random();

        static void Work() {
            //counter++;
            Interlocked.Increment(ref counter);
            try {
                int time = random.Next(500, 1500);
                Thread.Sleep(time);
            }
            finally {
                //counter--;
                Interlocked.Decrement(ref counter);
            }
        }

        static void Report() {
            while(true) {
                long number = Interlocked.Read(ref counter);

                Console.WriteLine($"{number} threads are active");
                Thread.Sleep(100);
                //Console.Clear();
            }
        }

        static void Main(string[] args) {
            var report = new Thread(Report) { IsBackground = true };
            report.Start();

            var threads = new Thread[150];

            for(int i = 0; i < threads.Length; i++) {
                threads[i] = new Thread(Work);
                threads[i].Start();
            }

            //Thread.Sleep(15000);
            Console.ReadLine();
        }
    }
}
