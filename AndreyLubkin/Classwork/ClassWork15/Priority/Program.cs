﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Priority {
    class ThreadWork {
        public Thread RunThread;
        public readonly ConsoleColor Color;

        private static bool stop;

        public int Count { get; set; }

        public ThreadWork(string name, ConsoleColor color) {
            RunThread = new Thread(Run) { Name = name };
            this.Color = color;
            Console.ForegroundColor = this.Color;
            Console.WriteLine($"Thread {RunThread.Name} was started");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void Run() {
            do {
                Count++;
            } while(!stop&&Count < 200_000_000);
            stop = true;
        }

        public void BeginInvoke() { RunThread.Start(); }
        public void EndInvoke() { RunThread.Join(); }

        public ThreadPriority Priority { set => RunThread.Priority = value; }
    }
    class Program {
        static void Main(string[] args) {
            var work1 = new ThreadWork("High priority", ConsoleColor.Green);
            var work2 = new ThreadWork("Low priority", ConsoleColor.Red);

            work1.Priority = ThreadPriority.Highest;
            work2.Priority = ThreadPriority.Lowest;

            work2.BeginInvoke();
            work1.BeginInvoke();

            work1.EndInvoke();
            work2.EndInvoke();

            Console.WriteLine();
            Console.WriteLine($"Thread {work1.RunThread.Name}, counter: {work1.Count}");
            Console.WriteLine($"Thread {work2.RunThread.Name}, counter: {work2.Count}");

            Console.ReadLine();
        }
    }
}
