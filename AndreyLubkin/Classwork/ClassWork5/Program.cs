﻿using System;

namespace ClassWork5 {
    class Enemy {
        public string name;
        public int movementSpeed;
        public int health;

        protected string msg = "aaa";
        public void Info() {
            Console.WriteLine($"Name: {name}, Speed: {movementSpeed}");
        }
    }

    class MonsterEnemy :Enemy {
        public int impactForce;
        public MonsterEnemy(string name, int moveSpeed, int health) {
            base.name = name;
            base.movementSpeed = moveSpeed;
            base.health = health;
            msg = "b";
        }

        public void Attack() {
            Console.WriteLine($"{name} is attacked!");
        }
    }

    class HarmlessEnemy :Enemy {
        public bool isHungry;
        public void setHungry() {
            isHungry = true;
        }
        public void setNotHungry() {
            isHungry = false;
        }
    }
    /// <summary>
    /// //////////////////
    /// </summary>

    class BaseClass {
        protected int a;
        protected int b;
        public BaseClass(int a, int b) {
            this.a = a;
            this.b = b;
        }

        public virtual void Method() {
            Console.WriteLine("Hello from BaseClass.Method()");
        }
    }

    class DerivedClass :BaseClass {
        public int c;
        public DerivedClass(int a, int b, int c): base(a,b) {
            this.c = c;
        }
        public override void Method() {
            Console.WriteLine("Hello from DerivedClass.Method()");
        }
    }

    class Program {
        static void Main(string[] args) {
            //Monster monster = new Monster();
            DerivedClass derivedClass = new DerivedClass(1,2,3);
            derivedClass.Method();
        }
    }
}
