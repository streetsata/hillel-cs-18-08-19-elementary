﻿using System;
using System.Collections.Generic;

namespace _02_Delegates {
    public class Calc {
        private Dictionary<string, Func<double, double, double>> operations;

        public Calc() {
            operations = new Dictionary<string, Func<double, double, double>> {
                { "+", (x,y) => x+y },
                { "-", (x,y) => x-y },
                { "*", (x,y) => x*y },
                { "/", this.DoDivision }
            };
        }

        public double PerfomOperation(string op, double x, double y) {
            if(!operations.ContainsKey(op)) 
                throw new ArgumentException($"{op} is exists");
            
            return operations[op].Invoke(x, y);
        }

        public void DefineOperation(string op, Func<double,double,double> body) {
            if(operations.ContainsKey(op))
                throw new ArgumentException($"{op} already exists");
            operations.Add(op, body);
        }

        private double DoDivision(double x, double y) {
            return (y != 0) ? x / y : throw new DivideByZeroException();
        }
    }
}
