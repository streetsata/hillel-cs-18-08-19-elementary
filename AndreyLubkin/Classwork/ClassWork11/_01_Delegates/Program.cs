﻿using System;

namespace _01_Delegates {
    class Person {
        int a;

        public int ID { get; set; }
        public string Name { get; set; }
        public int A { get => a; set => a = value; }
    }
    class Program {
        delegate void MyDel();
        delegate void MyDel2(int i);
        delegate int MyDel3(int a, int b);

        Action action;
        //delegate void MyDel();

        Action<int> action2;
        //delegete void MyDel2(int i);

        Predicate<int> predicate;
        //delegete bool MyDel3<in T>(T obj);
        Func<int> func;
        //delegate int MyDel4();
        Func<int, string> func1;
        //delegate string MyDel5(int i);

        static void MyDelFunc() { Console.WriteLine("Hello"); }
        static void MyDelFunc2(int i) { Console.WriteLine(i); }
        static int MyDelFunc3(int a, int b) { return a + b; }
        static bool MyDelFunc4(int a) { return (a >= 0) ? true : false; }
        static string MyDelFunc5(int a) { return a.ToString(); }

        static void SomeFunc(Person person) { Console.WriteLine(person.Name); }

        delegate void Del();
        delegate void Del2();
        delegate void Del3();

        static void F1() { Console.WriteLine("F1"); }
        static void F2() { Console.WriteLine("F2"); }
        static void F3() { Console.WriteLine("F3"); }


        static void Main(string[] args) {

            Del del0 = null;
            Del del21 = F1;
            Del del31 = F2;
            del0 = del21 + del31;

            Delegate[] delegates = del0.GetInvocationList();
            foreach(var item in delegates) {
                item.DynamicInvoke();
            }
            for(int i = 0; i < delegates.Length; i++) {
                if(i == 1) delegates[i].DynamicInvoke();
            }
            delegates[0].DynamicInvoke();

            Person person = new Person() { ID = 1, Name = "Serg" };
            SomeFunc(person);
            SomeFunc(new Person() { ID = 2, Name = "Artem" });
            MyDel del = MyDelFunc;
            del += MyDelFunc;
            del.Invoke();

            MyDel2 del2 = MyDelFunc2;
            del2 += MyDelFunc2; 
            del2.Invoke(new Random().Next(10,50));

            MyDel3 del3 = MyDelFunc3;
            var result = del3.Invoke(new Random().Next(10, 50), new Random().Next(10, 50));

            Action action = MyDelFunc;
            action.Invoke();

            Action<int> action2 = MyDelFunc2;
            action2.Invoke(new Random().Next(10, 50));

            Predicate<int> predicate = MyDelFunc4;
            bool res = predicate.Invoke(new Random().Next(-10, 10));

            Func<int, string> func1 = MyDelFunc5;
            var str = func1.Invoke(15);
            Console.WriteLine(str);

            Func<int, int> myDelegate;
            myDelegate = delegate (int a) { return a * 2; }; // Лямбда-метод
            myDelegate = (x) => { return x * 2; }; // Лямбда-оператор
            myDelegate = x => x * 2; // Лямбда-выражение

            Action action1;
            action1 = () => {
                Console.WriteLine("Hello");
                Console.WriteLine("Hello");
                Console.WriteLine("Hello");
            };
        }
    }
}
