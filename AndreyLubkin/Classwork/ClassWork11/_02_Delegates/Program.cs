﻿using System;

namespace _02_Delegates {
    class Program {
        static void Main(string[] args) {
            Calc calc = new Calc();
            calc.DefineOperation("mod", (x, y) => x % y);
            var result = calc.PerfomOperation("+", 3, 2);
            Console.WriteLine(result);
            Console.WriteLine(calc.PerfomOperation("mod",7,4));
        }
    }
}
