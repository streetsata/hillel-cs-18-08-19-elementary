﻿using System;

namespace _03_Delegates {
    class Person {
        public event Action GoToSleep;
        public event EventHandler DoWork;
        public string Name { get; set; }

        public void TakeTime(DateTime now) {
            if(now.Hour >= 8)
                GoToSleep?.Invoke();
            else {
                var args = new EventArgs();
                DoWork?.Invoke(this,args);
            }
        }

    }

    //static byte Test() { return 1; }

        

    class Program {
        static void Main(string[] args) {
            

            int[] arr = null;
            int? a = null;
            a = 9;

            Person person = new Person() { Name = "Sergii" };

            person.GoToSleep += Person_GoToSleep_Handler;
            person.DoWork += Person_DoWork_Handler;

            person.TakeTime(DateTime.Parse("9/29/2019 21:04:01"));
            person.TakeTime(DateTime.Parse("9/29/2019 4:04:01"));

        }

        private static void Person_DoWork_Handler(object sender, EventArgs e) {
            if(sender is Person)
                Console.WriteLine($"{((Person)sender).Name} goes work");
        }

        private static void Person_GoToSleep_Handler() {
            Console.WriteLine("Human goes sleep");
        }
    }
}
