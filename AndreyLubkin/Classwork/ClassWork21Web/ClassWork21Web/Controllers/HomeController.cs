﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassWork21Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace ClassWork21Web.Controllers
{
    public class HomeController : Controller
    {
        BookShopContext db = new BookShopContext();
        public IActionResult Index()
        {
            IEnumerable<Book> books = db.Books;
            ViewBag.Books = books;

            return View();
        }
    }
}