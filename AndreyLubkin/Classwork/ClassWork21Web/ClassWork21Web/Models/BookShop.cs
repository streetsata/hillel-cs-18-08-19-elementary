﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClassWork21Web.Models {
    public class BookShopContext : DbContext {
        public DbSet<Book> Books { get; set; }
        public DbSet<Purchases> Purchases { get; set; }
    }
}
