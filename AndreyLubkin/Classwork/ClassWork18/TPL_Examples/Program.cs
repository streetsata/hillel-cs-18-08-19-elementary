﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TPL_Examples {
    class Program {
        static void MyTask() {
            Console.WriteLine($"MyTask() is runned. {Thread.CurrentThread.ManagedThreadId}");
            for(int i = 0; i < 10; i++) {
                Thread.Sleep(400);
                Console.WriteLine($"Count {i}");
            }
            Console.WriteLine($"MyTask was ended {Thread.CurrentThread.ManagedThreadId}");
        }
        static void ContinationTask(Task task) {
            Console.WriteLine($"ContinationTask() is runned. {Thread.CurrentThread.ManagedThreadId}");
            for(int i = 0; i < 5; i++) {
                Thread.Sleep(500);
                Console.WriteLine($"Count {i}");
            }
            Console.WriteLine($"Contination was ended {Thread.CurrentThread.ManagedThreadId}");
        }

        static bool Bool_MyTaskReturned() {
            return true;
        }

        static int Sum(object v) {
            int x = (int)v, sum = 0;
            for(; x > 0; x--) {
                sum += x;
            }

            Thread.Sleep(3000);
            return sum;
        }

        static void Main(string[] args) {

            Task<bool> task1 = Task<bool>.Factory.StartNew(Bool_MyTaskReturned);
            Console.WriteLine(task1.Result);

            Task<int> task2 = Task<int>.Factory.StartNew(Sum, 9);
            Console.WriteLine(task2.Result);

            //var action = new Action(MyTask);
            //var task = new Task(action);

            //var continationTask = new Action<Task>(ContinationTask);

            //Task taskContination = task.ContinueWith(ContinationTask);
            //task.Start();
            //task.Wait();

            for(int i = 0; i < 60; i++) {
                Console.Write(".");
                Thread.Sleep(100);
            }

            Console.WriteLine("Main() thread was ended");
            Console.ReadLine();
        }
    }
}
