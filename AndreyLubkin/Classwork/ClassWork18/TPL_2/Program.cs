﻿using System;
using System.Threading;
using System.Threading.Tasks;
namespace TPL_2 {
    class Program {
        static void MyTask(object st) {
            CancellationToken cancellationToken = (CancellationToken)st;

            cancellationToken.ThrowIfCancellationRequested();

            Console.WriteLine("MyTask was Running");

            for(int i = 0; i < 10; i++) {
                if(cancellationToken.IsCancellationRequested) {
                    Console.WriteLine("Cancel");
                    cancellationToken.ThrowIfCancellationRequested();
                }
                Thread.Sleep(500);
                Console.WriteLine($"Count {i}");
            }
            Console.WriteLine("MyTask() was ended");
        }
        static void Main(string[] args) {
            CancellationTokenSource tokenSource = new CancellationTokenSource();

            Task task = Task.Factory.StartNew(MyTask, tokenSource.Token);

            Thread.Sleep(2000);
            try {
                tokenSource.Cancel();
                task.Wait();
            }
            catch(Exception e) {
                if(task.IsCanceled) Console.WriteLine("Task Cancel");
                Console.WriteLine(e.InnerException.ToString());
            }
            finally {
                task.Dispose();
                tokenSource.Dispose();
            }

            Console.WriteLine("Main() was ended");
            Console.ReadLine();
        }
    }
}
