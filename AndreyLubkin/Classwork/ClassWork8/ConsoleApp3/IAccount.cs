﻿using System;

namespace ConsoleApp3 {
    public interface IAccount :IComparable<IAccount> {
        void PayInFunds(decimal amount);
        bool WithdrawFunds(decimal amount);
        decimal GetBalance();
    }
}
