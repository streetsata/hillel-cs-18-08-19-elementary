﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3 {
    class Enumerator :IEnumerator<int>, IEnumerable {
        int count;
        int limit;

        public Enumerator(int limit) {
            count = -1;
            this.limit = limit;
        }

        public int Current { get { return count; } }

        object IEnumerator.Current { get { return count; } }

        public void Dispose() {
            
        }

        public IEnumerator GetEnumerator() {
            return this;
        }

        public bool MoveNext() {
            if(++count == limit) return false;
            else return true;
        }

        public void Reset() {
            count = -1;
        }
    }
}
