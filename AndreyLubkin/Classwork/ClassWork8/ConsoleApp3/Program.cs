﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace ConsoleApp3 {
    class Program {
        static void Main(string[] args) {
            IAccount a = new BankAccaunt();
            a.PayInFunds(50);
            Console.WriteLine($"Pay in 50");
            a.PayInFunds(20);
            Console.WriteLine($"Pay in 20");

            if(a.WithdrawFunds(10)) {
                Console.WriteLine("Withdraw 10");
            }

            Console.WriteLine($"Account balance {a.GetBalance()}");

            List<IAccount> accounts = new List<IAccount>();
            Random random = new Random(1);

            for(int i = 0; i < 20; i++) {
                Thread.Sleep(10);
                IAccount account = new BankAccaunt(random.Next(0,10_000));
                accounts.Add(account);
            }

            accounts.Sort();

            foreach(IAccount account in accounts) {
                Console.WriteLine(account.GetBalance());
            }
            Console.WriteLine();
            // Enumerator
            var stringEnumerator = "Hello world".GetEnumerator();
            while(stringEnumerator.MoveNext()) {
                Console.WriteLine(stringEnumerator.Current);
            }
            Console.WriteLine();
            //using foreach

            foreach(char ch in "hello world") {
                Console.WriteLine(ch);
            }
        }
    }
}
