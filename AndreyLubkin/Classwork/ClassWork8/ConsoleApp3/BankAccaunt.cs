﻿using System;

namespace ConsoleApp3 {
    public class BankAccaunt :IAccount, IComparable<IAccount> {
        private decimal balance;
        public BankAccaunt() {
        }
        public BankAccaunt(decimal amount) {
            this.balance = amount;
        }

        public int CompareTo(IAccount obj) {
            if(obj == null) return 1;

            //IAccount account = obj as IAccount;
            // if(account == null) throw new ArgumentException();
            return this.balance.CompareTo(obj.GetBalance());
        }

        public decimal GetBalance() {
            return balance;
        }

        public void PayInFunds(decimal amount) {
            balance += amount;
        }

        public bool WithdrawFunds(decimal amount) {
            if(balance < amount) return false;
            else balance -= amount;
            return true;
        }
    }
}
