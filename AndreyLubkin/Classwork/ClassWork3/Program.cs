﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ClassWork {
    class MyStack<T> where T: struct {
        private T[] items = new T[100];
        int stackPointer = 0;

        public void Push(T item) {
            if(stackPointer == items.Length) throw new Exception("Stack is full");
            
            items[stackPointer++] = item;
        }

        public T Pop() {
            if(stackPointer<=0) throw new Exception("Stack is empty");
            return items[--stackPointer];
        }
    }

    class Enemy {
        public int x { get; private set; }
        public int y { get; private set; }
        public int Lives { get; private set; }
        public string id { get; set; }

        static Enemy() {

        }

        // Default constructor
        public Enemy() {
            x = 50;
            y = 50;
            Lives = 3;
            id = new Guid().ToString();
        }

        // User constructor
        public Enemy(int x, int y) {
            this.x = x;
            this.y = y; 
        }

        public Enemy(int x, int y, int lives) : this(x, y) {
            Lives = lives;
        }

        public override string ToString() {
            return $"X: {x}, Y: {y}, Lives: {Lives}";
        }
    }

    class Person {
        public byte Id { get; set; }
        public string Name { get; set; }
        public byte Age { get; set; }
    }

    class Program {
        static void Func(out int a, out int b) {
            a = 10;
            b = 6;
            // 0011
            // ^
            // 1010
            // 1010
            int temp = a;
            a = b;
            b = temp;
        }
        static void Main(string[] args) {
            Enemy enemy = new Enemy();
            Console.WriteLine(enemy);

            //ArrayList arrayList = new ArrayList() { 1, "dfs", true };

            MyStack<int> stack = new MyStack<int>();
            

            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(4);
            stack.Push(5);
            stack.Push(6);
            stack.Push(7);
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
        }
    }
}
