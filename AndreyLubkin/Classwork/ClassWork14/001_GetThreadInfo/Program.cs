﻿using System;
using System.Threading;
using System.Linq;

namespace _001_GetThreadInfo {
    class Program {

        static void Main(string[] args) {

            #region simple Thread
            //{
            //    Thread thread = Thread.CurrentThread;
            //    thread.Name = "Main";


            //    Console.WriteLine(thread.Name);
            //    Console.WriteLine(thread.IsBackground);
            //    Console.WriteLine(thread.Priority);
            //    Console.WriteLine(thread.ManagedThreadId);
            //    Console.WriteLine(thread.ThreadState);
            //}
            //Console.WriteLine();
            //{
            //    Console.WriteLine(Environment.CurrentManagedThreadId);
            //    Console.WriteLine(Environment.ProcessorCount);
            //}
            //Console.WriteLine();
            //{
            //    ThreadStart threadStart = new ThreadStart(Func1);

            //    Thread t1 = new Thread(threadStart);

            //    t1.Start();
            //    t1.IsBackground = true;

            //    Thread.Sleep(1000);
            //    Console.WriteLine("Main thread was ended");
            //}
            //void Func1() {
            //    for(int i = 0; i < 10000; i++) {
            //        Thread.Sleep(500);
            //        Console.WriteLine("From Secondary Thread");
            //    }

            //}
            #endregion

            Console.WriteLine("Sync call");
            int res = DelegateThread(1, 3000);
            Console.WriteLine("Sync res " + res);
            Console.WriteLine("Async call");

            MyDelegate my = DelegateThread;
            IAsyncResult asyncResult = my.BeginInvoke(1, 2000, null, null);

            while(!asyncResult.IsCompleted) {
                Console.Write(".");
                Thread.Sleep(50);
            }
            Console.WriteLine();

            int resAsync = my.EndInvoke(asyncResult);
            Console.WriteLine(resAsync);
            Console.ReadLine();
        }
        delegate int MyDelegate(int data, int time);
        static int DelegateThread(int data, int time) {
            Console.WriteLine("DelegateThread was started");
            Console.WriteLine($"Thread ID {Environment.CurrentManagedThreadId}");
            Thread.Sleep(time);
            Console.WriteLine("DelegateThread was ended");
            return ++data;
        }
    }
}
