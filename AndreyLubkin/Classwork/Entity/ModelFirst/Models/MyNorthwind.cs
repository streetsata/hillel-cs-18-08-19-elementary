﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelFirst.Models {
    class MyNorthwind :DbContext {
            public MyNorthwind()
                : base("DbConnection") { }
            public DbSet<Product> Products { get; set; }
    }
}
