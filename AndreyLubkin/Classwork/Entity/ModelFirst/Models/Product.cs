﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelFirst.Models {
    public class Product {
        public int ProductID { get; set; }
        [Required]
        public string ProductName { get; set; }
        public decimal? Cost { get; set; }
        public short Stock { get; set; }
        [Required]
        public bool Discontinued { get; set; }
        public int CategoryID { get; set; }
    }
}
