﻿using ModelFirst.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModelFirst {
    public partial class Form1 :Form {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {

            dataGridView1.DataSource = GetProducts();

        }

        private List<Product> GetProducts() {
            using(var db = new MyNorthwind()) {
                var query = db.Products;
                var listProducts = query.ToList();
                return listProducts;
            }
        }
    }
}
