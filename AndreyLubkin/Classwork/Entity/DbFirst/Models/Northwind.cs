﻿//using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFirst.Models {
    public class Northwind :DbContext {
        public Northwind()
            : base("DbConnection") { }
        public DbSet<Product> Products { get; set; }
    }
}
