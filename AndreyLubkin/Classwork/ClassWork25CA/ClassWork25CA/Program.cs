﻿using ClassWork25CA.Models;
using LoggerConsole;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace ClassWork25CA {
    class Program {
        static void Main(string[] args) {
            //QueriesCategories();
            QueriesProducts();
            //QueriesWithLike();
            //AddProduct(6, "MC", 129M, false);
        }

        private static void AddProduct(int categoryID, string productName, decimal? price, bool isDiscontinued) {
            using(var db = new Northwind()) {
                var newProduct = new Product() {
                    CategoryID = categoryID,
                    ProductName = productName,
                    Cost = price,
                    Discontinued = isDiscontinued
                };
                db.Products.Add(newProduct);
                db.SaveChanges();
            }
        }

        private static void QueriesWithLike() {
            using(var db = new Northwind()) {
                var loggerFactory = db.GetService<ILoggerFactory>();
                loggerFactory.AddProvider(new ConsoleLoggerProvider());
                Console.Write("Enter part of query: ");
                string input = Console.ReadLine();

                IQueryable<Product> products = db.Products
                    .Where(p => EF.Functions.Like(p.ProductName, $"%{input}%"));

                foreach(var item in products) {
                    Console.WriteLine($"{item.ProductName} has {item.Stock}. Discounted? {item.Discontinued}");
                }
            }
        }

        private static void QueriesProducts() {
            using(var db = new Northwind()) {
                string input;
                decimal price;
                do {
                    Console.Write("Enter a product price: ");
                    input = Console.ReadLine();

                } while(!decimal.TryParse(input, out price));

                IQueryable<Product> products = db.Products;
                    //.Where(p => p.Cost > price)
                    //.OrderByDescending(p => p.Cost);
                foreach(var item in products) {
                    Console.WriteLine($"{item.ProductName} {item.Cost} {item.CategoryID}");
                }
            }
        }

        private static void QueriesCategories() {
            using(var db = new Northwind()) {
                var loggerFactory = db.GetService<ILoggerFactory>();
                loggerFactory.AddProvider(new ConsoleLoggerProvider());
                IQueryable<Category> categories = db.Categories.Include(c => c.Products);
                foreach(var item in categories) {
                    Console.WriteLine($"{item.CategoryName} has {item.Products.Count} products");
                }
            }
        }
    }
}
