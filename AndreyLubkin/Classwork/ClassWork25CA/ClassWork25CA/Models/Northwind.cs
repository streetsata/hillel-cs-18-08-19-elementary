﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassWork25CA.Models {
    public class Northwind : DbContext{
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            optionsBuilder.UseSqlServer(
                @"Data Source=(LocalDb)\MSSQLLocalDB;"+
                "Initial Catalog=Northwind;"+
                "Integrated Security=true;"+
                "MultipleActiveResultSets=true;"
                );
        }
    }
}
//Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=true;MultipleActiveResultSets=true;
//Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-MvcMovie;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\Movies.mdf