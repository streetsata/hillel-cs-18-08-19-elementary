using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Web
{
    public class MyStartup
    {
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Middleware
            app.UseDefaultFiles();
            app.UseStaticFiles();
            // app.UseEndpoints(endpoints =>
            //     {
            //         endpoints.MapControllerRoute(
            //             name: "default",
            //             pattern: "{controller=Home}/{action=GetData}/{id?}");
            //     });
        }
    }
}