﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ClassWork24.Controllers {
    public class FsController :Controller {
        public IActionResult FsInfo() {
            return View();
        }

        public IActionResult DrivesInfo() {
            return View();
        }

        public IActionResult Directories() {
            return View();
        }

        public IActionResult DeleteDirectories() {
            string userFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string[] customFolder = new string[] { userFolder,"Code","NewFolder" };

            string dir = System.IO.Path.Combine(customFolder);
            System.IO.Directory.Delete(dir, recursive: true);

            return RedirectToAction("Directories");
        }
    }
}