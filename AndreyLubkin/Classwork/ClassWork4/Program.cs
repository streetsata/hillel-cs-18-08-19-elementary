﻿using System;

namespace ClassWork4 {
    class Program {
        class IntArrayWrapper {
            private int[] arr = new int[100];

            public int this[int i] {
                get { return arr[i]; }
                set { arr[i] = value; }
            }
        }

        class NamedArray {
            private int[] arr = new int[100];
            
            public int this[string name] {
                get {
                    switch(name) {
                        case "zero":
                            return arr[0];
                        case "one":
                            return arr[1];
                        default: return -1;
                    }

                }
                set {
                    switch(name) {
                        case "zero":
                            arr[0] = value;
                            break;
                        case "one":
                            arr[1] = value;
                            break;
                        default: throw new Exception();
                    }
                }
            }
        }

        static void Main(string[] args) {
            //IntArrayWrapper x = new IntArrayWrapper();
            //x[0] = 99;
            //Console.WriteLine(x[0]);

            //boxing unboxing
            {
                object o = 99;
                int oVal = (int)o;
            }
            //Console.WriteLine("Hello World!");
        }
    }
}
