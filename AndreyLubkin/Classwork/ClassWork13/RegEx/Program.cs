﻿using System;
using System.Net;
using System.Text.RegularExpressions;

/*
    METAСИМВОЛЫ - это символы для составления шаблона поиска в подстроке
    
    \d - Определяет символ цифр
    \D - Любой символ, который не является цифрой
    \w - Любой символ цифры, буквы, знак подчеркивания
    \W - Любой символ, который не является цифрой, буквой, знаком подчеркивания
    \s - Любой пробельный (непечатный) символ [Tab,Enter,Space]
    \S - Любой символ кроме пробельных символов
    . - Любой символ, кроме символа новой строки
    [0-5][a-zA-Z] - Диапазон
    
    КВАНТИФИКАТОРЫ - символы которые определяют где и сколько раз необходимое вхождение символа может встречаться

    ^ - в начале строки
    $ - в конце строки
    + - одно и более вхождений шаблонов в строке
    * - может встречатся, а может и нет
*/

namespace RegEx {
    class Program {
        static void Main(string[] args) {
            {
                //string pattern = @"\d";
                //Regex regex = new Regex(pattern);
                //bool result = regex.IsMatch("ss1");
            }
            {
                //string pattern = @"^\d*\D+\d+$";
                //var regex = new Regex(pattern);
                //string[] array = { "test","123test","test123","123", "123test123" };
                //foreach(var item in array) {
                //    Console.WriteLine(
                //            regex.IsMatch(item)
                //            ? $"Строка {item} соответствует шаблону {pattern}"
                //            : $"Строка {item} НЕ соответствует шаблону {pattern}"
                //        );
                //}
            }

            {
                //string line = "Какой-то текст который от другого текста ничем не отличается";
                //MatchCollection matches = Regex.Matches(line, "текст");
                //Console.WriteLine(matches.Count);
            }
            {
                //string line = "Какой-то текст   \n       который от другого текста               ничем не отличается";
                //line = new Regex(@"\s").Replace(line, "789");
            }
            {
                string line = string.Empty;
                using (WebClient wc = new WebClient()) {
                    line = wc.DownloadString("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange");
                    line = new Regex(@"\s+").Replace(line, " ");
                    Match match = Regex.Match(line, @"<txt>Долар США</txt> <rate>(.*?)</rate> <cc>(.*?)</cc>",RegexOptions.Singleline);
                    Console.WriteLine(match.Groups[1].Value);
                }
            }
        }
    }
}
