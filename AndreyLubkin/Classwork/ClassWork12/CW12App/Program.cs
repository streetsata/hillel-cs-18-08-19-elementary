﻿using System;
using HW12Class_01;

namespace CW12App {
    class Program {
        public delegate void EventDelegate();
        class MyClass {
            public event EventDelegate myEvent = null;
            public event EventDelegate my {
                add { my += value; }
                remove { my -= value; }
            }

            public void InvokeEvent() { myEvent.Invoke(); }
        }
        static void Main(string[] args) {
            //MyClass instance = new MyClass();

            //instance.myEvent += Handler1;
            //instance.myEvent += Handler2;

            //instance.InvokeEvent();

            //instance.myEvent -= Handler2;

            //instance.InvokeEvent();
            Keybord keybord = new Keybord();
            keybord.PressKeyA += Keybord_PressKeyA;

            keybord.Start();
        }

        private static void Keybord_PressKeyA() {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine();
            Console.WriteLine("   x   ");
            Console.WriteLine("  x x  ");
            Console.WriteLine(" x   x ");
            Console.WriteLine("xxxxxxx");
            Console.WriteLine("x     x");
            Console.ForegroundColor = ConsoleColor.Gray;


        }

        private static void Handler2() {
            Console.WriteLine("Handler2");
        }

        private static void Handler1() {
            Console.WriteLine("Handler1");
        }
    }
}
