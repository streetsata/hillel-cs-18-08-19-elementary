﻿using System;

namespace HW12Class_01 {
    public delegate void PressKeyEventHandler();
    public class Keybord {
        public event PressKeyEventHandler PressKeyA = null;

        public void PressKeyAEvent() {
            if(PressKeyA != null) {
                { PressKeyA.Invoke(); }
            }
        }

        public void Start() {
            while(true) {
                string s = Console.ReadLine();
                s = s.ToLower();
                switch(s) {
                    case "a":
                        PressKeyAEvent();
                        break;
                    case "exit":
                        goto Exit;
                    default:
                        Console.WriteLine("Нет обработчкика нажатия");
                        break;
                }
            }
            Exit:
            Console.WriteLine("Exit!");
        }
    }

    public delegate void EventAbstractDelegate();

    interface IInterface {
        event EventAbstractDelegate myAbstractEvent;
    }

    public class BaseClass :IInterface {
        public virtual event EventAbstractDelegate myAbstractEvent;
        public void InvokeEvent() {
            myAbstractEvent.Invoke();
        }
    }

    public class DerivedClass :BaseClass {
        public override event EventAbstractDelegate myAbstractEvent {
            add { base.myAbstractEvent += value; }
            remove { base.myAbstractEvent -= value; }
        }
    }
}
