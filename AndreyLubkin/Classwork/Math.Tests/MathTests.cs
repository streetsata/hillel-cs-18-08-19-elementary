﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Math.Tests {
    [TestFixture]
    class MathTests {
        //MyMath _math;
        //[SetUp]
        //void Func() {
        //    _math = new MyMath();
        //}
        [Test]
        public void Add_WhenCalled_ReturnSumOfArgs() {
            // Arrange
            MyMath math = new MyMath();
            int expected = 12;
            int a = 5, b = 7;
            // Act
            var result = math.Add(a, b);
            //Asert
            Assert.AreEqual(result, expected);
            //Assert.That(result, Is.EqualTo(expected));
            //Assert.That(result, Is.Not.Null);
        }
        [Test]
        [TestCase(2,1,2)]
        [TestCase(1,2,2)]
        [TestCase(2,2,2)]
        public void Max_WhenCalled_ReturnedTheGreaterArg(int a, int b, int expectedResult) {
            MyMath math = new MyMath();
            var result = math.Max(a, b);
            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}
