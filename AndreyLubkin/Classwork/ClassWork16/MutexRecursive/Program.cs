﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MutexRecursive {
    class SomeClass {
        private Mutex mutex = new Mutex();

        public void Method1() {
            mutex.WaitOne();
            {
                Console.WriteLine("Method1 was started " + Thread.CurrentThread.ManagedThreadId);
                Method2();
            }
            mutex.ReleaseMutex();
            Console.WriteLine("Method1 was ended " + Thread.CurrentThread.ManagedThreadId);
        }
        public void Method2() {
            mutex.WaitOne();
            {
                Console.WriteLine("Method2 was started " + Thread.CurrentThread.ManagedThreadId);
            }
            mutex.ReleaseMutex();
            Console.WriteLine("Method2 was ended " + Thread.CurrentThread.ManagedThreadId);
        }
    }
    class Program {
        static void Main(string[] args) {
        }
    }
}
