﻿using System;
using System.Threading;

namespace Mutex_ {
    class Program {
        private static readonly Mutex mutex = new Mutex(false, "MutexSample(2DAC0E8B-A7D0-45EB-A3CF-F543C45E0C43)");
        static void Main(string[] args) {
            
            var threads = new Thread[5];

            for(int i = 0; i < threads.Length; i++) {
                threads[i] = new Thread(Foo) { Name = i.ToString() };
                threads[i].Start();
            }

            Console.ReadLine();
        }

        static void Foo() {
            bool myMotex = mutex.WaitOne();
            {
                Console.WriteLine($"Поток {Thread.CurrentThread.Name} зашел в защищенную область");
                Thread.Sleep(4000);
                Console.WriteLine($"Поток {Thread.CurrentThread.Name} покинул в защищенную область");
            }
            mutex.ReleaseMutex();
        }
    }
}
