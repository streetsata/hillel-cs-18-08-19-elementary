﻿using System;
using System.Threading;

namespace Thread_Pool {
    class Program {
        static void Main(string[] args) {
            ShowThreadInfo();
            Console.WriteLine(new string('-',20));
            Console.WriteLine();
            ThreadPool.QueueUserWorkItem(new WaitCallback(Task1));
            ShowThreadInfo();
            Console.WriteLine(new string('-', 20));
            Console.WriteLine();
            Thread.Sleep(1000);
            ThreadPool.QueueUserWorkItem(Task2);
            ShowThreadInfo();
            Thread.Sleep(1000);
            Console.WriteLine(new string('-', 20));
            Console.WriteLine();
            Console.ReadLine();
        }

        static void Task1(object state) {
            Thread.CurrentThread.Name = "1";
            Console.WriteLine($"Name: {Thread.CurrentThread.Name}, ManagedID {Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(2000);
        }
        static void Task2(object state) {
            Thread.CurrentThread.Name = "2";
            Console.WriteLine($"Name: {Thread.CurrentThread.Name}, ManagedID {Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(200);
        }

        static void ShowThreadInfo() {
            int availableWorkThreads, availableIOThreads, maxThreads, maxIOThreads;
            ThreadPool.GetAvailableThreads(out availableWorkThreads, out availableIOThreads);
            ThreadPool.GetMaxThreads(out maxThreads, out maxIOThreads);
            Console.WriteLine($"Work Threads in pool:{availableWorkThreads} from {maxThreads}");
            Console.WriteLine($"Work Threads in pool:{availableIOThreads} from {maxIOThreads}");

        }
    }
}
