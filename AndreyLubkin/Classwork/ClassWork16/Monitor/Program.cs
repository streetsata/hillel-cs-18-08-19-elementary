﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Monitor_1 {

    // Критическая секция
    /* 
    Monitor.Enter(); 
    блокирует блок кода так, что его может использовать только текущий поток. 
    При этом все остальные потоки ждут пока текщий закончит работу.
    Monitor.Exit();
    */
    // Объект блокировки

    class Program {
        static object block = new object(); // Объект блокировки
        void Foo() {
            lock(block) {
                int hash = Thread.CurrentThread.GetHashCode();
                for(int i = 0; i < 10; i++) {
                    Console.WriteLine($"Thread # {hash} step {i}");
                }
                Console.WriteLine(new string('-', 30));
            }
            //Monitor.Exit(block);
        }

        static void Foo2() {
            for(int i = 0; i < 20000; i++) {
                lock(block) {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(new string(' ', 10)+"Secondary");
                    Console.ForegroundColor = ConsoleColor.White;
                    Thread.Sleep(10);
                }
            }
        }

        static void Main(string[] args) {
            //Program p = new Program();
            //for(int i = 0; i < 3; i++) {
            //    new Thread(p.Foo).Start();
            //    Thread.Sleep(500);
            //}
            Thread thread = new Thread(Foo2);
            thread.Start();

            Thread.Sleep(1);

            for(int i = 0; i < 20000; i++) {
                lock(block) {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Primary");
                    Console.ForegroundColor = ConsoleColor.White;
                    Thread.Sleep(10);
                }
            }

            Console.ReadLine();
        }
    }
}
