﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FunWithParallel {
    class Program {
        static void Task1() {
            Console.WriteLine("Task1() was runned");
            for(int i = 0; i < 80; i++) {
                Thread.Sleep(10);
                Console.Write("+");
            }
            Console.WriteLine("Task1() was ended");
        }
        static void Task2() {
            Console.WriteLine("Task2() was runned");
            for(int i = 0; i < 80; i++) {
                Thread.Sleep(10);
                Console.Write("-");
            }
            Console.WriteLine("Task2() was ended");
        }
        static void Main(string[] args) {
            Console.WriteLine("Main Thread was started");

            Console.WriteLine($"Real core(s): {Environment.ProcessorCount}");

            ParallelOptions options = new ParallelOptions() {
                MaxDegreeOfParallelism = Environment.ProcessorCount == 8 ? 8 : 1
            };
            Console.ReadKey();

            Parallel.Invoke(options, Task1, Task2, Task1, Task2, Task1, Task2, Task1, () => Console.WriteLine("Hello world!"));
            Console.WriteLine("Main thread was ended");
            Console.ReadLine();
        }
    }
}
