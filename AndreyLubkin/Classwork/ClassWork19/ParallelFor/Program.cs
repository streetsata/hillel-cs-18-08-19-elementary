﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace ParallelFor {
    class Program {
        static List<int> data;
        static void Foo(int i) {
            data[i] = data[i] / 10;
            if(data[i] < 10_000) data[i] = 0;
            if(data[i] >= 10_000) data[i] = 100;
            if(data[i] > 20_000) data[i] = 200;
            if(data[i] > 30_000) data[i] = 300;

        }
        static void Main(string[] args) {
            data = new List<int>();
            Stopwatch timer = new Stopwatch();
            timer.Start();
            for(int i = 0; i < 99_000_000; i++) {
                data.Add(i);
            }
            timer.Stop();
            Console.WriteLine($"Ordinary for: \t{timer.ElapsedTicks}");
            timer.Reset();

            timer.Start();
            Parallel.For(0, 99_000_000, (i) => {
                data.Add(i);
                //data[i] = i;
            });
            timer.Stop();
            Console.WriteLine($"Prallel for: \t{timer.ElapsedTicks}");
            Console.WriteLine("Main thread was ended");
            Console.ReadLine();

        }
    }
}
