﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Async {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow :Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void BtnDownload_Click(object sender, RoutedEventArgs e) {
            txt.Text = "Strarting sync download\n";

            var req = WebRequest.Create("https://www.microsoft.com");

            req.Timeout = 2000;
            req.Method = "GET";

            try {
                var res = req.GetResponse();
                txt.Text += "Sync completed\n";
                string headerText = res.Headers.ToString();
                txt.Text += headerText;
            }
            catch(WebException ex) {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
