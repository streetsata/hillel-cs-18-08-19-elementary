﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelForEach {
    
    class Program {
        static void ShowData(int value, ParallelLoopState state) {
            if(value < 0) {
                state.Break();
                Console.WriteLine($"Value {value}");
            }
        }
        static void Main(string[] args) {
            var data = new int[100_000_000];

            for(int i = 0; i < data.Length; i++) {
                data[i] = i;
            }
            //data[1000] = -1;

            ParallelLoopResult result = Parallel.ForEach(data, ShowData);

            //if(!result.IsCompleted) {
                Console.WriteLine(result.LowestBreakIteration);
            //}

        }
    }
}
