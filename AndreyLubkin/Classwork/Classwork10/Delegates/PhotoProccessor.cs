﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegates {
    public delegate void PhotoFilterHandler(Photo photo);

    public class PhotoProcessor {
        //public void Proccess(string path) {
        //    var photo = Photo.Load(path);

        //    var filter = new PhotoFilter();
        //    filter.ApplyBrightness(photo);
        //    filter.ApplyContrast(photo);
        //    filter.ApplyResize(photo);

        //    photo.Save();
        //}

        public void Process(string path, PhotoFilterHandler filterHandler) {
            var photo = Photo.Load(path);

            filterHandler.Invoke(photo);

            photo.Save();
        }

    }
}
