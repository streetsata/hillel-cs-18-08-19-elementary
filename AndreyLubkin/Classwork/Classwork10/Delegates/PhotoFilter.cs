﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegates {
    public class PhotoFilter {
        public void ApplyBrightness(Photo photo) {
            Console.WriteLine("Apply Brightness");
        }
        public void ApplyContrast(Photo photo) {
            Console.WriteLine("Apply Contrast");
        }

        public void ApplyResize(Photo photo) {
            Console.WriteLine("Apply Resize");
        }
    }
}
