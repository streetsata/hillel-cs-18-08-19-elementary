﻿using System;
using Delegates;

namespace DelegatesApp {

    class Program {
        public delegate void PlayerInfo();
        public delegate void PlayerInfoWithGoals(int goals);
        static void PlayerInfoFunc(int goals) {
            Console.WriteLine($"Криштмано Роналдо: ~{goals} голов за сезон");
        }
        static void F1() { Console.WriteLine("F1"); }
        static void F2() { Console.WriteLine("F2"); }
        static void F3() { Console.WriteLine("F3"); }
        static void F4() { Console.WriteLine("F4"); }

        static void Main(string[] args) {
            //PlayerInfoWithGoals playerInfo = PlayerInfoFunc;
            //playerInfo.Invoke(50);

            PlayerInfo playerInfo = null;
            PlayerInfo playerInfo1 = F1;
            PlayerInfo playerInfo2 = F2;
            playerInfo = playerInfo1 + playerInfo2;
            playerInfo.Invoke();

            playerInfo -= playerInfo1;

            Console.WriteLine();
            playerInfo.Invoke();


            //// IoC dependencies injection
            //var process = new PhotoProcessor();
            ////proccess.Proccess("photo.jpg");

            //var filter = new PhotoFilter();

            //PhotoFilterHandler photoFilterHandler = filter.ApplyBrightness;

            //photoFilterHandler += filter.ApplyContrast;
            //photoFilterHandler += filter.ApplyResize;
            //photoFilterHandler += RemoveRedEyaFilter;
            //process.Process("pic.jpg", photoFilterHandler);
        }

        static void RemoveRedEyaFilter(Photo photo) {
            Console.WriteLine("RemoveRedEyaFilter");
        }
    }
}
