﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClassWork22Web.Models {
    public class Purchases {
        public int Id { get; set; }
        public string Person { get; set; }
        public int BookId { get; set; }
        public DateTime Date { get; set; }
    }
}
