﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ClassWork22Web.Models {
    public class BookStoreContext : DbContext {
        public DbSet<Book> Books { get; set; }
        public DbSet<Purchases> Purchases { get; set; }
    }
}
