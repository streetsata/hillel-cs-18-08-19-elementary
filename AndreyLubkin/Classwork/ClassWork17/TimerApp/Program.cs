﻿using System;
using System.Threading;

namespace TimerApp {
    class Program {
        static void PrintTime(object state) {
            Console.WriteLine($"Time is: {DateTime.Now.ToLongTimeString()}");
        }
        static void Main(string[] args) {
            TimerCallback timerCB = new TimerCallback(PrintTime);
            Timer timer = new Timer(timerCB, null, 0, 1000);

            Console.ReadLine();
        }
    }
}
