﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PoolThreadMutex {

    class Program {
        static Semaphore pool = null;
        static void Main(string[] args) {
            pool = new Semaphore(2, 4);

            for(int i = 1; i <= 10; i++) {
                Thread thread = new Thread(Foo);
                thread.Start(i);
            }
            Thread.Sleep(2000);
            pool.Release();

            Console.ReadLine();
        }

        private static void Foo(object num) {
            pool.WaitOne();
            {
                Console.WriteLine($"Thread {num} enter");
                Thread.Sleep(5000);
                Console.WriteLine($"====Thread {num} exit");
                Console.WriteLine();
            }
            pool.Release();
        }
    }
}
