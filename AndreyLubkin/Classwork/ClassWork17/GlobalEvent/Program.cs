﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GlobalEvent {
    class Program {
        static EventWaitHandle manual = null;
        static void Main(string[] args) {
            manual = new EventWaitHandle(false, EventResetMode.ManualReset, "{11882284-6B2A-4465-9777-0C58A9D6E4BC}");
            Thread thread = new Thread(Foo);
            thread.IsBackground = true;
            thread.Start();

            Console.WriteLine("Press any key");
            Console.ReadKey();

            manual.Set();
            Console.ReadLine();
        }

        private static void Foo() {
            manual.WaitOne();
            while(true) {
                Console.WriteLine("Hello World!");
                Thread.Sleep(300);
            }
        }
    }
}
