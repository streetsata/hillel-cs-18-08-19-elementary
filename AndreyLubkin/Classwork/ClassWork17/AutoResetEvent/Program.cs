﻿using System;
using System.Threading;

namespace AutoResetEvent_ {
    class Params {
        public int A { get; set; }
        public int B { get; set; }

        public Params(int a, int b) {
            A = a;
            B = b;
        }

    }
    class Program {
        public static AutoResetEvent waitHandle = new AutoResetEvent(false);
        static void Main(string[] args) {
            Console.WriteLine($"Main() invoked on thread {Thread.CurrentThread.ManagedThreadId}");
            Params p = new Params(10, 10);
            Thread thread = new Thread(Add);
            thread.Start(p);

            waitHandle.WaitOne();
            Console.WriteLine("Other thread is done!");

            Console.ReadLine();
        }

        private static void Add(object obj) {
            Console.WriteLine($"Add() invoked on thread {Thread.CurrentThread.ManagedThreadId}");
            Params pr = obj as Params;
            Thread.Sleep(2000);
            Console.WriteLine($"{pr.A} + {pr.B} = {pr.A+pr.B}");

            waitHandle.Set();
        }
    }
}
