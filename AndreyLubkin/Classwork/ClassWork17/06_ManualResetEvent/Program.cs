﻿using System;
using System.Threading;

namespace _06_ManualResetEvent {
    class Program {
        static ManualResetEvent manual = new ManualResetEvent(false);
        static void Main(string[] args) {
            Thread[] threads = { new Thread(Foo1), new Thread(Foo2) };
            foreach(var item in threads) item.Start();
            Console.ReadKey();
            manual.Set();

            Console.ReadLine();
            
        }
        private static void Foo1() {
            Console.WriteLine("Foo1");
            manual.WaitOne();
            Console.WriteLine("Thread 1 was ended");
        }
        private static void Foo2() {
            Console.WriteLine("Foo2");
            manual.WaitOne();
            Console.WriteLine("Thread 2 was ended");
        }

        
    }
}
