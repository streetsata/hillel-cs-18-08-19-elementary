﻿using System;
using System.Threading;

namespace AutoResetEvent_2 {
    class Program {
        static AutoResetEvent autoResetEvent = new AutoResetEvent(false);
        static void Main(string[] args) {
            var thread = new Thread(Foo);
            thread.Start();

            Console.ReadKey();
            autoResetEvent.Set();
            Console.ReadKey();
            autoResetEvent.Set();
            Console.ReadKey();
            autoResetEvent.Set();
            Console.ReadLine();
        }

        private static void Foo() {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("O");
            autoResetEvent.WaitOne();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("O");
            autoResetEvent.WaitOne();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("O");
        }
    }
}
