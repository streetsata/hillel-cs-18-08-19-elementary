﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Async {
    class Program {
        public delegate int BinaryOp(int x, int y);
        private static bool isDone = false;

        static void Main(string[] args) {
            Console.WriteLine($"Main() invoked on thread {Thread.CurrentThread.ManagedThreadId}");

            BinaryOp b = new BinaryOp(Add);
            IAsyncResult ar = b.BeginInvoke(10, 10, new AsyncCallback(AddComplete), "Thanks");
            while(!isDone) {
                Console.WriteLine("Working...");
                Thread.Sleep(1000);
            }
            int answer = b.EndInvoke(ar);
            Console.WriteLine();
            Console.WriteLine($"10 + 10 is {answer}");

            Console.ReadLine();
        }

        private static void AddComplete(IAsyncResult ar) {
            string msg = ar.AsyncState as string;
            Console.WriteLine($"AddComplete() invoked on thread {Thread.CurrentThread.ManagedThreadId}");
            Console.WriteLine(msg);
            isDone = true;
        }

        private static int Add(int x, int y) {
            Console.WriteLine($"Add() invoked on thread {Thread.CurrentThread.ManagedThreadId}");

            Thread.Sleep(5000);
            return x + y;
        }
    }
}

