﻿using System;
using ClassWork.Sketch;

namespace ClassWork {
    class Person {
        public byte Id { get; set; }
        public string Name { get; set; }
        public byte Age { get; set; }
    }

    class Program {
        static void Func(out int a, out int b) {
            a = 10;
            b = 6;
            // 0011
            // ^
            // 1010
            // 1010
            int temp = a;
            a = b;
            b = temp;
        }
        static void Main(string[] args) {
            int a, b;
            int.TryParse("5", out a);
            Console.WriteLine(a);
            //Console.WriteLine($"a: {a}, b: {b}");
            //Func(out a, out b);
            //Console.WriteLine($"a: {a}, b: {b}");

            //StructData structData1, structData2;
            //structData2 = new StructData();
            //structData2.xStruct = 5;
            //structData1 = structData2;
            //Console.WriteLine(structData1.xStruct);

            //ClassData ClassData1, ClassData2;
            //ClassData2 = new ClassData();
            //ClassData2.xClass = 5;
            //ClassData1 = ClassData2;

            //Console.WriteLine(ClassData1.xClass);
            //ClassData2.xClass = 6;
            //Console.WriteLine(ClassData1.xClass);
            //ClassData1.ConsoleColore1 = ConsoleColore.Blue;
            //Console.BackgroundColor = ConsoleColor.Green;
        }
    }
}
