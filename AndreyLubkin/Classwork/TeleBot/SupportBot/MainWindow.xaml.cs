﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telegram.Bot;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace SupportBot {
    public partial class MainWindow :Window {
        ObservableCollection<TelegramUser> Users;
        TelegramBotClient botClient;

        public MainWindow() {
            InitializeComponent();
            InitBot();
        }

        private async void InitBot() {
            Users = new ObservableCollection<TelegramUser>();

            usersList.ItemsSource = Users;

            string token = "807040869:AAGt5gF5fz4P-FGg6MznWxXk3h-3QJfHpkk";

            botClient = new TelegramBotClient(token) { Timeout = TimeSpan.FromSeconds(5) };

            #region Load - Хотел создать для этого отдельный метод, но в async метод нельзя передать ссылку на переменную..
            string data = string.Empty;
            using(StreamReader sr = new StreamReader("db.log")) {
                data = sr.ReadToEnd();
            }
            string[] msgs = data.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            foreach(string msg in msgs) {
                string sender = Regex.Match(msg, @"<Sender>(.*?)<Sender/>", RegexOptions.Singleline).Groups[1].Value;
                string id = Regex.Match(msg, @"<ChatId>(.*?)<ChatId/>", RegexOptions.Singleline).Groups[1].Value;
                string nick = string.Empty;
                bool isFind = false;
                foreach(var us in Users) {
                    if(us.Id.ToString() == id) {
                        nick = us.Nick;
                        isFind = true;
                    }
                }
                if(!isFind) {
                    var user = await botClient.GetChatMemberAsync(id, Convert.ToInt32(id));
                    nick = user.User.FirstName;
                }
                var person = new TelegramUser(nick, Convert.ToInt32(id));
                if(!Users.Contains(person))
                    Users.Add(person);
                string message = string.Empty;
                if(sender == "User") message = $"{person.Nick}: {Regex.Match(msg, @"<Text>(.*?)<Text/>", RegexOptions.Singleline).Groups[1].Value}";
                else if(sender == "Bot") message = $"{Regex.Match(msg, @"<Text>(.*?)<Text/>", RegexOptions.Singleline).Groups[1].Value}";
                Users[Users.IndexOf(person)].AddMessage(message);

            }
            #endregion

            botClient.OnMessage += BotClient_OnMessage;

            botClient.StartReceiving();
            BtnSendMsg.Click += SendMsg;
            txtSendMsg.KeyDown += (s, e) => { if(e.Key == Key.Return) { SendMsg(s, e); } };
        }

        private async void SendMsg(object sender, RoutedEventArgs e) {
            var concreteUser = Users[Users.IndexOf(usersList.SelectedItem as TelegramUser)];


            
            try {
                await botClient.SendTextMessageAsync(concreteUser.Id, txtSendMsg.Text);
                string responseMsg = $"Support Chat: {txtSendMsg.Text}";
                concreteUser.AddMessage(responseMsg);
                string logText = $"<Sender>Bot<Sender/><Time>{DateTime.Now}<Time/><ChatId>{concreteUser.Id}<ChatId/><Text>{responseMsg}<Text/>";
                File.AppendAllText("db.log", $"{logText}|");

                txtSendMsg.Text = string.Empty;
            }
            catch {
                MessageBox.Show("Требуется ввести текст сообщения!");
                return;
            }

            
        }

        private void BotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e) {
            string msg = $"<Sender>User<Sender/><Time>{DateTime.Now}<Time/><ChatId>{e.Message.Chat.Id}<ChatId/><Text>{e.Message.Text}<Text/>";

            File.AppendAllText("db.log", $"{msg}|");

            Debug.WriteLine(msg);

            this.Dispatcher.Invoke(() => {
                var person = new TelegramUser(e.Message.Chat.FirstName, e.Message.Chat.Id);

                if(!Users.Contains(person))
                    Users.Add(person);
                Users[Users.IndexOf(person)].AddMessage($"{person.Nick}: {e.Message.Text}");
            });



        }
    }
}
