﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportBot {
    class TelegramUser: IEquatable<TelegramUser>, INotifyPropertyChanged {
        private string nick;
        private long id;
        public ObservableCollection<string> Messages { get; set; }

        public string Nick {
            get { return nick; }
            set {
                nick = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.nick)));
            }
        }
        public long Id {
            get { return id; }
            set {
                id = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.id)));
            }
        }

        public TelegramUser(string nickName, long ChatId) {
            this.nick = nickName;
            this.id = ChatId;
            Messages = new ObservableCollection<string>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Equals(TelegramUser other) => other.Id == this.Id;

        public void AddMessage(string text) => Messages.Add(text);
    }
}
