using NUnit.Framework;
using PasswordBL;

namespace Tests {
    [TestFixture]
    public class PasswordTests {
        [Test]
        [TestCase("qwerty", 1)]
        [TestCase("qwerty123", 2)]
        [TestCase("1qazXSW2", 3)]
        [TestCase("1qazXSW@", 4)]
        [TestCase("@RTEM_IS)*!19*s", 5)]
        public void GetPasswordStrength_CheckPasswordStringStrength_returnedPoints(string password, int expected) {
            int result = PasswordLib.GetPasswordStrength(password); //expected
            Assert.AreEqual(result, expected);
        }
    }
}