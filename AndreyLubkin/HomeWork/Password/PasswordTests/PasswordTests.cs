using NUnit.Framework;
using PasswordBL;

namespace Password.Tests {
    [TestFixture]
    public class PasswordTests {
        [Test]
        [TestCase("qwerty", 1)]
        [TestCase("qwerty123", 2)]
        [TestCase("1qazXSW2", 3)]
        [TestCase("1qazXSW@", 4)]
        [TestCase("@RTEM_IS)*!19*s", 5)]
        public void GetPasswordStrength_CheckPasswordStringStrength_returnedPoints(string password,int expected) {
            int result = PasswordClass.GetPasswordStrength(password); //expected
            Assert.AreEqual(result, expected);
        }
    }
}