﻿using PasswordBL;
using System;

namespace PasswordApp {
    class Program {
        static void Main(string[] args) {
            Console.Write("Введите пароль: ");
            string pass = Console.ReadLine();
            int points = PasswordClass.GetPasswordStrength(pass);
            Console.Write($"Сложность важего пароля: ");
            for(int i = 1; i <= points; i++) {
                Console.Write("*");
            }
        }
    }
}
