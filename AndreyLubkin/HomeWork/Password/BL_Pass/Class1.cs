﻿using System;

namespace PasswordBL {
    public class PasswordClass {
        public static int GetPasswordStrength(string pass) {
            int points = 0;
            if(pass.Length > 0) points++;
            foreach(var ch in pass) {
                if(Char.IsDigit(ch)) {
                    points++;
                    break;
                }
            }
            foreach(var ch in pass) {
                if(Char.IsUpper(ch)) {
                    points++;
                    break;
                }
            }
            foreach(var ch in pass) {
                if(Char.IsSymbol(ch)) {
                    points++;
                    break;
                }
            }
            if(pass.Length > 9) points++;
            return points;
        }
    }
}
