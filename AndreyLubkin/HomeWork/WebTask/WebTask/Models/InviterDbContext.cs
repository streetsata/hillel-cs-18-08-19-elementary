﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTask.Models {
    public class InviterDbContext : DbContext {
        public InviterDbContext(DbContextOptions<InviterDbContext> options) : base(options) {

        }

        public DbSet<Form> Forms { get; set; }
    }
}
