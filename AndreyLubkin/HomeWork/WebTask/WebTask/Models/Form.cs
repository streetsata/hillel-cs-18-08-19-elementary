﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTask.Models {
    public class Form {
        public int Id { get; set; }
        public string Creator { get; set; }
        public string CreatorEmail { get; set; }
        public string FormText { get; set; }
        public string SendToEmails { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
