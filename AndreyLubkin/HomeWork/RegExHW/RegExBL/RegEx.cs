﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace RegExBL {
    public class Register {
        public static bool RegisterUser(string login,string password) {
            bool log = Regex.IsMatch(login, "^([a-zA-Z]+)$");
            bool pass = Regex.IsMatch(password, "^([0-9a-zA-Z]+)$");
            if(log & pass) return true;
            else return false;
        }
    }
    public class Pretext {
        public static string PretextChanger(string input) {
            Regex regex = new Regex(@"(?i)\b(в|без|до|из|к|на|по|о|от|перед|при|через|с|у|за|над|об|под|про|для|во)\b");
            input = regex.Replace(input, "ГАВ!");
            return input;
        }
    }
    public class IPWHOIS {
        /// <summary>
        /// Находит по айпи вашу жертву и после вызова к нему приедут ваши кенты и рабируются с ним что по чем
        /// </summary>
        /// <param name="ip">ip адресс жертвы</param>
        /// <returns>Возвращает словарь<string,string> со значениями [ip,continent,country,city,phone,currency]</returns>
        public static Dictionary<string,string> FindByIp(string ip) {
            ip = ip.Trim();
            if(!Regex.IsMatch(ip, @"\d+\.\d+\.\d+\.\d+")) throw new Exception("Ip is not valid");//*.*.*.*
                using(WebClient wc = new WebClient()) {
                string ipinfo = wc.DownloadString($"http://free.ipwhois.io/xml/{ip}");
                Dictionary<string, string> ipInfoDic = new Dictionary<string, string>();

                ipinfo = new Regex(@"\s+").Replace(ipinfo, "");
                ipInfoDic["ip"] = ip;
                ipInfoDic["continent"] = Regex.Match(ipinfo, @"<continent>(.*?)</continent>", RegexOptions.Singleline).Groups[1].Value;
                ipInfoDic["country"] = Regex.Match(ipinfo, @"<country>(.*?)</country>", RegexOptions.Singleline).Groups[1].Value;
                ipInfoDic["city"] = Regex.Match(ipinfo, @"<city>(.*?)</city>", RegexOptions.Singleline).Groups[1].Value;
                ipInfoDic["phone"] = Regex.Match(ipinfo, @"<country_phone>(.*?)</country_phone>", RegexOptions.Singleline).Groups[1].Value;
                ipInfoDic["currency"] = Regex.Match(ipinfo, @"<currency_symbol>(.*?)</currency_symbol>", RegexOptions.Singleline).Groups[1].Value;

                return ipInfoDic;

            }
        }
    }
    public class Email {
        public static bool Valid(string email) {
            email = email.Trim();
            return Regex.IsMatch(email, @"(?i)[a-z\.]+@[a-z]+\.[a-z]+");
        }
    }
}
