﻿using RegExBL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace RegExApp {
    class Program {
        static void Main(string[] args) {
            Task1();
            //Task2(); //текст находится в папке с исполняемым файлом (text.txt)
            Task3();
            Task4();
        }

        static void Task1() {
            Console.Write("Введите логин: ");
            string log = Console.ReadLine();
            Console.WriteLine();
            Console.Write("Введите пароль: ");
            string pass = Console.ReadLine();
            Console.WriteLine();
            if(Register.RegisterUser(log, pass))
                Console.WriteLine("Вы зарегистрированы!");
            else Console.WriteLine("Ошибка, вы ввели некоректные данные!");
        }
        static void Task2() {
            //в, без, до, из, к, на, по, о, от, перед, при, через, с, у, за, над, об, под, про, для.
            /*В моем подъезде без слов до этого момента разговаривать нельзя было из за кондуктора который приходит к 9. На нашей улице это не в новинку, но об этом можно узнать только под лавкой*/
            string text = string.Empty;
            using(StreamReader sr = new StreamReader("text.txt")) {
                string output = sr.ReadToEnd();
                text = Pretext.PretextChanger(output);
            }
            using(StreamWriter sw = new StreamWriter("text.txt")) {
                sw.Write(text);
            }
        }
        static void Task3() {
            //Console.WriteLine(IPWHOIS.FindByIp("212.46.31.255"));
            Console.Write("Введите IP: ");
            string ip = Console.ReadLine();
            try {
                Dictionary<string, string> ipInfo = IPWHOIS.FindByIp(ip);


                Console.WriteLine($"IP: {ipInfo["ip"]}");
                Console.WriteLine($"Континент: {ipInfo["continent"]}");
                Console.WriteLine($"Страна: {ipInfo["country"]}");
                Console.WriteLine($"Город: {ipInfo["city"]}");
                Console.WriteLine($"Код номера страны: {ipInfo["phone"]}");
                Console.WriteLine($"Валюта: {ipInfo["currency"]}");
            }
            catch {
                Console.WriteLine("Извините, IP адресс некорректный");
            }
        }
        static void Task4() {
            Console.Write("Введите email: ");
            string email = Console.ReadLine();
            //Console.WriteLine(Email.Valid("VASA@MAIL.RU")); 
            bool valid = Email.Valid(email);
            if(valid) Console.WriteLine("Почта корректная");
            else Console.WriteLine("Почта некорректная");
        }
    }
}
