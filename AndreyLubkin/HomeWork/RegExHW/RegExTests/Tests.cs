using NUnit.Framework;
using RegExBL;

namespace Tests {
    [TestFixture]
    public class Tests {
        [Test]
        [TestCase("Admin","Admin123",true)]
        [TestCase("olivertwitter17665%1uhjo", "foidk12gtydjGTFOBAE%@hd7", false)]
        [TestCase("andrewhisbwh", "pokiehdy2017", true)]
        public void Register_GetsLoginAndPassword_returnBoolIsValidLogAndPass(string login, string password,bool expected) {
            bool result = Register.RegisterUser(login, password);
            Assert.AreEqual(expected, result);
        }
        [Test]
        [TestCase("� ����� ���� �� ������� � ������ ������ �����", "� ����� ���� ���! ������� � ������ ������ �����")]
        [TestCase("� ����� ���� � ��� � �� ���� �� ���� ��������", "���! ����� ���� � ��� � �� ���� ���! ���� ��������")]
        public void PretextChanger_GetsText_returnedChangedText(string text, string expected) {
            text = Pretext.PretextChanger(text);
            Assert.AreEqual(text, expected);
        }
        [Test]
        [TestCase("192.99.203.93", "Newark")]
        [TestCase("212.46.31.255","Moscow")]
        [TestCase("45.168.79.5", "RioBananal")]
        public void FindByIp_GetsIp_returnedIpInfo(string ip,string expectedCity) {
            Assert.AreEqual(expectedCity, IPWHOIS.FindByIp(ip)["city"]);
        }
        [Test]
        [TestCase("lub.and@gmail.com",true)]
        [TestCase("VASA@MAIL.RU", true)]
        [TestCase("alexmilo.ru",false)]
        public void Valid_GetsEmailString_returnBoolIsValid(string email,bool expected) {
            Assert.AreEqual(expected, Email.Valid(email));
        }
    }
}