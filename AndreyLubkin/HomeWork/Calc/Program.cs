﻿using System;
using System.Collections.Generic;

namespace Calc {

    class Program {
        static void Main(string[] args) {
            if (args.Length != 0) {
                string inputArgs = string.Join("", args);
                Calculate(inputArgs);

            } else {
                Console.WriteLine("Ввод выражения:");
                string input = Console.ReadLine();
                Calculate(input);
            }

            Console.ReadLine();
        }


        private static void Calculate(string input) {
            /*
                Строковый калькулятор
            */

            DoubleStack numberStack = new DoubleStack(); // Стек чисел
            CharStack charStack = new CharStack(); // Стек символов
            // класс реализации стеков полностью переписал сам.


            char[] inputChar = Training(input); // подготовка вводимой строки
            try {
                for (int i = 0; i <= inputChar.Length; i++) { // перебор элеметов в строке
                    if (i == inputChar.Length) {
                        while (!charStack.isEmpty()) {
                            double count = CountChar(numberStack.Pop(), numberStack.Pop(), charStack.Pop());
                            numberStack.Push(count);
                        }
                        Console.WriteLine("Ответ: " + numberStack.Pop());
                    } else if (Char.IsDigit(inputChar[i])) {
                        if (i != inputChar.Length - 1 && Char.IsDigit(inputChar[i + 1])) {
                            Console.WriteLine("Допускается вычесление только натуральных чисел!");
                            break;
                        }
                        numberStack.Push((int)Char.GetNumericValue(inputChar[i]));
                    } else if (charStack.isEmpty()) {
                        charStack.Push(inputChar[i]);
                    } else if (getPriority(inputChar[i]) > getPriority(charStack.Peek())) {
                        charStack.Push(inputChar[i]);
                    } else if (getPriority(inputChar[i]) == -1) {
                        charStack.Push(inputChar[i]);
                    } else if (getPriority(inputChar[i]) == -2) {
                        while (getPriority(charStack.Peek()) != -1) {
                            double count = CountChar(numberStack.Pop(), numberStack.Pop(), charStack.Pop());
                            numberStack.Push(count);
                        }
                        charStack.Pop();
                    } else if (getPriority(inputChar[i]) <= getPriority(charStack.Peek())) {
                        do {
                            double count = CountChar(numberStack.Pop(), numberStack.Pop(), charStack.Pop());
                            numberStack.Push(count);
                        } while (!(charStack.ShowCount() <= 1) && getPriority(inputChar[i]) <= getPriority(charStack.Peek()));
                        charStack.Push(inputChar[i]);
                    }
                }
            }
            catch {
                Console.WriteLine("Вы ввели некоректное выражение!");
            }
        }

        private static char[] Training(string input) {
            input = input.Replace(" ", string.Empty);
            char[] inputChar = input.ToCharArray();
            return inputChar;
        }

        private static double CountChar(double num2, double num1, char sign) {
            double result;
            switch (sign) {
                case '+':
                result = num1 + num2;
                break;
                case '-':
                result = num1 - num2;
                break;
                case '*':
                result = num1 * num2;
                break;
                case '/':
                result = num1 / num2;
                break;
                case '^':
                result = Math.Pow(num1, num2);
                break;
                default:
                result = 0;
                break;
            }
            return result;
        }

        private static int getPriority(char input) {
            switch (input) {
                case '+':
                return 1;
                case '-':
                return 1;
                case '*':
                return 2;
                case '/':
                return 2;
                case '^':
                return 3;
                case '(':
                return -1;
                case ')':
                return -2;
                default:
                return 0;
            }
        }
    }

    public class DoubleStack {
        private List<double> stack = new List<double>();
        private int size;

        public DoubleStack() {
            size = 0;
        }

        public int ShowCount() {
            return size;
        }

        public bool isEmpty() {
            return size == 0;
        }

        public double Pop() {
            if (size <= 0) {
                throw new InvalidOperationException();
            }
            double Element = stack[--size];
            stack.RemoveAt(size);
            return Element;
        }

        public void Push(double newChar) {
            stack.Insert(size++, newChar);
        }

        public double Peek() {
            if (size <= 0) {
                throw new InvalidOperationException();
            }
            return stack[size - 1];
        }
    }

    public class CharStack {
        private List<char> stack = new List<char>();
        private int size;

        public CharStack() {
            size = 0;
        }

        public int ShowCount() {
            return size;
        }

        public bool isEmpty() {
            return size == 0;
        }

        public char Pop() {
            if (size <= 0) {
                throw new InvalidOperationException();
            }
            char Element = stack[--size];
            stack.RemoveAt(size);
            return Element;
        }

        public void Push(char newChar) {
            stack.Insert(size++, newChar);
        }

        public char Peek() {
            if (size <= 0) {
                throw new InvalidOperationException();
            }
            return stack[size - 1];
        }
    }
}
