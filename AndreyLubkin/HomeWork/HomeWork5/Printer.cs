﻿using System;

namespace HomeWork5 {
    class Printer {
        public void Print(string value) {
            Console.WriteLine(value);
        }
    }
    class PrinterGreen :Printer {
        public new void Print(string value) {
            Console.ForegroundColor = ConsoleColor.Green;
            base.Print(value);
            Console.ResetColor();
        }
    }
    class PrinterRed :Printer {
        public new void Print(string value) {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine(value);
            Console.ResetColor();
        }

    }
}
