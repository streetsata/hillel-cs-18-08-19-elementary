﻿using System;

namespace HomeWork5 {
    
    class Program {
        static void Main(string[] args) {
            Task1();
            Task2();
            Task3();
            Task4(); // ключи активации Pro "12345","747281","272191"
                     // ключи активации Expert "777777", "810481", "119248"
        }

        private static void Task1() {
            Printer UpCast = new PrinterGreen();
            UpCast.Print("Hello world!");

            PrinterGreen DownCast = (PrinterGreen)UpCast; // Приведение типов, возможный DownCasting
            //PrinterRed DownCast = (PrinterRed)UpCast; // Приведение типов, невозможный DownCasting

            DownCast.Print("Hello world");
        }

        private static void Task2() {
            ExcelentPupil excelentPupil = new ExcelentPupil();
            GoodPupil goodPupil = new GoodPupil();
            BadPupil badPupil = new BadPupil();
            ClassRoom classRoom = new ClassRoom(excelentPupil, goodPupil, badPupil);
            classRoom.ShowPupils();
        }
        private static void Task3() {
            Plane plane = new Plane(new Coords(100.5,1600),700000,2010,1000,40);
            Ship ship = new Ship(new Coords(200, 600), 260000, 1995, 15, "Аврора");
            Car car = new Car(new Coords(5, 16), 1000, 2000,"Ford",1500);
            Vehicle.ShowAllInfo(plane, ship, car);
        }
        private static void Task4() {
            DocumentWorker documentWorker = new DocumentWorker();
            string[] proKeys = { "12345","747281","272191" };
            string[] expertKeys = { "777777", "810481", "119248" };
            Console.WriteLine("Введите ключ активации:");
            string key = Console.ReadLine();
            bool license = false;
            for(int i = 0; i < proKeys.Length; i++) {
                if(proKeys[i] == key) {
                    documentWorker = new DocumentWorkerPro();
                    license = true;
                    Console.WriteLine("Лицензия Pro активирована");
                }
            }
            for(int i = 0; i < expertKeys.Length; i++) {
                if(expertKeys[i] == key) {
                    documentWorker = new DocumentWorkerExpert();
                    license = true;
                    Console.WriteLine("Лицензия Expert активирована");
                }
            }
            if(!license) {
                Console.WriteLine("Лицензия не активирована");
            }
            Console.WriteLine();
            Console.WriteLine("Попытка открыть документ:");
            documentWorker.OpenDocument();
            Console.WriteLine();
            Console.WriteLine("Попытка отредактировать документ:");
            documentWorker.EditDocument();
            Console.WriteLine();
            Console.WriteLine("Попытка сохранить документ:");
            documentWorker.SaveDocument();
            Console.WriteLine();
        }
    }
}
