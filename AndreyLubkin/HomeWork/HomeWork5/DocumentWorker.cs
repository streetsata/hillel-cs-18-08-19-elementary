﻿using System;

namespace HomeWork5 {
    class DocumentWorker {
        public virtual void OpenDocument() {
            Console.WriteLine("Документ открыт");
        }
        public virtual void EditDocument() {
            Console.WriteLine("Редактировать документ можно только в версии Pro");
        }
        public virtual void SaveDocument() {
            Console.WriteLine("Сохранять документ можно только в версии Pro");
        }
    }
    class DocumentWorkerPro :DocumentWorker {
        public override void EditDocument() {
            Console.WriteLine("Документ отредактирован");
        }
        public override void SaveDocument() {
            Console.WriteLine("Документ сохранен в старом формате, сохранение в остальных форматах доступно в версии Expert");
        }
    }
    class DocumentWorkerExpert :DocumentWorker {
        public override void SaveDocument() {
            Console.WriteLine("Документ сохранен в новом формате");
        }
    }
}
