﻿using System;

namespace HomeWork5 {
    abstract class Vehicle {
        public Coords Coord;
        public double Count;
        public int yearOfIssue;
        public Vehicle(Coords coords,double Count,int year) {
            this.Coord = coords;
            this.Count = Count;
            this.yearOfIssue = year;
        }
        public abstract void ShowInfo();
        public static void ShowAllInfo(params Vehicle[] vehicles) {
            for(int i = 0; i < vehicles.Length; i++) {
                vehicles[i].ShowInfo();
                Console.WriteLine();
            }
        }
    }
    class Coords {
        public double X;
        public double Y;
        public Coords(double x, double y) {
            this.X = x;
            this.Y = y;
        }
    }

    class Plane :Vehicle {
        public double hightInMeters;
        public int numberOfPassengers;
        public Plane(Coords coords, double Count, int year, double hight, int number) : base(coords, Count, year) {
            this.hightInMeters = hight;
            this.numberOfPassengers = number;
        }
        public override void ShowInfo() {
            Console.WriteLine("Самолет");
            Console.WriteLine($"Координаты: X:{Coord.X}Y:{Coord.Y}");
            Console.WriteLine($"Стоимость: {Count}$");
            Console.WriteLine($"Год изготовления: {yearOfIssue}");
            Console.WriteLine($"Высота полета: {hightInMeters} метров");
            Console.WriteLine($"Количество пассажиров: {numberOfPassengers}");
        }
    }

    class Car :Vehicle {
        public string Mark;
        public int carryingCapacity;
        public Car(Coords coords, double Count, int year, string Mark, int carryingCapacity) : base(coords, Count, year) {
            this.Mark = Mark;
            this.carryingCapacity = carryingCapacity;
        }
        public override void ShowInfo() {
            Console.WriteLine("Машина");
            Console.WriteLine($"Координаты: X:{Coord.X}Y:{Coord.Y}");
            Console.WriteLine($"Стоимость: {Count}$");
            Console.WriteLine($"Год изготовления: {yearOfIssue}");
            Console.WriteLine($"Марка: {Mark}");
            Console.WriteLine($"Грузоподьемность: {carryingCapacity} кг");
        }
    }
    class Ship :Vehicle {
        public int numberOfPassengers;
        public string registrationPort;
        public Ship(Coords coords, double Count, int year, int numberOfPassengers, string registrationPort) : base(coords, Count, year) {
            this.numberOfPassengers = numberOfPassengers;
            this.registrationPort = registrationPort;
        }
        public override void ShowInfo() {
            Console.WriteLine("Лодка");
            Console.WriteLine($"Координаты: X:{Coord.X}Y:{Coord.Y}");
            Console.WriteLine($"Стоимость: {Count}$");
            Console.WriteLine($"Год изготовления: {yearOfIssue}");
            Console.WriteLine($"Порт регистрации: {registrationPort}");
            Console.WriteLine($"Количество пассажиров: {numberOfPassengers}");
        }
    }
}
