﻿using System;

namespace HomeWork5 {
    class ClassRoom {
        private Pupil[] Pupils;
        public ClassRoom(params Pupil[] Pupils) {                   
            if(!(1<=Pupils.Length&&Pupils.Length <= 4)) throw new Exception("Конструктор принимает минимум 1 и максимум 4 ученика!");
            this.Pupils = Pupils;
        }

        public void ShowPupils() {
            for(int i = 0; i < Pupils.Length; i++) {
                Console.WriteLine($"Ученик номер {i+1}");
                Console.WriteLine("Учится:");
                Pupils[i].Study();
                Console.WriteLine("Читает");
                Pupils[i].Read();
                Console.WriteLine("Пишет");
                Pupils[i].Write();
                Console.WriteLine("Отдыхает");
                Pupils[i].Relax();
                Console.WriteLine();
            }
        }
    }
    abstract class Pupil {
        public abstract void Study();
        public abstract void Read();
        public abstract void Write();
        public abstract void Relax();
    }
    class ExcelentPupil :Pupil {
        public override void Study() {
            Console.WriteLine("Усердно учится");
        }
        public override void Read() {
            Console.WriteLine("Много читает");
        }
        public override void Write() {
            Console.WriteLine("Пишет все домашнее задание");
        }
        public override void Relax() {
            Console.WriteLine("Нет времени на отдых...");
        }
    }
    class GoodPupil :Pupil {
        public override void Study() {
            Console.WriteLine("Учится по настроению");
        }
        public override void Read() {
            Console.WriteLine("Иногда что то читает");
        }
        public override void Write() {
            Console.WriteLine("Иногда прокрастинирует вместо домашки");
        }
        public override void Relax() {
            Console.WriteLine("Любит погулять с друзьями");
        }
    }
    class BadPupil :Pupil {
        public override void Study() {
            Console.WriteLine("Вообще не учится");
        }
        public override void Read() {
            Console.WriteLine("Ненавидит читать");
        }
        public override void Write() {
            Console.WriteLine("'Мою тетрадь съела собака'");
        }
        public override void Relax() {
            Console.WriteLine("Деградирует на Ютубе");
        }
    }
}
