﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebTask2.Models
{
    public class WebTask2Context : DbContext
    {
        public WebTask2Context (DbContextOptions<WebTask2Context> options)
            : base(options)
        {
        }

        public DbSet<WebTask2.Models.Book> Book { get; set; }
    }
}
