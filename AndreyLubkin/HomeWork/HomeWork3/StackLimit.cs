﻿using System;
using System.Collections.Generic;

namespace HomeWork3 {
    /// <summary>
    /// Лимитированный стек
    /// </summary>
    class StackLimit<T> {
        private List<T> stack = new List<T>();
        private int size;
        private int limit;

        public StackLimit(int limit) {
            size = 0;
            if(limit <= 0) throw new Exception("Incorrect limit");
            this.limit = limit;
        }

        public int Count { get { return size; } }

        public bool IsEmpty() {
            return size == 0;
        }

        public T Pop() {
            if(size <= 0) {
                throw new InvalidOperationException();
            }
            T Element = stack[--size];
            stack.RemoveAt(size);
            return Element;
        }

        public void Push(T newItem) {
            stack.Insert(size++, newItem);
            if(size > limit) {
                size--;
                stack.RemoveAt(0);
            }
        }

        public T Peek() {
            if(size <= 0) {
                throw new InvalidOperationException();
            }
            return stack[size - 1];
        }
    }
}
