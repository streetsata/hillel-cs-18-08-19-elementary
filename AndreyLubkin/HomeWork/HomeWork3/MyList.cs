﻿using System;
using System.Collections.Generic;

namespace HomeWork3 {
    /// <summary>
    /// Моя реализауция List
    /// </summary>
    public class MyList<T> {
        /// <summary>
        /// Массив на основе которого работает MyList
        /// </summary>
        private T[] ListArr;
        /// <summary>
        /// Текущий размер MyList
        /// </summary>
        private int size;
        /// <summary>
        /// Дефолтный размер массива который лежит в основе
        /// </summary>
        private int defaultSize = 10;

        /// <summary>
        /// Конструктор
        /// </summary>
        public MyList() {
            ListArr = new T[defaultSize];
            size = 0;
        }

        public int Count { get { return size; } }

        /// <summary>
        /// Добавление элемента в MyList
        /// </summary>
        /// <param name="Element">Принимает добавляемый элемент</param>
        public void Add(T Element) {
            if(ListArr.Length == size) {
                T[] newArr = new T[ListArr.Length * 2];
                Array.Copy(ListArr, 0, newArr, 0, size);
                ListArr = newArr;
            }
            ListArr[size++] = Element;
        }
        /// <summary>
        /// Возвращение первого вхождения
        /// </summary>
        public int IndexOf(T Element) {
            for(int i = 0; i<ListArr.Length; i++) {
                if(EqualityComparer<T>.Default.Equals(ListArr[i],Element)) return i; 
            }
            return -1;
        }
        /// <summary>
        /// Удаление элемента по указанному индексу
        /// </summary>
        public void RemoveAt(int Index) {
            if(!(0 <= Index && Index < size)) throw new Exception("Out of border");
            T[] newArr = new T[ListArr.Length];
            Array.Copy(ListArr, 0, newArr, 0, Index);
            Array.Copy(ListArr, Index + 1, newArr, Index, size);
            ListArr = newArr;
            size--;
        }
        /// <summary>
        /// Возвращает элемент по индексу
        /// </summary>
        public T GetByIndex(int Index) {
            if(!(0 <= Index && Index < size)) throw new Exception("Out of border");
            return ListArr[Index];
        }

    }
}
