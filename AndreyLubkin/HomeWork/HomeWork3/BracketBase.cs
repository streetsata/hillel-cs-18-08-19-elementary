﻿using System;

namespace HomeWork3 {
    /// <summary>
    /// База скобочек которая нужна для скобочного алгоритма
    /// </summary>
    class BracketBase {
        private string[] openBracket;
        private string[] closeBracket;

        public BracketBase(string[] brackets) {
            openBracket = new string[brackets.Length];
            closeBracket = new string[brackets.Length];


            for(int i = 0; i < brackets.Length; i++) {
                if(brackets[i].Length % 2 != 0) {
                    throw new Exception("Incorrect args");
                }
                for(int j = 0; j < brackets[i].Length / 2; j++) {
                    openBracket[i] += brackets[i][j];
                    closeBracket[i] += brackets[i][brackets[i].Length / 2 + j];
                }
                if(openBracket[i] == closeBracket[i]) throw new Exception("Incorrect brackets");
            }
        }

        public bool IsBracket(string bracket) {
            if(Array.IndexOf(openBracket, bracket) > -1 || Array.IndexOf(closeBracket, bracket) > -1) return true;
            return false;
        }
        public bool IsOpen(string bracket) {
            if(Array.IndexOf(openBracket, bracket) > -1) return true;
            return false;
        }
        public bool IsClose(string bracket) {
            if(Array.IndexOf(closeBracket, bracket) > -1) return true;
            return false;
        }
        public int GetMaxBrecket() {
            int max = 0;
            foreach(var item in openBracket) {
                if(item.Length > max) {
                    max = item.Length;
                }
            }
            return max;
        }

        public int GetType(string bracket) {
            if(!this.IsBracket(bracket)) throw new Exception("Bracket is not Exists");
            if(this.IsOpen(bracket)) {
                return Array.IndexOf(openBracket, bracket);
            } else {
                return Array.IndexOf(closeBracket, bracket);
            }
        }

        public bool checkBrackets(string bracketsExpression) {
            string[] bracketObjects = this.GetObjects(bracketsExpression);
            
            MyStack<string> bracketsStack = new MyStack<string>();
            for(int i = 0; i < bracketObjects.Length; i++) {
                if(this.IsBracket(bracketObjects[i])) {
                    if(this.IsOpen(bracketObjects[i])) {
                        bracketsStack.Push(bracketObjects[i]);
                    } else if(this.IsClose(bracketObjects[i])) {
                        if(bracketsStack.Count <= 0) {
                            return false;
                        }
                        if(this.GetType(bracketsStack.Peek()) == this.GetType(bracketObjects[i])) {
                            bracketsStack.Pop();
                        } else {
                            return false;
                        }
                    }
                }
            }
            if(bracketsStack.Count == 0) {
                return true;
            } else {
                return false;
            }
        }
        private string[] GetObjects(string bracketsExpression) {
            int count = 0;
            for(int i = 0; i < bracketsExpression.Length; i++) {
                string temp = "";
                for(int j = 0; j < this.GetMaxBrecket(); j++) {
                    if(i + j >= bracketsExpression.Length) break;
                    temp += bracketsExpression[i + j];
                    if(this.IsBracket(temp)) {
                        count++;
                        i += j;
                    }
                }
            }
            int pointer = 0;
            string[] objects = new string[count];
            for(int i = 0; i < bracketsExpression.Length; i++) {
                string temp = "";
                for(int j = 0; j < this.GetMaxBrecket(); j++) {
                    if(i + j >= bracketsExpression.Length) break;
                    temp += bracketsExpression[i + j];
                    if(this.IsBracket(temp)) {
                        objects[pointer++] = temp;
                        i += j;
                    }
                }
            }
            return objects;
        }
    }
}
