﻿using System;
using System.Collections.Generic;
using Sketch.Tools;

namespace HomeWork3 {
    //Это 3- е дз!!!
    internal class MyList2<T> {/*
        private T[] array;
        private int current = 0;

        public int Count { get { return array == null ? 0 : array.Length; } }
        public void Add(T value)
        {
            if (array == null)
                array = new T[1];
            if (array.Length == current)
                Array.Resize(ref array, array.Length - 1);
            array[current] = value;
            current++;
        }
        public T this[int index]
        {
            get
            {
                if (array == null)
                {
                    throw new NullReferenceException();
                }
                else
                    if(index<0||index>array.Length-1)
                {
                    throw new IndexOutOfRangeException();
                }
                else
                {
                    return array[index];
                }
            }
        }
        */
        public T[] myList = null;

        public T this[int index] {
            get { return myList[index]; }
            set { myList[index] = value; }
        }

        public MyList2() {
            this.myList = new T[1];
        }

        public MyList2(int count) {
            this.myList = new T[count];
        }

        public void Add(T item) {
            T[] extendedList = new T[myList.Length + 1];
            extendedList[extendedList.Length - 1] = item;
            myList = extendedList;
        }

        public int Capacity {
            get { return myList.Length; }

        }

        //  Возвращает число элементов, которые фактически содержатся в коллекции MyList<T>.

        public int Count {
            get {
                int count = 0;
                for(int i = 0; i < myList.Length; i++) {
                    if(myList[i].ToString() != null) {
                        count++;
                    }
                }
                return count;
            }
        }
    }


    public static class ExtensionMyList {
        public static T[] GetArray<T>(this MyList<T> list) {
            T[] Arr = new T[list.Count];
            for(int i = 0; i < list.Count; i++) {
                Arr[i] = list.GetByIndex(i);
            }
            return Arr;
        }
    }
    class Program {
        static void Main(string[] args) {
            //bracketAction(); // 1 задание
            // 2 задание выполнено в StackLimit.cs
            // 3 задание выполнено в MyList.cs
            // 4 задание выполнено в ExtensionMyList

            MyList2<int> list = new MyList2<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            Console.WriteLine(list.Count);
            Console.WriteLine(list[4]);
        }

        private static void bracketAction() {
            int countTypes = Tools.GetCheckNumber(1, 5, "Введите количество различных типов скобочек:");
            string[] types = Tools.GetTypes(countTypes);

            BracketBase bracketBase = new BracketBase(types);

            Console.WriteLine("Введите скобочное выражение:");
            string bracketsExpression = Console.ReadLine();

            bool bracketsCheck = bracketBase.checkBrackets(bracketsExpression);
            string answer = bracketsCheck ? "правильное" : "неправильное";
            Console.WriteLine($"Скобочное выражение {answer}");
        }

    }
    /// <summary>
    /// Стек на основе листа моей реализации
    /// </summary>
    /// <typeparam name="T">Любой тип</typeparam>
    class MyStack<T> {
        private List<T> stack = new List<T>();
        private int size;

        public MyStack() {
            size = 0;
        }

        public int Count { get { return size; } }

        public bool isEmpty() {
            return size == 0;
        }

        public T Pop() {
            if(size <= 0) {
                throw new InvalidOperationException();
            }
            T Element = stack[--size];
            stack.RemoveAt(size);
            return Element;
        }

        public void Push(T newItem) {
            stack.Insert(size++, newItem);
        }

        public T Peek() {
            if(size <= 0) {
                throw new InvalidOperationException();
            }
            return stack[size - 1];
        }
    }
}
