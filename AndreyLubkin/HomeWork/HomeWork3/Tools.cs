﻿using System;

namespace Sketch.Tools {
    class Tools {
        public static int GetCheckNumber(int min, int max, string question) {
            int number = min - 1;

            while(!(min <= number && number <= max)) {
                try {
                    Console.WriteLine(question + $"({min}-{max})");
                    number = Int32.Parse(Console.ReadLine());
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }
        public static double GetCheckDouble(double min, double max, string question) {
            double number = min - 1;

            while(!(min <= number && number <= max)) {
                try {
                    Console.WriteLine(question + $"({min}-{max})");
                    number = Convert.ToDouble(Console.ReadLine());
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }

        internal static string[] GetTypes(int countTypes) {
            string[] types = new string[countTypes];
            Console.WriteLine("Введите тип скобочек в формате [открытая][закрытая]\nНапример: '()' или '2332'");
            for(int i = 0; i < countTypes; i++) {
                bool flag = true;
                while(flag) {
                    Console.WriteLine($"Введите {i+1} тип скобочек:");
                    string type = Console.ReadLine();
                    if(type.Length % 2 !=0) {
                        Console.WriteLine("Вы ввели некорректное значение!");
                    } else {
                        types[i] = type;
                        flag = false;
                    }
                }
            }
            return types;
        }
    }
}
