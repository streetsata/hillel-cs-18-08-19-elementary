﻿using System;
using System.Collections.Generic;

namespace ClassTest {
    class Program {
        static void Main(string[] args) {
            CharStack charStack = new CharStack();
            charStack.Push('1');
            charStack.Push('2');
            charStack.Push('3');
            charStack.Push('4');
            charStack.Push('5');
            charStack.Push('6');
            charStack.Push('7');
            charStack.Push('8');
            Console.WriteLine(charStack.ShowCount());
            Console.WriteLine(charStack.Pop());
            Console.WriteLine(charStack.Pop());
            Console.WriteLine(charStack.Pop());
            Console.WriteLine(charStack.Peek());
            Console.WriteLine(charStack.Peek());

        }
    }

    public class CharStack {
        private List<char> stack = new List<char>();
        private int size;

        public CharStack() {
            size = 0;
        }

        public int ShowCount() {
            return size;
        }

        public bool isEmpty() {
            return size == 0;
        }

        public char Pop() {
            if(size <= 0) {
                throw new InvalidOperationException();
            }
            char Element = stack[--size];
            stack.RemoveAt(size);
            return Element;
        }

        public void Push(char newChar) {
            stack.Insert(size++, newChar);
        }

        public char Peek() {
            if (size <= 0) {
                throw new InvalidOperationException();
            }
            return stack[size-1];
        }
    }

    public class CStack<T> {
        private T[] _array; //массив для хранения данных типа T
        private const int defaultCapacity = 10; //вместимость по умолчанию, потом можно расширить
        private int size; //размер

        public CStack() { //конструктор
            this.size = 0;
            this._array = new T[defaultCapacity];
        }

        public bool isEmpty() //проверка на пустоту
        {
            return this.size == 0;
        }

        public virtual int Count //параметр для вывода размера 
        {
            get {
                return this.size;
            }
        }

        public T Pop() //метод взятия с вершины
        {
            if (this.size == 0) { //вброс ошибки при взятии с пустого стека (Overflow)
                throw new InvalidOperationException();
            }
            return this._array[--this.size];
        }

        public void Push(T newElement) {
            if (this.size == this._array.Length) //если у нас переполнение...
            { //знаю, что неоптимально, но это c#...
                T[] newArray = new T[2 * this._array.Length];
                Array.Copy(this._array, 0, newArray, 0, this.size);
                this._array = newArray; //просто создаем новый массив с двойным размером
            }
            this._array[this.size++] = newElement; //вставляем элемент
        }

        public T Peek() {
            if (this.size == 0) {
                throw new InvalidOperationException();
            }
            return this._array[this.size - 1];
        }
    }
}
