﻿using System;

namespace School {
    public class StudentWithAdvisor: Student {
        private Teacher teacher;

        public Teacher TeachTeacher { get => teacher; set => teacher = value; }
        public override string ToString() {
            return $"Student {Name}, Teacher: {teacher.Name}";
        }
    }
}
