﻿using System;
using System.Collections.Generic;

namespace School {
    public class Teacher: Person {
        private Student[] students;

        public Student[] Students { get => students; set => students = value; }

        public override void Print() {
            Console.WriteLine("Hello from Teacher");
        }
        public override string ToString() {
            string student = "";
            foreach(var item in students) {
                student += item.Name+" ";
            }
            return $"Teacher {Name}, Students: {student}";
        }
        public override Person Clone(object obj) {
            if(obj == null) return null;
            Teacher clone = new Teacher();
            clone = obj as Teacher;
            return clone;
        }
        private static string[] TeacherNames = { "Сергей","Георгий","Михаил","Валерий" };
        public static Teacher RandomTeacher() {
            return new Teacher() { Name = TeacherNames[new Random().Next(0, TeacherNames.Length)], Age = new Random().Next(19, 30) };
        }
    }
}


//Gerych