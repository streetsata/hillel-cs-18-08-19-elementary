﻿using System;

namespace School {
    public class Student: Person {
        private int averageMark;
        private int course = 1;
        

        public int AverageMark { get => averageMark; set => averageMark = value; }
        public int Course { get => course; }
        public void NextCourse() {
            if(course < 6) {
                course++;
            }
        }
        public override void Print() {
            Console.WriteLine("Hello from Student");
        }
        public override string ToString() {
            return $"Student {Name}";
        }
        public override Person Clone(object obj) {
            if(obj == null) return null;
            Student clone = new Student();
            clone = obj as Student;
            return clone;
        }

        private static string[] StudentNames = { "Толян", "Ваня", "Петя","Вадим","Саня" };
        public static Student RandomStudent() {
            return new Student() { Name = StudentNames[new Random().Next(0, StudentNames.Length)], Age = new Random().Next(19, 30), AverageMark = new Random().Next(4, 12) };
        }
    }
}
