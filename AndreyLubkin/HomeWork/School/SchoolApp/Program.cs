﻿using School;
using System;
using System.Collections.Generic;

namespace SchoolApp {
    class Program {
        static void Main(string[] args) {
            Person[] people = new Person[6];
            people[0] = Person.RandomPerson();
            people[1] = StudentWithAdvisor.RandomStudent();
            people[2] = StudentWithAdvisor.RandomStudent();
            people[3] = StudentWithAdvisor.RandomStudent();
            people[4] = Teacher.RandomTeacher();
            people[5] = Person.RandomPerson();

            int teachers = 0;
            int students = 0;
            int persons = 0;

            for(int i = 0; i<people.Length; i++) {
                if(people[i].GetType() == typeof(Teacher)) {
                    teachers++;
                } else if(people[i] is Student) {
                    Student stu = people[i] as Student;
                    stu.NextCourse();
                    students++;
                    people[i] = stu;
                } else if(people[i].GetType() == typeof(Person)) {
                    persons++;
                }
            }
            people[1].Clone(people[5]);
            Console.WriteLine($"Persons: {persons}\nStudents: {students}\n");

            //foreach(var person in people) {
            //    Console.WriteLine(person);
            //}
            for(int i = 0; i < people.Length; i++) {
                Type tp = BaseType(people[i].GetType());
                string tpName;
                if(tp != null) {
                    tpName = tp.Name;
                } else {
                    tpName = people[i].GetType().Name;
                }
                Console.WriteLine($"{i + 1} Person: {tpName}");
                Console.WriteLine();
            }
        }

        static Type BaseType(Type type) {

            if(type.BaseType != null)
                return BaseType(type.BaseType);
            return null;
        }
    }
}
