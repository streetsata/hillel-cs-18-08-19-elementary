﻿using System;

namespace School {
    public class Person {
        private string name;
        private int age;

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = (0 < value && value < 150) ? value : 0; }

        public virtual void Print() {
            Console.WriteLine("Hello from Person");
        }
        public override string ToString() {
            return $"Person {Name}";
        }
        public override bool Equals(object obj) {
            return base.Equals(obj);
        }
        public override int GetHashCode() {
            return base.GetHashCode();
        }
        public virtual Person Clone(object obj) {
            if(obj == null) return null;
            Person clone = obj as Person;
            return clone;
        }
        private static string[] PersonNames = { "Дмитрий","Александр","Василий" };
        public static Person RandomPerson() {
            return new Person() { Name = PersonNames[new Random().Next(0, PersonNames.Length)], Age = new Random().Next(19, 30) };
        }
    }
}
