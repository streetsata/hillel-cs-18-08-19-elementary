﻿using System;

namespace Business {
    class Worker {
        private string fio;
        private string job;
        private int employeeYear;
        private int currentYear;


        public Worker(string fio, string job, int employeeYear, int currentYear) {
            this.fio = fio;
            this.job = job;
            this.employeeYear = employeeYear;
            this.currentYear = currentYear;
        }

        public string Fio {
            get {
                return fio;
            }
        }
        public string Job {
            get {
                return job;
            }
        }
        public int EmployeeYear {
            get {
                return employeeYear;
            }
        }

        public int CurrentYear {
            get {
                return currentYear;
            }
        }

        public void SetYear(int year) {
            if(1990 >= year) {
                currentYear = year;
            } else {
                throw new Exception();
            }
        }

        public static Worker[] AlphabetSort(Worker[] workerArr) {
            for(int i = 0; i < workerArr.Length; i++) {
                for(int j = 0; j < workerArr.Length - 1; j++) {
                    if(needToReOrder(workerArr[j], workerArr[j + 1])) {
                        Worker temp = workerArr[j];
                        workerArr[j] = workerArr[j + 1];
                        workerArr[j + 1] = temp;
                    }
                }
            }
            return workerArr;
        }

        public static Worker[] YearSort(Worker[] workerArr, int maxYear) {
            int counter = 0;
            foreach(Worker worker in workerArr) {
                if(worker.CurrentYear-worker.EmployeeYear >= maxYear) {
                    counter++;
                }
            }
            Worker[] newWorkerArr = new Worker[counter];
            int pos = 0;
            foreach(Worker worker in workerArr) {
                if(worker.CurrentYear - worker.EmployeeYear >= maxYear) {
                    newWorkerArr[pos] = worker;
                    pos++;
                }
            }
            return newWorkerArr;
        }

        private static bool needToReOrder(Worker w1, Worker w2) {
            for(int i = 0; i < (w1.Fio.Length > w2.Fio.Length ? w2.Fio.Length : w1.Fio.Length); i++) {
                if(w1.Fio.ToCharArray()[i] < w2.Fio.ToCharArray()[i]) return false;
                if(w1.Fio.ToCharArray()[i] > w2.Fio.ToCharArray()[i]) return true;
            }
            return false;
        }
    }
}
