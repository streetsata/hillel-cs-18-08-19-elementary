﻿namespace Business {
    class Order {
        private string buyerBill;
        private string merchantBill;
        private double amountUSD;

        public Order(string buyerBill, string merchantBill, double amountUSD) {
            this.buyerBill = buyerBill;
            this.merchantBill = merchantBill;
            this.amountUSD = amountUSD;
        }
        public string BuyerBill {
            get {
                return this.buyerBill;
            }
        }
        public string MerchantBill {
            get {
                return this.merchantBill;
            }
        }
        public double AmountUSD {
            get {
                return this.amountUSD;
            }
        }
        public double GetAmountEUR(double courseUSD_EUR) {
            return this.amountUSD * courseUSD_EUR;
        }
        public double GetAmountUAH(double courseUSD_UAH) {
            return this.amountUSD * courseUSD_UAH;
        }

        static public Order[] OrderArrSort(Order[] ordersArr) {
            for(int i = 1; i < ordersArr.Length; i++) {
                for(int j = 0; j < ordersArr.Length - i; j++) {
                    if(ordersArr[j].AmountUSD < ordersArr[j+1].AmountUSD) {
                        Swap(ref ordersArr[j], ref ordersArr[j+1]);
                    }
                }
            }
            return ordersArr;
        }
        static private void Swap(ref Order o1, ref Order o2) {
            Order temp = o1;
            o1 = o2;
            o2 = temp;
        }

        public static Order[] OrderAmount(Order[] orderArr, int maxAmount) {
            int counter = 0;
            foreach(Order order in orderArr) {
                if(order.AmountUSD >= maxAmount) {
                    counter++;
                }
            }
            Order[] newOrderArr = new Order[counter];
            int pos = 0;
            foreach(Order order in orderArr) {
                if(order.AmountUSD >= maxAmount) {
                    newOrderArr[pos] = order;
                    pos++;
                }
            }
            return newOrderArr;
        }
    }
}
