﻿using System;
using Business;
using Sketch.Tools;

namespace ClassHomeWork {
    class Program {
        static void Main(string[] args) {
            ChooseClass();
        }
        private static void ChooseClass() {
            bool flag = true;
            while(flag) {
                ChooseClassCW();
                try {
                    int choice = Int32.Parse(Console.ReadLine());
                    switch(choice) {
                        case 1:
                            Console.Clear();
                            OrderAction();
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 2:
                            Console.Clear();
                            WorkerAction(); //
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 3:
                            Console.Clear();
                            PriceAction(); //
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            Console.Clear();
                            RectangleAction(); //
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 0:
                            flag = false;
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Выберите коррекиное значение!");
                            break;
                    }
                }
                catch {
                    Console.Clear();
                    Console.WriteLine("Введите корректное значение!");
                }
            }
        }

        private static void ChooseClassCW() {
            Console.WriteLine("Выберите Class:");
            Console.WriteLine(" 1. Order");
            Console.WriteLine(" 2. Worker");
            Console.WriteLine(" 3. Price");
            Console.WriteLine(" 4. Rectangle");
            Console.WriteLine(" 0. Выход");
        }
        private static void OrderAction() {
            int n = Tools.GetCheckNumber(1, 100, "Введите размер массива:");
            Order[] OrderArr = new Order[n];
            Tools.OrderArrFill(ref OrderArr);
            Console.Clear();

            Console.WriteLine("Все элементы массива класса Order:");
            Tools.ShowOrderArr(OrderArr);
            Console.WriteLine();

            Console.WriteLine("1. Сортировка по убыванию");
            Order[] OrderSort = Order.OrderArrSort(OrderArr);
            Tools.ShowOrderArr(OrderSort);
            Console.WriteLine();

            Console.WriteLine("2. Вывод определенных сумм");
            int maxAmount = Tools.GetCheckNumber(0, 1000, "Введите сумму:");
            Order[] OrderAmount = Order.OrderAmount(OrderSort, maxAmount);
            Tools.ShowOrderArr(OrderAmount);
        }
        private static void WorkerAction() {
            int n = Tools.GetCheckNumber(1, 100, "Введите размер массива:");
            Worker[] WorkerArr = new Worker[n];
            Tools.WorkerArrFill(ref WorkerArr);
            Console.Clear();

            Console.WriteLine("Все элементы массива класса Worker:");
            Tools.ShowWorkerArr(WorkerArr);
            Console.WriteLine();

            Console.WriteLine("1. Сортировка по алфавиту:");
            Worker[] SortWorkerArr = Worker.AlphabetSort(WorkerArr);
            Tools.ShowWorkerArr(SortWorkerArr);
            Console.WriteLine();

            Console.WriteLine("2. Вывод учитывая стаж работы:");
            int maxYear = Tools.GetCheckNumber(0, 30, "Введите желаемый стаж:");
            Worker[] YearWorkerArr = Worker.YearSort(WorkerArr, maxYear);
            Tools.ShowWorkerArr(YearWorkerArr);
        }
        private static void PriceAction() {
            int n = Tools.GetCheckNumber(1, 100, "Введите размер массива:");
            Price[] PriceArr = new Price[n];
            Tools.PriceArrFill(ref PriceArr);
            Console.Clear();

            Console.WriteLine("Все элементы массива класса Price:");
            Tools.ShowPriceArr(PriceArr);
            Console.WriteLine();

            Console.WriteLine("1. Сортировка по алфавиту:");
            Price[] SortPriceArr = Price.AlphabetSort(PriceArr);
            Tools.ShowPriceArr(SortPriceArr);
            Console.WriteLine();

            Console.WriteLine("2. Вывод информации о товаре:");
            Console.WriteLine("Введите название товара:");
            string search = Console.ReadLine();
            Price[] SearchPriceArr = Price.ProductSearch(PriceArr, search);
            Tools.ShowPriceArr(SearchPriceArr);
        }
        private static void RectangleAction() {
            double sideOne = Tools.GetCheckDouble(0,1000,"Введите первую сторону прямоугольника:");
            double sideTwo = Tools.GetCheckDouble(0, 1000, "Введите вторую сторону прямоугольника:");

            Rectangle rectangle = new Rectangle(sideOne, sideTwo);
            Console.WriteLine("Введеный прямоугольник имеет:\n Площадь: {0}\n Периметр {1}",rectangle.Area,rectangle.Perimeter);
        }
    }
}