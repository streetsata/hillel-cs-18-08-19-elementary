﻿using System;

namespace Business {
    class Price {
        private string productName;
        private string shopName;
        private double productPrice;

        public Price(string productName, string shopName, double productPrice) {
            this.productName = productName;
            this.shopName = shopName;
            this.productPrice = productPrice;
        }
        public string ProductName {
            get {
                return productName;
            }
        }
        public string ShopName {
            get {
                return shopName;
            }
        }
        public double ProductPrice {
            get {
                return productPrice;
            }
        }

        public static Price[] AlphabetSort(Price[] workerArr) {
            for(int i = 0; i < workerArr.Length; i++) {
                for(int j = 0; j < workerArr.Length - 1; j++) {
                    if(needToReOrder(workerArr[j], workerArr[j + 1])) {
                        Price temp = workerArr[j];
                        workerArr[j] = workerArr[j + 1];
                        workerArr[j + 1] = temp;
                    }
                }
            }
            return workerArr;
        }
        private static bool needToReOrder(Price p1, Price p2) {
            for(int i = 0; i < (p1.ProductName.Length > p2.ProductName.Length ? p2.ProductName.Length : p1.ProductName.Length); i++) {
                if(p1.ProductName.ToCharArray()[i] < p2.ProductName.ToCharArray()[i]) return false;
                if(p1.ProductName.ToCharArray()[i] > p2.ProductName.ToCharArray()[i]) return true;
            }
            return false;
        }

        public static Price[] ProductSearch(Price[] priceArr, string search) {
            int counter = 0;
            foreach(Price price in priceArr) {
                if(price.ProductName.Contains(search)) {
                    counter++;
                }
            }
            Price[] newPriceArr = new Price[counter];
            int pos = 0;
            foreach(Price price in priceArr) {
                if(price.ProductName.ToLower().Contains(search.ToLower())) {
                    newPriceArr[pos] = price;
                    pos++;
                }
            }
            return newPriceArr;
        }
    }
}
