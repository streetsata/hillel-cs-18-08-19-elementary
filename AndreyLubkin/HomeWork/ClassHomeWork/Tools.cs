﻿using System;
using Business;

namespace Sketch.Tools {
    class Tools {
        public static int GetCheckNumber(int min, int max, string question) {
            int number = min - 1;

            while(!(min <= number && number <= max)) {
                try {
                    Console.WriteLine(question + $"({min}-{max})");
                    number = Int32.Parse(Console.ReadLine());
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }
        public static double GetCheckDouble(double min, double max, string question) {
            double number = min - 1;

            while(!(min <= number && number <= max)) {
                try {
                    Console.WriteLine(question + $"({min}-{max})");
                    number = Convert.ToDouble(Console.ReadLine());
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }
        public static void OrderArrFill(ref Order[] orderArr) {
            for(int i = 0; i < orderArr.Length; i++) {
                orderArr[i] = new Order(getRndBill(), getRndBill(), getRndNum(25, 500));
            }
        }
        public static void WorkerArrFill(ref Worker[] workerArr) {
            for(int i = 0; i < workerArr.Length; i++) {
                workerArr[i] = new Worker(getRndFio(),getRndJob(),getRndYear(),2019);
            }
        }
        public static void PriceArrFill(ref Price[] priceArr) {
            for(int i = 0; i < priceArr.Length; i++) {
                priceArr[i] = new Price(getRndPrdName(),getRndShName(),getRndPrice());
            }
        }

        private static string getRndPrdName() {
            string[] products = { "Помидоры", "Кукуруза", "Яблоки","Апельсины","Персики","Груши","Огурцы","Морковь","Арбуз","Киви","Мандарины","Манго","Дыня" };
            return getRndFromArr(products);
        }

        private static string getRndShName() {
            string[] shops = { "Радуга", "Ашан", "Пятерочка", "Магнит", "АТБ", "Метро", "Аллея", "Командор", "Эльдорадо", "Мерлен", "Розетка" };
            return getRndFromArr(shops);
        }

        private static double getRndPrice() {
            return Convert.ToDouble($"{getRndNum(1, 2)}{getRndNum(0, 9)}.{getRndNum(0, 9)}{getRndNum(0, 9)}");
        }

        private static string getRndFio() {
            string[] surnames = { "Бембеев", "Боженов", "Давтян", "Бирюков", "Смирнов", "Горбачёв", "Агаев", "Максимов", "Харитонов", "Булгаков", "Григорьева", "Фокина", "Сазонова", "Нестерова", "Фёдорова", "Пестова", "Соболева", "Цветкова", "Зиновьева", "Трофимова" };
            string[] initials = { "ПС", "ЗФ","КА","ЮС","ДГ","АВ" };
            return $"{getRndFromArr(surnames)} {getRndFromArr(initials)}";
        }

        private static string getRndJob() {
            string[] jobs = { "Junior SE", "Middle SE", "Senior SE", "Senior C++ Developer", "Junior Web Developer", "Junior C# Developer", "Senior C# Developer", "Junior QA", "Middle QA", "Middle C# Developer" };
            return getRndFromArr(jobs);
        }

        private static int getRndYear() {
            return getRndNum(1995, 2019);
        }

        public static void ShowOrderArr(Order[] orderArr) {
            foreach(Order order in orderArr) {
                Console.WriteLine("Счет плательщика: {0}, Счет получателя: {1}, Сумма(USD): {2}", order.BuyerBill, order.MerchantBill, order.AmountUSD);
            }
        }
        public static void ShowWorkerArr(Worker[] workerArr) {
            foreach(var worker in workerArr) {
                Console.WriteLine("ФИО: {0}, Должность: {1}, Год поступления: {2}",worker.Fio,worker.Job,worker.EmployeeYear);
            }
        }
        public static void ShowPriceArr(Price[] priceArr) {
            foreach(var price in priceArr) {
                Console.WriteLine("Название товара: {0}, Название магазина: {1}, Цена: {2}",price.ProductName,price.ShopName,price.ProductPrice);
            }
        }
        private static string getRndFromArr(string[] arr) {
            return arr[getRndNum(0, arr.Length - 1)];
        }
        private static int getRndNum(int min, int max) {
            Random rnd = new Random();
            return rnd.Next(min, max);
        }

        private static string getRndBill() {
            int randomBill = getRndNum(1000_0000, 9999_9999);
            return $"{randomBill}";
        }
    }
}
