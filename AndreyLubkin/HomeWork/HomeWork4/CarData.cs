﻿using System;

namespace HomeWork4 {
    class CarData {
        private string carMake;
        private string _VINcode;
        private string regCode;
        public CarData(string carMake, string VINcode,string regCode) {
            this.carMake = carMake;
            this._VINcode = VINcode;
            this.regCode = regCode;
        }
        public string CarMake {
            get {
                return carMake;
            }
        }
        public string VINCode {
            get {
                return _VINcode;
            }
        }
        public string RegCode {
            get {
                return regCode;
            }
            set {
                regCode = value;
            }
        }
        public override string ToString() {
            return $"Марка: {carMake}\nVIN код: {_VINcode}\nРегистрационный код: {regCode}";
        }
    }
    class CarDataBase {
        private CarData[] DB;
        private int count;
        public CarDataBase() {
            this.DB = new CarData[10];
            this.count = 0;
        }
        public int Length { get { return count; } }
        public void AddCar(string carMake, string VINcode, string regCode) {
            if(DB.Length == count) {
                CarData[] newArr = new CarData[DB.Length * 2];
                Array.Copy(DB, 0, newArr, 0, count);
                DB = newArr;
            }
            for(int i = 0; i < count; i++) {
                if(DB[i].VINCode == VINcode) throw new Exception("VIN Code and Regestration Code must be unique");
            }
            DB[count++] = new CarData(carMake, VINcode, regCode);
        }
        public CarData GetByVIN(string VINCode) {
            for(int i = 0; i < count; i++) {
                if(DB[i].VINCode == VINCode) {
                    return DB[i];
                }
            }
            return null;
        }
        public CarData[] FindByRegCode(string RegCodePart) {
            int counter = 0;
            for(int i = 0; i < count; i++) {
                if(DB[i].RegCode.Contains(RegCodePart)) {
                    counter++;
                }
            }
            counter = 0;
            CarData[] RegFindArr = new CarData[count];
            for(int i = 0; i < count; i++) {
                if(DB[i].RegCode.Contains(RegCodePart)) {
                    RegFindArr[counter++] = DB[i];
                }
            }
            return RegFindArr;
        }
        public void EditCarRegCode(string VINCode, string NewRegCode) {
            for(int i = 0; i < count; i++) {
                if(DB[i].VINCode == VINCode) {
                    DB[i].RegCode = NewRegCode;
                }
            }
        }
        public bool DelByVIN(string VINCode) {
            for(int i = 0; i < count; i++) {
                if(DB[i].VINCode == VINCode) {

                    CarData[] newArr = new CarData[DB.Length];
                    Array.Copy(DB, 0, newArr, 0, i);
                    Array.Copy(DB, i + 1, newArr, i, count);
                    DB = newArr;
                    count--;

                    return true;
                }
            }
            return false;
        }
        public CarData[] GetAllData() {
            CarData[] CarDataArr = new CarData[count];
            Array.Copy(this.DB, 0, CarDataArr, 0, count);
            return CarDataArr;
        }
    }
}
