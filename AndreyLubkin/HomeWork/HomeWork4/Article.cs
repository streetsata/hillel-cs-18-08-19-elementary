﻿using System;

namespace HomeWork4 {
    class Article {
        private string articleName;
        private string shopName;
        private decimal count;
        public Article(string articleName,string shopName, decimal count) {
            this.articleName = articleName;
            this.shopName = shopName;
            this.count = count;
        }
        public string ArticleName { get { return this.articleName; } }
        public string ShopName{ get { return this.shopName; } }
        public decimal Count { get { return this.count; } }

        public static void ShowInfo(Article article) {
            Console.WriteLine($"Название товара: {article.ArticleName}");
            Console.WriteLine($"Название магазина: {article.ShopName}");
            Console.WriteLine($"Стоимость: {article.Count} грн");
        }
    }

    class Shop {
        Article[] goods;
        public Shop(Article[] goods) {
            this.goods = goods;
        }

        public Article this[int index] {
            get {
                return goods[index];
            }
        }

        public int Count { get { return goods.Length; } }

        public Article[] FindArticle(string find) {
            Article[] findArticles;
            int counter = 0;
            for(int i = 0; i < goods.Length; i++) {
                if(goods[i].ArticleName.ToLower().Contains(find.ToLower())) {
                    counter++;
                }
            }
            findArticles = new Article[counter];
            counter = 0;
            for(int i = 0; i < goods.Length; i++) {
                if(goods[i].ArticleName.ToLower().Contains(find.ToLower())) {
                    findArticles[counter++] = goods[i];
                }
            }
            return findArticles;
        }
    }
}
