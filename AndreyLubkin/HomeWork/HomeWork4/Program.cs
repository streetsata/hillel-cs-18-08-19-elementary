﻿using System;
using Sketch.Tools;

namespace HomeWork4 {

    class Program {
        static void Main(string[] args) {
            HomeWork();
        }

        private static void HomeWork() {
            Task1();
            Task2();
            Task3();
            Task4();
            Task5();
            Task6();
        }

        private static void Task1() {
            MyMatrix matrix = new MyMatrix();
            Console.WriteLine("Ваша матрица:");
            Console.WriteLine(matrix[Tools.GetCheckNumber(1, 100, "Введите высоту матрицы"), Tools.GetCheckNumber(1, 100, "Введите ширину матрицы")]);
        }
        private static void Task2() {
            Article[] articles;
            fillArticleArr(out articles);
            Shop shop = new Shop(articles);

            Article.ShowInfo(shop[Tools.GetCheckNumber(0, shop.Count - 1, "Введите индекс:")]);

            Console.WriteLine("Введите название товара:");
            string find = Console.ReadLine();
            Article[] founded = shop.FindArticle(find);
            for(int i = 0; i < founded.Length; i++) {
                Article.ShowInfo(founded[i]);
                Console.WriteLine();
            }

        }
        private static void Task3() {
            int hours = 12;
            Clock clock = hours;
            Console.WriteLine(clock);

            Console.WriteLine();

            clock = new Clock(3);
            hours = (int)clock;
            Console.WriteLine(hours);
        }
        private static void fillArticleArr(out Article[] articles) {
            articles = new Article[5];
            articles[0] = new Article("Мяч","Детский мир",150);
            articles[1] = new Article("Ноутбук", "Эльдорадо", 16000);
            articles[2] = new Article("iPhone", "AppleStore", 26000);
            articles[3] = new Article("Книга", "Книжный", 35);
            articles[4] = new Article("Розы", "Магазин 'Аленький цветочек'", 400);
        }
        private static void Task4() {
            Fahrenheit fahrenheit = 96.8;
            Celcius celcius;
            celcius = (Celcius)fahrenheit;
            Console.WriteLine(celcius);
            celcius = 36;
            fahrenheit = (Fahrenheit)celcius;
            Console.WriteLine(fahrenheit);
        }
        private static void Task5() {
            Dollar dollar = 26.7;
            Euro euro;
            euro = (Euro)dollar;
            Console.WriteLine($"Доллар: {dollar} => Евро: {euro}");
            dollar = (Dollar)euro;
            Console.WriteLine($"Евро: {euro} => Доллар: {dollar}");
        }

        static CarDataBase DataBase = new CarDataBase();
        private static void Task6() {
            DataBase.AddCar("Audi", "345821", "G8WCV6WPGH4");
            DataBase.AddCar("BMW", "937510", "SKF70L2SFV5");
            DataBase.AddCar("Ford", "648294", "KV52MKS7VS0");
            /*
                Заполняю базу данных случайными данными для простоты проверки. Я к тому что укоротил VIN и тд
            */
            Console.Clear();
            Console.WriteLine("Добро пожаловать в CarDataBase!");
            Menu();
        }

        private static void Menu() {
            bool flag = true;
            while(flag) {
                Console.Clear();
                Console.WriteLine(" 1. Меню поиска\n 2. Занести в базу данные по новой машине\n 3. Редактирование информации о машине по VIN коду\n 4. Удалить машину с базы по VIN коду\n -1. Выход из программы");
                Console.Write("Выбор: ");
                string choose = Console.ReadLine();
                switch(choose) {
                    case "1":
                        Console.Clear();
                        MenuFind();
                        break;
                    case "2":
                        Console.Clear();
                        AddCar();
                        break;
                    case "3":
                        Console.Clear();
                        EditCar();
                        break;
                    case "4":
                        Console.Clear();
                        DelCar();
                        break;
                    case "-1":
                        flag = false;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Вы ввели некорректное значение!");
                        break;
                }

            }
            

        }

        private static void MenuFind() {
            bool flag = true;
            while(flag) {
                Console.Clear();
                Console.WriteLine(" 1. Поиск по VIN коду машины\n 2. Поиск по регистрационному номеру машины\n 7. Отобразить список всех машин\n 0. Назад");
                Console.Write("Выбор: ");
                string choose = Console.ReadLine();
                switch(choose) {
                    case "1":
                        Console.Clear();
                        FindByVin();
                        break;
                    case "2":
                        Console.Clear();
                        FindByNum();
                        break;
                    case "7":
                        Console.Clear();
                        ShowAll();
                        break;
                    case "0":
                        Console.Clear();
                        flag = false;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Вы ввели некорректное значение!");
                        break;
                }

            }
        }

        private static void FindByVin() {
            Console.WriteLine("Введите VIN:");
            string VIN = Console.ReadLine();
            CarData car = DataBase.GetByVIN(VIN);
            if(car != null) {
                Console.WriteLine(car);
            } else {
                Console.WriteLine("По указаному VIN автомобилей не найдено!");
            }
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }

        private static void FindByNum() {
            Console.WriteLine("Введите Регистрационный номер или его часть:");
            string RegCode = Console.ReadLine();
            CarData[] findCars = DataBase.FindByRegCode(RegCode);
            if(findCars.Length != 0) {
                for(int i = 0; i < findCars.Length; i++) {
                    Console.WriteLine(findCars[i]);
                    Console.WriteLine();
                }
            } else {
                Console.WriteLine("По указаному Регистрационному номеру автомобилей не найдено!");
            }
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }

        private static void ShowAll() {
            CarData[] allCars = DataBase.GetAllData();
            if(allCars.Length != 0) {
                for(int i = 0; i < allCars.Length; i++) {
                    Console.WriteLine(allCars[i]);
                    Console.WriteLine();
                }
            } else {
                Console.WriteLine("Автомобилей не найдено!");
            }
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }

        private static void AddCar() {
            Console.WriteLine("Введите VIM нового автомобиля:");
            string VIMCode = Console.ReadLine();
            if(DataBase.GetByVIN(VIMCode) != null) {
                Console.WriteLine("Автомобиль с таким VIM уже существует!");
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Введите марку автомобиля:");
            string carMake = Console.ReadLine();
            Console.WriteLine("Введите Регистрационный номер автомобиля:");
            string RegCode = Console.ReadLine();
            DataBase.AddCar(carMake, VIMCode, RegCode);
            Console.WriteLine("Автомобиль успешно добавлен!");
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }

        private static void EditCar() {
            Console.WriteLine("Введите VIN автомобиля, который вы хотите редактировать:");
            string VIN = Console.ReadLine();
            CarData car = DataBase.GetByVIN(VIN);
            if(car != null) {
                Console.WriteLine("Введите Регистрационный номер на который вы бы хотели изменить: (текущий {0})",car.RegCode);
                string NewReg = Console.ReadLine();
                DataBase.EditCarRegCode(VIN, NewReg);
                Console.WriteLine("Редактирование прошло успешно!");
            } else {
                Console.WriteLine("По указаному VIN автомобилей не найдено!");
            }
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }

        private static void DelCar() {
            Console.WriteLine("Введите VIN:");
            string VIN = Console.ReadLine();
            if(DataBase.GetByVIN(VIN) != null) {
                DataBase.DelByVIN(VIN);
                Console.WriteLine("Автомобиль успешно удален из базы!");
            } else {
                Console.WriteLine("По указаному VIN автомобилей не найдено!");
            }
            Console.WriteLine("Нажмите любую клавишу...");
            Console.ReadKey();
        }
    }
    
}
