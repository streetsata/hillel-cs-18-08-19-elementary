﻿namespace HomeWork4 {
    class Clock {
        int hours;
        public Clock() {
            this.hours = 0;
        }
        public Clock(int num) {
            this.hours = num;
        }

        public static explicit operator int(Clock clock) { return clock.hours; }
        public static implicit operator Clock(int num) { return new Clock(num); }

        public override string ToString() {
            return $"{this.hours} hours";
        }
    }
}
