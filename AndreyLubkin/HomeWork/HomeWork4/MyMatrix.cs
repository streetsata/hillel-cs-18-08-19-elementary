﻿namespace HomeWork4 {
    class MyMatrix {
        private int[,] matrix;

        public MyMatrix() {
            matrix = new int[1, 1];
        }

        public string this[int length,int width] {
            get {
                this.matrix = new int[length, width];
                string toStr = "";
                for(int i = 0; i < length; i++) {
                    for(int j = 0; j < width; j++) {
                        toStr += $"{matrix[i, j]}\t";
                    }
                    toStr += "\n";
                }
                return toStr;
            }
        }
        
        public int[,] GetLastMatrix() {
            return matrix;
        }

        public override string ToString() {
            return "Введите в формате matrix[length,width]";
        }
    }
}
