﻿using System;

namespace HomeWork4 {
    class Celcius {
        private double temperature;
        public Celcius(double temperature) {
            this.temperature = temperature;
        }

        public override string ToString() {
            return $"{this.temperature} °C";
        }

        public static implicit operator Celcius(int num) { return new Celcius(num); }

        public static explicit operator double(Celcius temp) { return temp.temperature; }
        public static explicit operator int(Celcius temp) { return (int)temp.temperature; }

        public static explicit operator Fahrenheit(Celcius temp) {
            double celTemp = (9 / (double)5 * temp.temperature) + 32;
            return new Fahrenheit(Math.Round(celTemp, 2));
        }
    }
    class Fahrenheit {
        private double temperature;
        public Fahrenheit(double temperature) {
            this.temperature = temperature;
        }

        public override string ToString() {
            return $"{this.temperature} °F";
        }

        public static implicit operator Fahrenheit(int num) { return new Fahrenheit(num); }
        public static implicit operator Fahrenheit(double num) { return new Fahrenheit(num); }

        public static explicit operator double(Fahrenheit temp) { return temp.temperature; }
        public static explicit operator int(Fahrenheit temp) { return (int)temp.temperature; }

        public static explicit operator Celcius(Fahrenheit temp) {
            double celTemp = 5 / (double)9 * (temp.temperature - 32);
            return new Celcius(Math.Round(celTemp, 2));
        }
    }

}
