﻿using System;

namespace HomeWork4 {
    struct Dollar {
        private decimal dollars;
        public Dollar(decimal dollars) {
            this.dollars = dollars;
        }

        public override string ToString() {
            return $"{Math.Round(this.dollars,2)} $";
        }

        public static implicit operator Dollar(int num) { return new Dollar(num); }
        public static implicit operator Dollar(double num) { return new Dollar((decimal)num); }

        public static explicit operator double(Dollar count) { return (double)count.dollars; }
        public static explicit operator int(Dollar count) { return (int)count.dollars; }

        public static explicit operator Euro(Dollar count) {
            return new Euro(count.dollars*(decimal)1.14);
        }
    }
    struct Euro {
        private decimal euro;
        public Euro(decimal euro) {
            this.euro = euro;
        }

        public override string ToString() {
            return $"{Math.Round(this.euro, 2)} Euro";
        }

        public static implicit operator Euro(int num) { return new Euro(num); }
        public static implicit operator Euro(double num) { return new Euro((decimal)num); }

        public static explicit operator double(Euro count) { return (double)count.euro; }
        public static explicit operator int(Euro count) { return (int)count.euro; }

        public static explicit operator Dollar(Euro count) {
            return new Dollar(count.euro / (decimal)1.14);
        }
    }
}
