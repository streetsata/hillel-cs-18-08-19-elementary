﻿using System;

namespace Sketch.Tools {
    class Tools {
        public static int GetCheckNumber(int min, int max, string question) {
            int number = min - 1;

            while(!(min <= number && number <= max)) {
                try {
                    Console.WriteLine(question + $"({min}-{max})");
                    number = Int32.Parse(Console.ReadLine());
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }
        public static double GetCheckDouble(double min, double max, string question) {
            double number = min - 1;

            while(!(min <= number && number <= max)) {
                try {
                    Console.WriteLine(question + $"({min}-{max})");
                    number = Convert.ToDouble(Console.ReadLine());
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }
        public static int GetCheckNumber(int[] param,string question) {
            int number = 0; 
            bool flag = true;
            while(flag) {
                try {
                    Console.WriteLine(question);
                    number = Int32.Parse(Console.ReadLine());
                    for(int i = 0; i < param.Length; i++) {
                        if(param[i] == number) {
                            flag = false;
                        }
                    }
                    if(flag) {
                        Console.WriteLine("Введите корректные данные!");
                    }
                    
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }

    }
}
