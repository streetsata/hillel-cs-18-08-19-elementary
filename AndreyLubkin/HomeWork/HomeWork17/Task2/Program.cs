﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task2 {
    class Program {
        static bool createdNew;
        static readonly Mutex mutex = new Mutex(false, "{1237CC1C-B13A-4E66-B885-0A07449B1FE6}", out createdNew);

        [STAThread]
        static void Main(string[] args) {
            if(!createdNew) {
                MessageBox.Show("Программа уже запущена.");
                return;
            }
            Console.WriteLine("Программа работает!");
            Console.ReadLine();
        }
    }
}
