﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task4 {
    class Program {
        static void Main(string[] args) {
            Func<int, int, int> AddDel = Add;
            IAsyncResult ar = AddDel.BeginInvoke(5, 6, Callback, AddDel);
            while(!ar.IsCompleted) { Console.Write(".");Thread.Sleep(300); }
            Console.ReadLine();
        }

        private static void Callback(IAsyncResult ar) {
            //Console.WriteLine(ar.AsyncState);
            Func<int, int, int> AddDel = (Func<int, int, int>)ar.AsyncState;
            int result = AddDel.EndInvoke(ar);
            Console.WriteLine($"Result: {result}");
        }

        private static int Add(int num1, int num2) {
            Thread.Sleep(2000);
            return num1 + num2;
        }
    }
}
