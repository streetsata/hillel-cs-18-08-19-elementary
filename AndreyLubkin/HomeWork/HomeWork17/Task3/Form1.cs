﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task3 {
    public partial class Form1 :Form {
        delegate void FooDel(string msg);
        static bool isDone = false;
        public Form1() {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e) {
            button1.Enabled = false;
            FooDel del = new FooDel(Foo);
            IAsyncResult result = del.BeginInvoke("IsComplete",null, null);
            while(!result.IsCompleted) {  }
            MessageBox.Show("IsComplete закончился");
            button1.Enabled = true;
        }

        private void Button2_Click(object sender, EventArgs e) {
            button2.Enabled = false;
            FooDel del = new FooDel(Foo);
            IAsyncResult result = del.BeginInvoke("EndInvoke",null, null);
            del.EndInvoke(result);
            MessageBox.Show("EndInvoke закончился");
            button2.Enabled = true;
        }

        private void Button3_Click(object sender, EventArgs e) {
            button3.Enabled = false;
            FooDel del = new FooDel(Foo);
            IAsyncResult result = del.BeginInvoke("Callback", new AsyncCallback(Callback), null);
            while(!isDone) { }
            button3.Enabled = true;
            isDone = false;
        }

        private void Callback(IAsyncResult ar) {
            MessageBox.Show("Callback закончился");
            isDone = true;
        }

        private void Foo(string msg) {
            MessageBox.Show($"Происходит выполнение {msg}. ThreadID: {Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(2000);
        }
    }
}
