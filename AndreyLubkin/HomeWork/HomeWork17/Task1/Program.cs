﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task1 {
    class Program {
        static Semaphore Pool = null;

        static void Main(string[] args) {
            Pool = new Semaphore(1,3);
            Thread[] threads = new Thread[5];
            for(int i = 0; i < threads.Length; i++) {
                threads[i] = new Thread(Add);
                threads[i].Start();
            }
            Console.ReadLine();
        }

        private static void Add() {
            Pool.WaitOne();
            string log;
            using(StreamReader sr = new StreamReader("info.log")) {
                log = sr.ReadToEnd();
            }
            Console.WriteLine($"Поток {Thread.CurrentThread.ManagedThreadId} считал из файла: '{log}'");
            Thread.Sleep(1000);
            Pool.Release();
        }
    }
}
