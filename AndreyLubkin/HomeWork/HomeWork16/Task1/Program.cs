﻿using System;
using System.Threading;

namespace Task1 {
    class Program {
        public static int counter = 1;
        public static int num;
        static void Main(string[] args) {
            Logic();
        }

        private static void Logic() {
            Console.WriteLine("Сколько потоков вы хотите?");
            Console.Write("Введите число: ");
            string snum = Console.ReadLine();
            try {
                num = Int32.Parse(snum);
            }
            catch(Exception) {
                Console.WriteLine("Вы ввели некорректное число!");
                return;
            }

            Thread thread = new Thread(Method);
            thread.Start();
        }

        private static void Method() {
            if(counter < num+1) {
                //Thread.Sleep();
                Console.WriteLine($"Thread {Environment.CurrentManagedThreadId} - {counter}");
                counter++;
                var threadAnother = new Thread(Method);
                threadAnother.Start();
                Thread.Sleep(10000000);
            }
        }
    }
}
