﻿using System;
using System.Threading;

namespace Task2Matrix {
    class Program {
        class Sign {
            public static char GetSign() {
                Random rnd = new Random();
                int t = rnd.Next(10);
                if(t <= 2)
                    return (char)('0' + rnd.Next(10));
                else if(t <= 4)
                    return (char)('a' + rnd.Next(27));
                else if(t <= 6)
                    return (char)('A' + rnd.Next(27));
                else
                    return (char)(rnd.Next(32, 255));
            }

            public static void PrintWhite(int left, int top) {
                Console.ForegroundColor = ConsoleColor.White;
                Console.SetCursorPosition(left,top);
                Console.Write(Sign.GetSign());
                Console.ForegroundColor = ConsoleColor.White;
            }

            public static void PrintLightGreen(int left, int top) {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.SetCursorPosition(left,top);
                Console.Write(Sign.GetSign());
                Console.ForegroundColor = ConsoleColor.White;
            }

            public static void PrintGreen(int left, int top) {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.SetCursorPosition(left,top);
                Console.Write(Sign.GetSign());
                Console.ForegroundColor = ConsoleColor.White;
            }
            public static void PrintHole(int left, int top) {
                Console.SetCursorPosition(left, top);
                Console.Write(" ");
            }
        }
        static void Main(string[] args) {
            Console.CursorVisible = false;
            //int height = Console.WindowHeight;
            int width = Console.WindowWidth;
            //Matrix(10,height);
            Thread[] threads = new Thread[width-1];
            for(int i = 0; i < threads.Length; i++) {
                threads[i] = new Thread(MatrixThread);
                threads[i].Start(i);
                Thread.Sleep(10);
            }
            //Thread t1 = new Thread(MatrixTreD);
            //t1.Start(0);
            //Thread t2 = new Thread(Mat1);
            //t2.Start(1);
        }

        private static void MatrixThread(object obj) {
            int left = (int)obj;
            Random rnd = new Random();
            Thread.Sleep(rnd.Next(200, 5000));
            Matrix(rnd.Next(3,8), Console.WindowHeight, left);
        }

        static void Matrix(int length,int height,int left) {
            int pos = 0;
            int ms = new Random().Next(100, 300);
            while(true) {
                Thread.Sleep(ms);
                for(int i = 0; i <= length; i++) {
                    if(pos - i >= height) continue;
                    if(i==length&&pos - i >= 0) {
                        Sign.PrintHole(left, pos - i);
                    }
                    else if(pos - i >= 0) {
                        if(i == 0) {
                            Sign.PrintWhite(left, pos - i);
                        } else if(i == 1) {
                            Sign.PrintLightGreen(left, pos - i);
                        } else {
                            Sign.PrintGreen(left, pos - i);
                        }
                    } else break;

                }
                pos++;
                if(pos >= height + length) {
                    //for(int i = 0; i < height; i++) {
                    //    Sign.PrintHole(left,i);
                    //}
                    pos = 0;
                }
            }
        }
    }
}
