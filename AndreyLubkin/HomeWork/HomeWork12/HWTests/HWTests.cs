using NUnit.Framework;
using HomeWorkBL;
using System;

namespace Tests {
    [TestFixture]
    public class HWTests {
        //PartOne
        [Test]
        [TestCase(2, 3, 6, 3.67)]
        [TestCase(26, 34, 5, 21.67)]
        [TestCase(42, 42, 42, 42)]
        public void avarange_Gets3Numbers_returnedAvarangeNumber(int a, int b, int c, double expected) {
            var result = Avarange.avarange(a, b, c);

            Assert.AreEqual(result, expected);
        }
        [Test]
        [TestCase(4, 6, "+", 10)]
        [TestCase(8, 3, "-", 5)]
        [TestCase(3, 6, "*", 18)]
        [TestCase(100, 10, "/", 10)]
        public void Calc_GetsNum1Num2Operator_returnedDecision(int num1, int num2, string op, double expected) {
            var result = Calculations.Calc(op, num1, num2);

            Assert.AreEqual(result, expected);
        }
        [Test]
        [TestCase(2, 3, 6, 3.67)]
        [TestCase(26, 34, 5, 21.67)]
        [TestCase(42, 42, 42, 42)]
        public void avarangeDelArr_GetsDelegatesArr_returnedAvarange(int a, int b, int c, double expected) {
            Func<int>[] funcs = { () => a, () => b, () => c };
            var result = DelArr.avarangeDelArr(funcs);

            Assert.AreEqual(result, expected);
        }

        //PartTwo
        [Test]
        [TestCase(1, true)]
        [TestCase(4, false)]
        [TestCase(233, true)]
        [TestCase(222, false)]
        [TestCase(2584, true)]
        public void IsFibonacci_CheckNumIsFibonacci_returnedBool(long num, bool expected) {
            FibDel fibonacciDelegate = Fibonacci.IsFibonacci;
            var result = fibonacciDelegate.Invoke(num);
            Assert.AreEqual(result, expected);
        }
    }
}