﻿using System;

namespace Tools {
    public class Tool {
        public static int GetCheckNumber(string question) {
            int number = 0;

            while(true) {
                try {
                    Console.Write(question+" ");
                    number = Int32.Parse(Console.ReadLine());
                    Console.WriteLine();
                    break;
                }
                catch {
                    Console.WriteLine("Введите корректное число!");
                }
            }
            return number;
        }
    }
}
