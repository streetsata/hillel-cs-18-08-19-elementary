﻿using System;
using HomeWorkBL;
using Tools;

namespace HomeWork {
    class Program {
        static void Main(string[] args) {
            //PartOne
            Task1_1();
            Task2_1();
            Task3_1();
        }

        static void Task1_1() {
            Console.WriteLine(Avarange.avarange(26, 34, 5));
        }
        static void Task2_1() {
            int num1 = Tool.GetCheckNumber("Введите первое число:");
            int num2 = Tool.GetCheckNumber("Введите второе число:");
            Console.Write("Введите оператор: ");
            string op = Console.ReadLine();
            try {
                Console.WriteLine($"Решение: {Calculations.Calc(op, num1, num2)}");
            }
            catch {
                Console.WriteLine("Извините, вы допустили ошибку");
            }
        }
        static void Task3_1() {
            Func<int>[] funcs = { () => new Random().Next(0, 100), () => new Random().Next(0, 100), () => new Random().Next(0, 100) };
            double res = DelArr.avarangeDelArr(funcs);
            double res2 = DelArr.avarangeDelArr(funcs);
            Console.WriteLine(res);
        }
    }
}
