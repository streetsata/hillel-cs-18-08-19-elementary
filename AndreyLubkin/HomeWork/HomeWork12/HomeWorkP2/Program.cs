﻿using HomeWorkBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWorkP2 {
    class Program {
        [STAThread]
        static void Main(string[] args) {
            //PartTwo
            Task1_2();
            Task2_2();
            Task3_2();
        }

        private static void Task2_2() {
            Console.WriteLine(Fibonacci.IsFibonacci(4));
        }

        static void Task1_2() {
            ConsoleStopwatch stopwatch = new ConsoleStopwatch();
            stopwatch.Launch();
        }
        static void Task3_2() {
            var window = new Window();
            Application.Run(window);
        }
    }
}
