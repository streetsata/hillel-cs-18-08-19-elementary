﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeWorkP2 {
    public class ConsoleStopwatch {
        bool swStatus = false;
        DateTime before;
        TimeSpan spendTime;
        Keybord keybord;
        public void Launch() {
            Console.WriteLine("Запустить секундомер - S\nОстановить секундомер - D");
            keybord = new Keybord();
            keybord.PressKeyS += Keybord_PressKeySEvent;
            keybord.PressKeyD += Keybord_PressKeyDEvent;
            keybord.Start();
        }

        private void Keybord_PressKeySEvent() {
            Console.Clear();
            Console.WriteLine("Секундомер запущен (D - Остановить)");
            before = DateTime.Now;
        }
        private void Keybord_PressKeyDEvent() {
            if(!swStatus) Console.WriteLine("Секундомер не запущен!");
            Console.Clear();
            spendTime = DateTime.Now - before;
            Console.WriteLine($"Секундомер насчитал: {spendTime} (Q - Выйти)");
        }
    }
    public delegate void PressKeyEventHandler();
    public class Keybord {
        public event PressKeyEventHandler PressKeyS = null;
        public event PressKeyEventHandler PressKeyD = null;


        private void PressKeySEvent() {
            if(PressKeyS != null) {
                { PressKeyS.Invoke(); }
            }
        }
        private void PressKeyDEvent() {
            if(PressKeyD != null) {
                { PressKeyD.Invoke(); }
            }
        }

        public void Start() {
            bool isTrue = true;
            while(isTrue) {
                ConsoleKeyInfo key = Console.ReadKey(true);
                switch(key.Key) {
                    case ConsoleKey.S:
                        PressKeySEvent();
                        break;
                    case ConsoleKey.D:
                        PressKeyDEvent();
                        break;
                    case ConsoleKey.Q:
                        isTrue = false;
                        break;
                }
            }
        }
    }
    public class Window : Form {
        Button loaf1;
        Button loaf2;
        public Window() {
            //Настройка формы
            base.AutoSize = false;
            base.Size = new Size(240, 150);

            //Настройка компонентов
            loaf1 = new Button();
            loaf1.Location = new Point(20, 35);
            loaf1.Text = "Синий";

            loaf2 = new Button();
            loaf2.Location = new Point(120, 35);
            loaf2.Text = "Красный";

            loaf1.Click += (sender, e) => base.Close(); 
            loaf2.Click += (sender, e) => base.Size = new Size((int)(base.Size.Width + base.Size.Width * 0.1), (int)(base.Size.Height + base.Size.Height * 0.1));

            //Добавление компонентов
            Controls.Add(loaf1);
            Controls.Add(loaf2);

            Text = "Морфеус";
            FormBorderStyle = FormBorderStyle.FixedDialog;
        }
    }
}
