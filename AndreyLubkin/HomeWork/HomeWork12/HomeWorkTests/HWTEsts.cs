using NUnit.Framework;
using HomeWorkBL;

namespace Tests {
    [TestFixture]
    public class Tests {
        [SetUp]
        public void Setup() {
        }

        [Test]
        [TestCase(2,3,6)]
        public void avarange_Gets3Numbers_returnedAvarangeNumber(int a, int b, int c, double expected) {
            var result = Avarange.avarange(a, b, c);

            Assert.AreEqual(result, expected);
        }
    }
}