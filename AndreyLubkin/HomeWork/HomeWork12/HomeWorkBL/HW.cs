﻿using System;

namespace HomeWorkBL {
    public class Avarange {
        public static Func<int, int, int, double> avarange;
        static Avarange() {
            avarange = delegate (int a, int b, int c) {
                return Math.Round(((double)(a + b + c) / 3),2);
            };
        }
    }
    public class Calculations {
        private static Func<int, int, double> Add;
        private static Func<int, int, double> Sub;
        private static Func<int, int, double> Mul;
        private static Func<int, int, double> Div;
        static Calculations() {
            Add = (a, b) => { return a + b; };
            Sub = (a, b) => { return a - b; };
            Mul = (a, b) => { return a * b; };
            Div = (a, b) => {
                if(b == 0) throw new DivideByZeroException();
                return a / b;
            };
        }
        public static double Calc(string op, int num1, int num2) {
            switch(op) {
                case "+":
                    return Add(num1, num2);
                case "-":
                    return Sub(num1, num2);
                case "*":
                    return Mul(num1, num2);
                case "/":
                    return Div(num1, num2);
                default:
                    throw new Exception("Operator doesn't exists");
            }
        }
    }
    public class DelArr {
        public static Func<Func<int>[], double> avarangeDelArr;
        static DelArr() {
            avarangeDelArr = delegate (Func<int>[] funcs) {
                int adder = 0;
                foreach(var func in funcs) {
                    adder += func.Invoke();
                }
                return Math.Round(((double)adder / funcs.Length),2);
            };
        }
    }


    public delegate bool FibDel(long num);
    public class Fibonacci {
        public static bool IsFibonacci(long num) {
            double root5 = Math.Sqrt(5);
            double phi = (1 + root5) / 2;

            long index = (long)Math.Floor(Math.Log(num * root5) / Math.Log(phi) + 0.5);
            long indexFib = (long)Math.Floor(Math.Pow(phi, index) / root5 + 0.5);

            return (indexFib == num);
        }
    }
}
