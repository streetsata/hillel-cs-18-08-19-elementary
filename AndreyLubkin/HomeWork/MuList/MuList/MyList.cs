﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MuList {
    class MyList<T> :IList {

        private T[] ListArr;
        private int size;
        private int defaultSize = 10;
        public MyList() {
            ListArr = new T[defaultSize];
            size = 0;
        }
        object IList.this[int index] {
            get {
                if(!(0 <= index && index < size)) throw new Exception("Out of border");
                return ListArr[index];
            }
            set {
                if(!(0 <= index && index < size)) throw new Exception("Out of border");
                ListArr[index] = (T)value;
            }
        }

        public bool IsFixedSize { get { return false; } }

        public bool IsReadOnly { get { return false; } }

        public int Count { get { return size; } }

        public bool IsSynchronized { get { return true; } }

        public object SyncRoot { get { return this; } }

        public int Add(object value) {
            if(ListArr.Length == size) {
                T[] newArr = new T[ListArr.Length * 2];
                Array.Copy(ListArr, 0, newArr, 0, size);
                ListArr = newArr;
            }
            ListArr[size++] = (T)value;
            return size-1;
        }

        public void Clear() {
            size = 0;
        }

        public bool Contains(object value) {
            for(int i = 0; i < ListArr.Length; i++) {
                if(EqualityComparer<T>.Default.Equals(ListArr[i], (T)value)) return true;
            }
            return false;
        }

        public void CopyTo(Array array, int index) {
            if(!(0 <= array.Length-index && array.Length - index < size)) throw new Exception("Out of border");
            Array.Copy(array, index, ListArr, 0, size-1);
        }

        public IEnumerator GetEnumerator() {
            return new MyListEnumerator<T>(ListArr, size);
        }

        public int IndexOf(object value) {
            for(int i = 0; i < ListArr.Length; i++) {
                if(EqualityComparer<T>.Default.Equals(ListArr[i], (T)value)) return i;
            }
            return -1;
        }

        public void Insert(int index, object value) {
            if(!(0 <= index && index <= size)) throw new Exception("Out of border");
            if(index == size) {
                Add(value);
            } else {
                ListArr[index] = (T)value;
            }
        }

        public void Remove(object value) {
            RemoveAt(IndexOf(value));
        }

        public void RemoveAt(int index) {
            if(!(0 <= index && index < size)) throw new Exception("Out of border");
            T[] newArr = new T[ListArr.Length];
            Array.Copy(ListArr, 0, newArr, 0, index);
            Array.Copy(ListArr, index + 1, newArr, index, size);
            ListArr = newArr;
            size--;
        }
    }
    class MyListEnumerator<T> :IEnumerator {
        private T[] ListArr;
        private int size;
        private int pos = -1;

        public MyListEnumerator(T[] ListArr, int size) {
            this.ListArr = ListArr;
            this.size = size;
        }

        public object Current {
            get {
                if(pos == -1 || pos >= size)
                    throw new InvalidOperationException();
                return ListArr[pos];
            }
        }

        public bool MoveNext() {
            if(pos < size - 1) {
                pos++;
                return true;
            } else
                return false;
        }

        public void Reset() {
            pos = -1;
        }
    }
}
