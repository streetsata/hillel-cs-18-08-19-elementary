# Task 002

### Вопросы

- You want to store information about the employees that work in my company. Is this a job for a value type or a reference type?
--reference type

- Can an object be referred to by more than one reference?
--Нет, ссылочный объект не можжет ссылаться на более чем одну ссылку. Была бы неопределенность по факту

- What happens when an object no longer has any references to it?
--Его удаляет автоматические сборщик мусора

- If you use a value type rather than a reference type, will your program still work correctly?
--Это зависит от самого проекта и функции что он выполняет. Если код работает правильно, от использования value type он не будет работать правильнее)

- How can you make a data type immutable?
--Используя константу. Readonly можно присвоить в конструкторе, поэтому он не совсем подходит

- Are value types more efficient that reference types?
--Опять же, очень зависит от ситуации. нельзя сказать однозначно что из них работает более эффективно, но я нашел целое исследование по этому поводу где говориться в каких случаях лучше использовать reference types, а в каких value types.
>https://medium.com/@mdfarragher/whats-faster-in-c-a-struct-or-a-class-99e4761a7b76

- What is the difference between the stack and the heap?
--Стек это область в оперативной памяти, она работает по принципу "Последний зашел, первый вышел". Куча это хранилище памяти которое тоже находится в ОЗУ. Однако Куча допускает динамическое выделение памяти и не работает по принципу стека(FIFO). Грубо говоря склад для данных.

- Do you need to add a constructor method for every object that you create?
--Нет, это не обязательно. Некоторые переменный могут иметь стандартное заданное значение, а некоторые могут присваиватся во время жизненного цикла объекта и тд.

- Is there ever any point in creating objects that cannot be constructed?
--Есть если например метод делает какие то математические операции как Math. Он работает без конструктора

- What does it mean when a member of a type is made static?
--Это значит что он будет доступен на уровне класса

- Should you provide default values for all of the parameters to a method?
--Нет, это делать не обязательно. Параметры можно задавать в конструкторе или во время жизненного цикла объекта 

- Does using a lot of overridden methods slow a program down?
--На скорость работы override метод влиять не должен
