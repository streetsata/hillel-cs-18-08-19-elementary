﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegates {
    public delegate int FindChar(char ch, string str);
    public class DelegateTask {
        public static int FindCharIndex(char ch, string str) {
            for(int i = 0; i < str.Length; i++) {
                if(str[i] == ch) return i;
            }
            return -1;
        }
        public static int CountChars(char ch, string str) {
            int counter = 0;
            for(int i = 0; i < str.Length; i++) {
                if(str[i] == ch) counter++;
            }
            return counter;
        }
    }
}
