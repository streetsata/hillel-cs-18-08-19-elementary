﻿using System;

namespace Delegates {
    public delegate bool FibDel(long num);
    public class Fibonacci {
        public static bool IsFibonacci(long num) {
            double root5 = Math.Sqrt(5);
            double phi = (1 + root5) / 2;

            long index = (long)Math.Floor(Math.Log(num * root5) / Math.Log(phi) + 0.5);
            long indexFib = (long)Math.Floor(Math.Pow(phi, index) / root5 + 0.5);

            return (indexFib == num);
        }
    }

}
