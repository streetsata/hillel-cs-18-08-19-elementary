using NUnit.Framework;
using Delegates;
using System;

namespace Tests {
    [TestFixture]
    public class DelegatesTests {
        //FindChar findChar;
        Delegate[] delegates;

        [SetUp]
        public void Setup() {
            FindChar findChar = DelegateTask.FindCharIndex;
            findChar += DelegateTask.CountChars;
            delegates = findChar.GetInvocationList();
        }

        [Test]
        [TestCase(1, true)]
        [TestCase(4, false)]
        [TestCase(233, true)]
        [TestCase(222, false)]
        [TestCase(2584, true)]
        public void IsFibonacci_CheckNumIsFibonacci_returnedBool(long num,bool expected) {
            FibDel fibonacciDelegate = Fibonacci.IsFibonacci;
            var result = fibonacciDelegate.Invoke(num);
            Assert.AreEqual(result, expected);
        }
        [Test]
        [TestCase('o',"Hello world",4)]
        [TestCase('o', "Somebody once told me",1)]
        [TestCase('a', "Howdyho", -1)]
        public void FindCharIndex_GetsCharAndString_returnIndex(char ch,string str,int expected) {
            var result = delegates[0].DynamicInvoke(ch,str);
            Assert.AreEqual(result, expected);
        }
        [Test]
        [TestCase('o', "Hello world", 2)]
        [TestCase('o', "Somebody once told me", 4)]
        [TestCase('a', "Howdyho", 0)]
        public void CountChars_GetsCharAndString_returnCountOfChars(char ch, string str, int expected) {
            var result = delegates[1].DynamicInvoke(ch, str);
            Assert.AreEqual(result, expected);
        }
    }
}