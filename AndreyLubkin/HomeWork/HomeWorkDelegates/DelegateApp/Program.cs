﻿using System;
using Delegates;

namespace DelegateApp {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine(Fibonacci.IsFibonacci(4));
            FindChar findChar = DelegateTask.FindCharIndex;
            findChar += DelegateTask.CountChars;
            //var result = findChar.Invoke('o', "Hello world");
            Delegate[] delegates = findChar.GetInvocationList();
            Console.WriteLine(delegates[0].DynamicInvoke('o', "Hello world"));
            Console.WriteLine(delegates[1].DynamicInvoke('o', "Hello world"));
            //Console.WriteLine(result);
        }
    }
}
