﻿using System;

namespace HomeWork8 {
    abstract class AbstractHandler {
        public abstract void Open();
        public abstract void Create();
        public abstract void Change();
        public abstract void Save();
    }
    class XMLHandler :AbstractHandler {
        public override void Change() {
            Console.WriteLine("Изменение XML документа");
        }

        public override void Create() {
            Console.WriteLine("Создание XML документа");
        }

        public override void Open() {
            Console.WriteLine("Открытие XML документа");
        }

        public override void Save() {
            Console.WriteLine("Сохранение XML документа");
        }
    }
    class TXTHandler :AbstractHandler {
        public override void Change() {
            Console.WriteLine("Изменение TXT документа");
        }

        public override void Create() {
            Console.WriteLine("Создание TXT документа");
        }

        public override void Open() {
            Console.WriteLine("Открытие TXT документа");
        }

        public override void Save() {
            Console.WriteLine("Сохранение TXT документа");
        }
    }
    class DOCHandler :AbstractHandler {
        public override void Change() {
            Console.WriteLine("Изменение DOC документа");
        }

        public override void Create() {
            Console.WriteLine("Создание DOC документа");
        }

        public override void Open() {
            Console.WriteLine("Открытие DOC документа");
        }

        public override void Save() {
            Console.WriteLine("Сохранение DOC документа");
        }
    }
}
