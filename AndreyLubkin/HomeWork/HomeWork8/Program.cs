﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HomeWork8 {
    abstract class Worker :IComparable,IEnumerable {
        public List<string> jobTitle = new List<string>();
        public int CompareTo(object obj) {
            Worker w = obj as Worker;
            if(w != null)
                return this.Salary().CompareTo(w.Salary());
            else
                throw new Exception("Невозможно сравнить два объекта");
        }

        public IEnumerator GetEnumerator() {
            return jobTitle.GetEnumerator();
        }

        public abstract decimal Salary();
    }

    class FixedPaymentWorker :Worker {
        public decimal FixedPayment { get; set; }
        public FixedPaymentWorker(decimal fixedPayment) {
            this.FixedPayment = fixedPayment;
        }
        public override decimal Salary() {
            return FixedPayment;
        }
    }
    class HourlyPaymentWorker :Worker {
        public decimal HourlyPayment { get; set; }
        public HourlyPaymentWorker(decimal hourlyPayment) {
            this.HourlyPayment = hourlyPayment;
        }
        public override decimal Salary() {
            return (decimal)(20.8 * 8 * (double)HourlyPayment);
        }
    }

    class Program {
        static void Main(string[] args) {
            Task1();
            Task2();
            Task3();
        }

        private static void Task1() {
            AbstractHandler[] handlerArr = new AbstractHandler[3];
            handlerArr[0] = new XMLHandler();
            handlerArr[1] = new TXTHandler();
            handlerArr[2] = new DOCHandler();
            for(int i = 0; i < handlerArr.Length; i++) {
                handlerArr[i].Create();
                handlerArr[i].Open();
                handlerArr[i].Change();
                handlerArr[i].Save();
                Console.WriteLine();
            }
        }
        private static void Task2() {
            Player player = new Player();
            player.Stop();
            player.Pause();
            player.Play();
            player.Pause();
            player.Play();
            player.Stop();

            Console.WriteLine();
            player.Record();
            player.Pause();
            player.Stop();
        }
        private static void Task3() {
            Console.WriteLine();
            Worker[] workers = new Worker[5];
            FillWorkers(ref workers);

            ShowWorkers(workers);
            Console.WriteLine("=============");
            Array.Sort(workers);

            ShowWorkers(workers);
        }

        private static void ShowWorkers(Worker[] workers) {
            for(int i = 0; i < workers.Length; i++) {
                Console.WriteLine($"Работник {i + 1}");
                Console.WriteLine($"Месячная плата: {workers[i].Salary()}");
                Console.WriteLine("Должностя:");
                foreach(var item in workers[i]) {
                    Console.WriteLine(" " + item);
                }
            }
        }

        private static void FillWorkers(ref Worker[] workers) {
            workers[0] = new FixedPaymentWorker(20000);
            workers[0].jobTitle.Add("Должность 1");
            workers[0].jobTitle.Add("Должность 2");
            workers[1] = new HourlyPaymentWorker(200);
            workers[1].jobTitle.Add("Должность 3");
            workers[2] = new FixedPaymentWorker(22000);
            workers[2].jobTitle.Add("Должность 1");
            workers[2].jobTitle.Add("Должность 4");
            workers[3] = new HourlyPaymentWorker(260);
            workers[3].jobTitle.Add("Должность 2");
            workers[3].jobTitle.Add("Должность 5");
            workers[4] = new HourlyPaymentWorker(300);
            workers[4].jobTitle.Add("Должность 1");

        }
    }
}
