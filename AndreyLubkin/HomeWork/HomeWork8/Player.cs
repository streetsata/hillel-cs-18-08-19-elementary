﻿using System;

namespace HomeWork8 {
    interface IPlayable {
        void Play();
        void Pause();
        void Stop();
    }
    interface IRecodable {
        void Record();
        void Pause();
        void Stop();
    }
    class Player :IPlayable, IRecodable {
        private bool isPlaing;
        private bool isRecording;
        private bool isPaused;

        public Player() {
            isPlaing = false;
            isRecording = false;
            isPaused = false;
        }

        public void Play() {
            if(!isRecording && !isPlaing) {
                Console.WriteLine("Запуск проигрывания");
                isPlaing = true;
            } else if(isPlaing && isPaused) {
                Console.WriteLine("Проигрывание возобновлено");
                isPaused = false;
            } else if(isPlaing) {
                Console.WriteLine("Проигрывание уже работает");
            } else {
                Console.WriteLine("Отключите запись для проигрывания");
            }
        }

        public void Record() {
            if(!isRecording && !isPlaing) {
                Console.WriteLine("Запуск записи");
                isRecording = true;
            } else if(isRecording && isPaused) {
                Console.WriteLine("Запись возобновлена");
                isPaused = false;
            } else if(isRecording) {
                Console.WriteLine("Запись уже работает");
            } else {
                Console.WriteLine("Отключите проигрывание для записи");
            }
        }

        public void Pause() {
            if(isRecording && !isPaused) {
                Console.WriteLine("Запись приостановлена");
                isPaused = true;
            } else if(isRecording && isPaused) {
                Console.WriteLine("Запись уже на паузе");
            } else if(isPlaing && !isPaused) {
                Console.WriteLine("Проигрывание приостановлена");
                isPaused = true;
            } else if(isPlaing && isPaused) {
                Console.WriteLine("Проигрывание уже на паузе");
            } else {
                Console.WriteLine("Нечего ставить на паузу");
            }
        }

        public void Stop() {
            if(isRecording) {
                Console.WriteLine("Запись остановлена");
                isRecording = false;
                isPaused = false;
            } else if(isPlaing) {
                Console.WriteLine("Проигрывание остановлено");
                isPlaing = false;
                isPaused = false;
            } else {
                Console.WriteLine("Нечего останавливать");
            }
        }
    }
}
