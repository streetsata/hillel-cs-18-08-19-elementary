﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTest.ViewModels {
    public class CalcViewModel {
        public int? num1 { get; set; }
        public int? num2 { get; set; }
    }
}
