﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTest.ViewModels {
    public class NameViewModel {
        public string Name { get; set; }
        public int? Age { get; set; }
    }
}
