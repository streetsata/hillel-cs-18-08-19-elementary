﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Text.Json.Serialization;
using ApiTest.ViewModels;

namespace ApiTest.Controllers
{
    /// <summary>
    /// Контроллер /Sketch
    /// </summary>
    [Produces("application/json")]
    [Route("[controller]")]
    [ApiController]
    public class SketchController : ControllerBase
    {
        /// <summary>
        /// Index response
        /// </summary>
        /// <returns>Возвращает тестовый json</returns>
        [HttpGet]
        public JsonResult Index() {
            return new JsonResult(new { Test = "Awesome" });
        }

        /// <summary>
        /// Создает json объект на основе принимаемых значений
        /// </summary>
        /// <param name="nameModel">Принимает Name и Age(необязательно)</param>
        /// <returns>Возвращает json</returns>
        [HttpPost]
        [Route("post")]
        public JsonResult postFunction([FromBody] NameViewModel nameModel) {
            var model = nameModel;
            if(nameModel.Age == null) {
                return new JsonResult(new { success = true, result = $"Hello! My name is {nameModel.Name}" });
            } else {
                return new JsonResult(new { success = true, result = $"Hello! My name is {nameModel.Name}", age = $"I'm {nameModel.Age} year old" });
            }
        }

        /// <summary>
        /// Вычесляет сумму чисел
        /// </summary>
        /// <param name="calcModel">Принимает num1 и num2</param>
        /// <returns>json объект с вычислениями и результатом</returns>
        [HttpPost]
        [Route("calc")]
        public JsonResult Calculate([FromBody] CalcViewModel calcModel) {
            return new JsonResult(new { result = $"{calcModel.num1} + {calcModel.num2} = {calcModel.num1+calcModel.num2}" });
        } 
    }
}