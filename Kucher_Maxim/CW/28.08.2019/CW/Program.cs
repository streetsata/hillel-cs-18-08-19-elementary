﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW
{
    struct MyStryct
    {
        public int MyProperty { get; set; }
        public int MyProperty1 { get; set; }

        public MyStryct(int x, int y)
        {
            MyProperty = x;
            MyProperty1 = y;
        }
    }

    static class MyClass
    {
        public static int MyProperty { get; set; }
        private static int temp;
    }
    class Enemy
    {
        public int x { get; private set; }

        public int y { get; private set; }

        public int Lives { get; private set; }

        private static int MaxLives = 99;
        static Enemy()
        {
            Console.WriteLine("Call Static ctor");
        }

        //Default constuctor
        public Enemy()
        {
            x = 10;
            y = 50;
            Lives = 3;
        }

        // User constructor
        public Enemy(int X)

        {
            x = X;
            Lives = 3;
        }
        public Enemy(int X, int Y)
            :this(X)
        {
            y = Y;
            Lives = 3;
        }
        public Enemy(int X, int Y, int lives)
            : this(X,Y)
        {
 
            Lives = lives;
        }
        public override string ToString()
        {
            return $"X: {x}, Y: {y}, Lives: {Lives}";
        }
    }



    class MyStack<TValue> // where TValue : struct or class or new() or Random определение какие типы можно использовать
    {
        private TValue[] items = new TValue[100];
        int stackTop = 0;
        public void Push(TValue item)
        {
            if (stackTop == items.Length)
                throw new Exception("Stack full");
            items[stackTop] = item;
            stackTop++;
        }
        public TValue Pop()
        {
            stackTop--;
            return items[stackTop];
        }
    }

    public static class ExtensionMethods
    {
        public static int LineCount(this string str)
        {
            return str.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }
        public static int IndexOF(this StringBuilder str, char ch)
        {
            for(int i = 0; i <str.Length; i++)
            {
                if (str[i] == ch) return i;
            }
            return -1;
        }
    }
    
    class Program
    {
        //static void Method(int age, int height)
        //{
        //    Console.WriteLine(age + height);
        //}
        static void Main(params string[] args)
        {
            string str = @"first line
second line
third line
fourth line";
            StringBuilder stringBuilder = new StringBuilder("Hello, World!");

            Console.WriteLine(str.LineCount());





















            //foreach(var item in args)
            //{
            //    Console.WriteLine(item);
            //}



            //Enemy name = new Enemy(50, 100);
            //Console.WriteLine(name);
            //Method(height: 10, age: 5);
            





















            //MyStack<int> stack = new MyStack<int>();
            //stack.Push(1);
            //stack.Push(2);
            //stack.Push(3);
            //stack.Push(4);
            //stack.Push(5);
            //Console.WriteLine(stack.Pop());
            //Console.WriteLine(stack.Pop());
            //Console.WriteLine(stack.Pop());
            //Console.WriteLine(stack.Pop());
            //Console.WriteLine(stack.Pop());

            //MyStack<string> myStack = new MyStack<string>();
            //myStack.Push("One");
            //myStack.Push("Two");
            //myStack.Push("Three");
            //Console.WriteLine(myStack.Pop());
            //Console.WriteLine(myStack.Pop());
            //Console.WriteLine(myStack.Pop());


        }
    }
}
