﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Enemy
    {
        public int hp = 100;
        public int damage = 10;
        public int speed = 1;
    }
    class Monster : Enemy
    {
       
        public Monster()
        {
            Console.WriteLine("Create monster");
        }
    }

    class MeeleMonster : Monster
    {
        public MeeleMonster()
        {
            Console.WriteLine("Meele atack, speed = " + speed);
        }
    }

    class RangeMonster : Monster
    {
        public RangeMonster()
        {
            
            damage = 20;
            Console.WriteLine("Range atack is " + damage);
        }
        
    }
    class Program
    {
        static void Main(string[] args)
        {
            MeeleMonster meeleMonster = new MeeleMonster();
            RangeMonster rangeMonster = new RangeMonster();
        }
    }
}
