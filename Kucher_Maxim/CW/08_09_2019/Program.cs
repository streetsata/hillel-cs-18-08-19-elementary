﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08_09_2019
{
    //abstract class Transport
    //{
    //    double CarryingCapacing { get; set; }
    //    double Speed { get; set; }
    //    double Mass { get; set; }
    //    public abstract bool Move();
        


    //}

    //abstract class GasEngine : Transport
    //{
    //    public double Cunsumption { get; set;}
       
    //}

    //class CarAudi : GasEngine
    //{
    //    public override bool Move()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    class Transport
    {

    }

    class Lada : Transport {
        public Transport transport { get; set; }
    }

    class Audi : Transport { }


    class BaseClass
    {
        public int a;
    }

    class DerivedClass : BaseClass
    {
        public int b;
    }

    class AnoterDerivedClass : BaseClass
    {
        public int c;
    }
    class Program
    {
        static void Main(string[] args)
        {
            //Transport carAudi = new CarAudi();
            //carAudi.Move();
            //Lada lada = new Lada();
            //lada.transport = new Transport();

            //Transport transport = (Transport)lada;
            //transport.

            DerivedClass derivedClass = new DerivedClass();
            BaseClass baseClass = derivedClass;
            BaseClass anotherDerivedClass = new AnoterDerivedClass();
        }
    }
}
