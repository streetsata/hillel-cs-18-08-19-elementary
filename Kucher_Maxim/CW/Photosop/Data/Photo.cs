using System;

namespace MyPhotoshop
{
	public class Photo
	{
		public int width;
		public int height;
		public Pixel[,] data;

        public Pixel this[int index1, int index2]
        {
            get { return data[index1, index2]; }
            set { data[index1, index2] = value; }
        }
	}
}

