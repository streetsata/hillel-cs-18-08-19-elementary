﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPhotoshop
{
    public class Pixel
    {
        private double r;
        private double g;
        private double b;

        public double R { get { return r; }  set{r = Check(value);} }
        public double G { get { return g; }  set{g = Check(value);} }
        public double B { get { return b; }  set{b = Check(value);} }
        private double Check(double v)
        {
            if (v >= 0 && v <= 1)  return v;
            else throw new Exception();
        }

        public static double PixelCheck(double v)
        {
            if (v < 0) return v = 0;
            if(v>1) return v = 1;
            return v;
        }
    }
}
