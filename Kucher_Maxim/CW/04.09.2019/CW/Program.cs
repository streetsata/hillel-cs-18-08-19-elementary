﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW
{
    class Program
    {
        class IntArrayWrapper
        {
            private int[] arr = new int[100];
            public int this[int i] {
                get { return arr[i]; }
                set { arr[i] = value; }
            }
        }

        class NamesArray
        {
            private int[] arr = new int[100];

            public int this[string name]
            {
                get
                {
                    switch (name)
                    {
                        case "zero":
                            return arr[0];
                        case "one":
                            return arr[1];
                        default:
                            return -1;     
                    }
                }
                set
                {
                    switch (name)
                    {
                        case "zero":
                            arr[0] = value;
                            break;
                        case "one":
                            arr[1] = value;
                            break;
                       
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            IntArrayWrapper x = new IntArrayWrapper();
            x[0] = 99;
            Console.WriteLine(x[0]);

            NamesArray y = new NamesArray();
            y["zero"] = 99;
            Console.WriteLine(y["zero"]);

            //boxing unboxing
            {
                object o = 99;
                int oVal = (int)o; //достаём из кучи и кладём в стэк значение
            }


            //Console.WriteLine(IsCorrectString("(([])[])"));
            //Console.WriteLine(IsCorrectString("((][])"));
            //Console.WriteLine(IsCorrectString("((("));
            //Console.WriteLine(IsCorrectString("(x)"));
            Console.WriteLine(IsCorrectString("23344332"));
        }
        private static bool IsCorrectString(string str)
        {
            var stack = new Stack<string>();
            for (int i = 0, j = i + 1; i < str.Length - 1 && j < str.Length; i += 2, j+=2)
            {
                var temp = str[i].ToString() + str[j].ToString();
                switch (temp)
                {
                    case "23":
                    case "34":
                        stack.Push(temp);
                        break;
                    case "32":
                        if (stack.Count == 0) return false;
                        if (stack.Pop() != "23") return false;
                        break;
                     
                    case "43":
                        if (stack.Count == 0) return false;
                        if (stack.Pop() != "34") return false;
                        break;
                    default:
                        return false;
                }
                
            }
            return stack.Count == 0;
        }
    }
}
