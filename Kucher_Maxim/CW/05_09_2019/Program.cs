﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_09_2019
{
    //class Enemy
    //{
    //    private int b;
    //    public int a;
    //    public void Func()
    //    {
    //        Console.WriteLine("Hello from Base Class " + a);
    //    }

    //}

    //class Monster : Enemy
    //{
    //    public Monster()
    //    {
    //        a = 5;
    //        //Func();
    //    }
    //}

    //class FlyMonster : Monster
    //{
    //    public FlyMonster()
    //    {
    //        Func();
    //    }
    //}

        class BaseClass
    {
        private int a, b;
        public BaseClass(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
        protected void Method()
        {
            Console.WriteLine("Hello from BaseClass.Method");
        }
    }
        
        class DerivedClass : BaseClass
    {
        int c;
        public DerivedClass(int a, int b, int c) : base(a,b)
        {

        }
        public void Method()
        {
            Console.WriteLine("Hello from Derivedclass.Method");
        }
    }
    class Program
    {

        
        static void Main(string[] args)
        {
            DerivedClass derivedClass = new DerivedClass(5,10,15);
            derivedClass.Method();
            //Monster monster = new Monster();
            //FlyMonster flyMonster = new FlyMonster();
        }
    }
}
