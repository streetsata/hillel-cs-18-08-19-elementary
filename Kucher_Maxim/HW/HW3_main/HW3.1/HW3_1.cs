﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3._1
{
    class HW3_1
    {
        private static bool IsCorrectString(string str)
        {
            var stack = new Stack<string>();
            if (str.Contains("2") && str.Contains("3") && str.Contains("4"))
            {
                for (int i = 0, j = i + 1; i < str.Length - 1 && j < str.Length; i += 2, j += 2)
                {
                    var temp = str[i].ToString() + str[j].ToString();
                    switch (temp)
                    {
                        case "23":
                        case "34":
                            stack.Push(temp);
                            break;
                        case "32":
                            if (stack.Count == 0) return false;
                            if (stack.Pop() != "23") return false;
                            break;

                        case "43":
                            if (stack.Count == 0) return false;
                            if (stack.Pop() != "34") return false;
                            break;
                        default:
                            return false;
                    }

                }
            }else
            {
               for(int i = 0; i < str.Length; i++)
                {
                    var temp = str[i].ToString();
                    switch(temp)
                    {
                        case"(":
                        case "[":
                            stack.Push(temp);
                            break;
                        case ")":
                            if (stack.Count == 0) return false;
                            if (stack.Pop() != "(") return false;
                            break;
                        case "]":
                            if (stack.Count == 0) return false;
                            if (stack.Pop() != "[") return false;
                            break;
                    }
                }
            }
            return stack.Count == 0;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(IsCorrectString("(([])[])"));
            Console.WriteLine(IsCorrectString("((][])"));
            Console.WriteLine(IsCorrectString("((("));
            Console.WriteLine(IsCorrectString("(x)"));
            Console.WriteLine(IsCorrectString("23344332"));

        }
    }
}
