﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    class MyStack<TValue> 
    {
        public TValue[] items = new TValue[5];
        int stackTop = 0;
        public void Push(TValue item)
        {
            if (stackTop >= items.Length)
            {
                for (int i = 0; i < items.Length - 1; i++)
                {
                    items[i] = items[i + 1];
                }
                stackTop--;
            }
                items[stackTop] = item;
                stackTop++;
            
        }
        public TValue Pop()
        {
            stackTop--;
            return items[stackTop];
        }
    }
   

    class HW3_2
    {
        static void Main(string[] args)
        {

            MyStack<int> stack = new MyStack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(4);
            stack.Push(5);
            stack.Push(6);
            stack.Push(7);
            stack.Push(8);
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());

            MyStack<string> myStack = new MyStack<string>();
            myStack.Push("One");
            myStack.Push("Two");
            myStack.Push("Three");
            myStack.Push("Four");
            myStack.Push("Five");
            myStack.Push("Six");
            myStack.Push("Seven");
            Console.WriteLine(myStack.Pop());
            Console.WriteLine(myStack.Pop());
            Console.WriteLine(myStack.Pop());
            Console.WriteLine(myStack.Pop());
            Console.WriteLine(myStack.Pop());
        }
    }
}
