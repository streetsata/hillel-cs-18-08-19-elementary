﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3._3
{
    public class MyList<T>
    {
        private T[] items = new T[100];
        private int stackTop = 0;
        public T this[int i]
        {
            get { return items[i]; }
        }

        public int StackTop {
            get
            {
                return stackTop;
            }

        }
        public void Push(T item)
        {
            if (stackTop == items.Length)
                Console.WriteLine("List is full");

            items[stackTop] = item;
            stackTop++;
        }
        public T Pop()
        {
            stackTop--;
            return items[stackTop];
        }
    }


        public static class Extension
        {
            public static T[] GetArray<T>(this MyList<T> List)
            {
            T[]  x = new T[100];
            Console.WriteLine("Output emenents from the extension method: ");
            for(int i =0;i < 5; i++) {  x[i] = List[i]; }
            for (int i = 0; i < 5; i++) { Console.Write ("{0} ", x[i]); }
            Console.WriteLine();
            return x;
        }
        }
    class HW3_3
    {
        static void Main(string[] args)
        {
            MyList<int> myList = new MyList<int>();
            myList.Push(15);
            myList.Push(20);
            myList.Push(25);
            myList.Push(30);
            myList.Push(35);
            Console.WriteLine(myList[1]);
            Console.WriteLine(myList[2]);
            Console.WriteLine(myList[3]);
            Console.WriteLine(myList.StackTop);
            MyList<int>[] List = new MyList<int>[100];
            myList.GetArray();
        }
    }
}
