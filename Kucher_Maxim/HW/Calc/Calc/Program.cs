﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc
{
	class Program
	{
		static int mult(string x, string y)
		{
			int res;
			var x1 = Convert.ToInt32(x);
			var y1 = Convert.ToInt32(y);
			res = x1 * y1;
            return res;
		}
		static int division(string x, string y)
		{
			int res;
			var x1 = Convert.ToInt32(x);
			var y1 = Convert.ToInt32(y);
			res = x1 / y1;
            return res;
		}
		static int sum(string x, string y)
		{
			int res;
			var x1 = Convert.ToInt32(x);
			var y1 = Convert.ToInt32(y);
			res = x1 + y1;
            return res;
		}
		static int subt(string x, string y)
		{
			int res;
			var x1 = Convert.ToInt32(x);
			var y1 = Convert.ToInt32(y);
			res = x1 - y1;
            return res;
		}
		static void Main(string[] args)
		{
			string numb, endstr;
			string resstr = "0";
			int res, end;
			Console.WriteLine("Enter an example with a space: ");
			numb = Console.ReadLine();
			string[] OneOf = numb.Split(' ');
            if (numb.Length < 6)
            {
                
                if (numb.Contains('*'))
                {
                    res = mult(OneOf[0], OneOf[2]);
                    Console.WriteLine("Multiplication = {0}", res);
                }
                if (numb.Contains('/'))
                {
                    res = division(OneOf[0], OneOf[2]);
                    Console.WriteLine("Division = {0}", res);
                }
                if (numb.Contains('+'))
                {
                    res = sum(OneOf[0], OneOf[2]);
                    Console.WriteLine("Sum = {0}", res);
                }
                if (numb.Contains('-'))
                {
                    res = subt(OneOf[0], OneOf[2]);
                    Console.WriteLine("Subtraction = {0}", res);
                }
            }
            else
            {
                if (numb.Contains('*') || numb.Contains('/'))
                {
                    for (int i = 0; i < OneOf.Length; i++)
                    {
                        if (OneOf[i] == "*")
                        {
                            res = mult(OneOf[i - 1], OneOf[i + 1]);
                            resstr = res.ToString();
                        }
                        if (OneOf[i] == "/")
                        {
                            res = division(OneOf[i - 1], OneOf[i + 1]);
                            resstr = res.ToString();
                        }

                    }
                }
                if ((numb.Contains('+') && numb.Contains('*')) || (numb.Contains('+') && numb.Contains('/')))
                {
                    for (int i = 0; i < OneOf.Length; i++)
                    {
                        if (OneOf[i] == "+")
                        {
                            if (i == 1)
                            {
                                end = sum(OneOf[i - 1], resstr);
                                endstr = end.ToString();
                                Console.WriteLine("Final = {0}", endstr);
                            }
                            if (i == 3)
                            {
                                end = sum(resstr, OneOf[i + 1]);
                                endstr = end.ToString();
                                Console.WriteLine("Final = {0}", endstr);
                            }
                        }
                    }
                }
                if ((numb.Contains('-') && numb.Contains('*')) || (numb.Contains('-') && numb.Contains('/')))
                {
                    for (int i = 0; i < OneOf.Length; i++)
                    {
                        if (OneOf[i] == "-")
                        {
                            if (i == 1)
                            {
                                end = subt(OneOf[i - 1], resstr);
                                endstr = end.ToString();
                                Console.WriteLine("Final = {0}", endstr);
                            }
                            if (i == 3)
                            {
                                end = subt(resstr, OneOf[i + 1]);
                                endstr = end.ToString();
                                Console.WriteLine("Final = {0}", endstr);
                            }
                        }
                    }
                }
            }





		}
			
		}
	}

