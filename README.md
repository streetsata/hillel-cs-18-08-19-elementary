# Hillel-CS-18-08-elementary

## Summary

* BLOCK # 1: Create and use types
* BLOCK # 2: Manage program flow
* BLOCK # 3: Debug applications and implement security
* BLOCK # 4: Implement data access
* BLOCK # 5: Web App using ASP.NET Core
* Additional block: What else?
* Required Links

****

## *LERNING PATH*

### BLOCK # 1: Create and use types

1. **Create types**
    * Value and reference types
    * Creating value types
    * Creating reference types
2. **Consume types**
    * Boxing and unboxing
    * Cast types
3. **Enforce encapsulation**
    * Access modifiers
    * Enforce encapsulation by using properties
    * Enforce encapsulation by using accessors
    * Enforce encapsulation by using explicit interface implementation
4. **Create and implement a class hierarchy**
    * Design and implement an interface
    * Inherit from a base class
    * Create and implement classes based on the IComparable, IEnumerable, IDisposable, and IUnknown interfaces
5. **Find, execute, and create types at runtime by using reflection**
    * Create and apply attributes
    * Read attributes
    * Using reflection
    * Generate code at runtime by using CodeDOM and Lambda expressions
    * Use types from the System.Reflection namespace
6. **Manage the object life cycle**
    * Garbage collection in .NET
    * Manage unmanaged resources
    * Implement IDisposable, including interaction with finalization
    * Manage IDisposable by using the Using statement
    * Manage finalization and garbage collection
7. **Manipulate strings**
    * The string type
    * Manipulate strings by using the StringBuilder, StringWriter, and StringReader classes
    * Search strings
    * Enumerate string methods
    * Format strings
    * Use string interpolation
8. **Design pattern - Creational Patterns**
    * Singleton
    * Abstract Factory
    * Factory Method
    * Builder
9. **Team application development**
    * E-Shop

****

### BLOCK # 2: Manage program flow

1. **Implement multithreading and asynchronous processing**
    * The Task Parallel library
    * Parallel LINQ
    * Tasks
    * Continuation Tasks
    * Threads and ThreadPool
    * Tasks and the User Interface
    * Using async and await
    * Using concurrent collections
2. **Manage multithreading**
    * Resource synchronization
    * Implementing locking
    * Cancelling a long-running task
    * Implementing thread-safe methods
3. **Implement program flow**
    * Iterating across collections
    * Program decisions
    * Evaluating expressions
4. **Create and implement events and callbacks**
    * Event handlers
    * Unsubscribing from a delegate
    * Using events
    * Create events with built-in delegate types
    * Create delegates
    * Use lambda expressions (anonymous methods)
    * Anonymous methods
5. **Implement exception handling**
    * Exception types
    * The try-catch construction
    * Using the base class of an exception
    * Implement try-catch-finally blocks
    * Throwing exceptions
    * Rethrowing an exception
    * Creating custom exceptions
    * Handling inner exceptions
    * Handling aggregate exceptions
    * Exceptions as part of managed error handing
6. **Design pattern - Behavioral Patterns**
    * Strategy
    * Template Method
    * Mediator
    * Iterator
    * Observer
    * Visitor

****

### BLOCK # 3: Debug applications and implement security

1. **Validate application input**
    * Using JSON
    * Validate JSON data
    * Choose the appropriate data collection type
    * Use the Entity Framework to design your data storage
    * Manage data integrity
    * Evaluate a regular expression to validate the input format
    * Use built-in functions to validate data type and content
2. **Perform symmetric and asymmetric encryption**
    * Cryptography and cryptoanalysis
    * Symmetric and asymmetric encryption
    * Data encryption using AES symmetric encryption
    * Encrypting data using other symmetric standards
    * Implementing public and private key management
    * Digital signatures and certificates
    * Implement the System.Security namespace
    * Data integrity by hashing data
    * Encrypting streams
3. **Manage assemblies**
    * Version assemblies
    * Sign assemblies using strong names
    * The Global Assembly Cache (GAC)
    * Implement side-by-side hosting
    * Put an assembly in the global assembly cache
    * Create a WinMD assembly
4. **Debug an application**
    * Create and manage preprocessor compiler directives
    * Choose an appropriate build configuration
    * Manage programming program database files and (debug symbols)
5. **Implement diagnostics in an application**
    * Implement logging and tracing
    * Profiling applications
    * Create and monitor performance counters
    * Write to the event log
6. **Team application development**
    * Extension "E-Shop"

****

### BLOCK # 4: Implement data access

1. **Perform I/O operations**
    * Read and write files and streams
    * Files storage
    * Read and write from the network by using classes in the System.Net namespace
    * Implement asynchronous I/O operations
2. **Consume data**
    * Retrieve data from a database
    * Update data in a database
    * Consume JSON and XML data
    * Retrieve data by using Windows Communication Foundation (WCF)
3. **Query and manipulate data and objects by using LINQ**
    * Create method-based LINQ queries
    * Query data by using query comprehension syntax
    * Select data by using anonymous types
    * Force execution of a query
    * Read, filter, create, and modify data structures by using LINQ to XML
4. **Serialize and deserialize data by using binary serialization, custom serialization, XML Serializer, JSON Serializer, and Data Contract Serializer**
    * Sample data
    * Use binary serialization
    * Use custom serialization
    * Manage versions with binary serialization
    * Use XML serializer
    * Use JSON Serializer
    * Use Data Contract Serializer
5. **Store data in and retrieve data from collections**
    * Store and retrieve data by using dictionaries, arrays, lists, sets, and queues
    * Choose a collection type
    * Initialize a collection
    * Add and remove items from a collection
    * Use typed vs. non-typed collections
    * Implement custom collections
    * Implement collection interfaces
6. **Design pattern - Structural Patterns**
    * Adapter
    * Facade
    * Decorator
    * Composite
    * Proxy

****

### BLOCK # 5: Web App using ASP.NET Core

1. **Basic ASP.NET Core Razor Page**
2. **Basic ASP.NET Core MVC**
3. **Basic building Web Services and Applications with ASP.NET Core**
4. **Unit Testing** 

****

## What else

1. **GIT**
2. **DataBase Facultative**
3. **HTML/CSS/JS independently**

****

## Required Links

* Learning Management System (LMS): [LMS Hillel](https://lms.ithillel.ua/)
* GitLab: [hillel-cs-18-08-19-elementary](https://gitlab.com/streetsata/hillel-cs-18-08-19-elementary)
